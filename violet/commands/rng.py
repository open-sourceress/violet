# coding=utf-8
import random
import re
import typing

from discord.ext.commands import BadArgument

from ..violetlib.coglocals import *


class DiceExpression:
    format_re = re.compile(r'(?:(\d*)d)?(\d*)')  # "20" -> 1d20, "d20" -> 1d20, "20d" -> 20d6, "20d20" -> 20d20
    __slots__ = ('dice', 'sides')

    def __init__(self, num_dice: int, sides: int):
        self.dice = num_dice
        self.sides = sides

    def roll(self) -> typing.List[int]:
        return [random.randint(1, self.sides) for _ in range(self.dice)]

    def __str__(self) -> str:
        return f'{self.dice}d{self.sides}'

    @classmethod
    async def convert(cls, ctx: VioletContext, arg: str) -> 'DiceExpression':
        match = cls.format_re.match(arg)
        if match is None:
            raise BadArgument(f'`{arg}` is not a valid die expression')
        else:
            return cls(int(match.group(1) or 1), int(match.group(2) or 6))


class Random(Cog):
    @command()
    async def choice(self, ctx: VioletContext, *options: str):
        """ Pick one result from the options provided.
            All options are weighted evenly by default. Specify an option multiple times to increase its weight."""
        if not options:
            raise BadArgument('You must specify at least one option')
        pick = random.choice(options)
        await ctx.send('Your result: {}.', pick)

    choice.example_usage = """
    `{prefix}choice rose daisy violet` - pick a flower, with an equal 33% chance for each
    `{prefix}choice rose daisy violet violet violet` - pick a flower, with a 60% chance of picking violet
    `{prefix}choice "rose daisy" violet` - pick either "rose daisy" or "violet", with a 50% chance for each 
    """

    @command()
    async def choices(self, ctx: VioletContext, n: int, *options: str):
        """ Pick one or more results from the options provided, with replacement.
            All options are weighted evenly by default. Specify an option multiple times to increase its weight."""
        if not options:
            raise BadArgument('You must specify at least one option')
        elif n <= 0:
            raise BadArgument('You must pick at least one option')
        elif n > 5:
            raise BadArgument(f'You must pick at most 5 options')
        elif n > len(options):
            raise BadArgument(f'You must specify {n} options or pick fewer results')
        picks = random.choices(options, k=n)
        await ctx.send('Your results: ' + ', '.join('{}' for _ in picks), *picks)

    choices.example_usage = """
    `{prefix}choices 2 rose daisy violet` - pick two flowers (or the same one twice), with a 33% chance for each
    `{prefix}choices 2 rose daisy violet violet` - pick two flowers, with a 50% chance for violet
    `{prefix}choices 2 rose "daisy violet" violet` - pick two of "rose", "daisy violet", or "violet"
    """

    @command()
    async def sample(self, ctx: VioletContext, n: int, *options: str):
        """ Pick one or more results from the options provided, with replacement.
            All options are weighted evenly by default. Specify an option multiple times to increase its weight."""
        if not options:
            raise BadArgument('You must specify at least one option')
        elif n <= 0:
            raise BadArgument('You must pick at least one option')
        elif n > 5:
            raise BadArgument(f'You must pick at most 5 options')
        elif n > len(options):
            raise BadArgument(f'You must specify {n} options or pick fewer results')
        picks = random.sample(options, k=n)
        await ctx.send('Your results: ' + ', '.join('{}' for _ in picks), *picks)

    sample.example_usage = """
    `{prefix}sample 2 rose daisy violet` - pick two unique flowers, with a 33% chance for each
    `{prefix}sample 2 rose daisy violet violet` - pick two unique flowers, with a 50% chance for violet
    `{prefix}sample 2 rose "daisy violet" violet` - pick two of "rose", "daisy violet", or "violet"
    """

    @command(aliases=['dice'])
    async def roll(self, ctx: VioletContext, *dice: DiceExpression):
        """ Roll some dice, in `{count}d{sides}` format.
            If nothing is specified, one six-sided die is rolled.
            If just a number is specified, one die with that many sides is rolled.
            If more than one die is rolled, each roll and the sum will be shown.
            Multiple expressions can be specified, in which case they'll be shown individually as well as summed."""
        if not dice:
            dice = [DiceExpression(1, 6)]
        elif len(dice) > 5:
            raise BadArgument(f'You must roll at most 5 sets of dice')

        for i, expr in enumerate(dice, start=1):
            if expr.dice > 10:
                raise BadArgument(f'Expression #{i}: you must roll at most 10 dice per set')
            elif expr.sides > 100:
                raise BadArgument(f'Expression #{i}: you must use dice with at most 100 sides')
            elif expr.sides <= 0:
                raise BadArgument(f'Expression #{i}: dice must have at least 1 side')

        results = [expr.roll() for expr in dice]
        lines = []
        params = []
        for expr, result in zip(dice, results):
            line_fmt = '{}: ' + ', '.join('{}' for _ in result)
            params.append(expr)
            params.extend(result)
            if expr.dice > 1:
                lines.append(line_fmt + ' (sum: {})')
                params.append(sum(result))
            else:
                lines.append(line_fmt)
        if len(lines) > 1:
            lines.append('Total sum: {}')
            params.append(sum(map(sum, results)))
        await ctx.send('\n'.join(lines), *params)

    roll.example_usage = """
    `{prefix}roll` - roll a six-sided die
    `{prefix}roll 20` - roll a 20-sided die
    `{prefix}roll d20` - alternative way to roll a 20-sided die
    `{prefix}roll 3d` - roll three six-sided dice
    `{prefix}roll 3d20` - roll three 20-sided dice
    `{prefix}roll 2d6 10 3d20` - roll two six-sided dice, a 10-sided die, and three 20-sided dice
    """
