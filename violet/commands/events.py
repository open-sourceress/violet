# coding=utf-8
import traceback
import typing

from discord.ext import commands

from ..violetlib import format
from ..violetlib.coglocals import *


class Events(Cog, hidden=True):
    clocks: typing.Tuple[str, ...] = ' '.join(':clock{0}: :clock{0}30:'.format(i + 1) for i in range(12)).split()
    clocks = clocks[-2:] + clocks[:-2]  # Move 12:00 and 12:30 to the front

    @Cog.listener()
    async def on_command_error(self, ctx: VioletContext, exc: Exception) -> None:
        if hasattr(ctx.command, 'on_error'):
            return
        elif isinstance(exc, commands.CommandNotFound):
            return
        else:
            return await self.display_error(ctx, exc)

    async def display_error(self, ctx: VioletContext, exc: Exception) -> None:
        exc = getattr(exc, 'original', exc)
        if isinstance(exc, commands.NoPrivateMessage):
            await ctx.send(':no_entry: This command can\'t be used in DMs.')
        elif isinstance(exc, commands.BadArgument):
            await ctx.send(f':x: {exc!s}.')
        elif isinstance(exc, commands.MissingRequiredArgument):
            await ctx.send(':x: You must specify a {}.', exc.param.name)
        elif isinstance(exc, commands.NotOwner):
            await ctx.send(':no_entry_sign: You are not the bot owner.')
        elif isinstance(exc, commands.MissingPermissions):
            await ctx.send(':no_entry_sign: You need permission to {} to do that.',
                           format.permissions(*exc.missing_perms))
        elif isinstance(exc, commands.BotMissingPermissions):
            await ctx.send(':anger: I need permission to {} to do that.', format.permissions(*exc.missing_perms))
        elif isinstance(exc, commands.CommandOnCooldown):
            period = ctx.command._buckets._cooldown.per
            completion = (period - exc.retry_after) / period  # Part of cooldown that's already passed
            idx = int(completion * len(self.clocks)) % len(self.clocks)  # Index in self.clocks (% catches 0.0 case)
            await ctx.send('{} Slow down! You can use this command again in {:.3}s.', self.clocks[idx], exc.retry_after)
        else:
            if ctx.guild is None:
                loc_fmt = 'channel=DMs with {channel.recipient}({channel.recipient.id}) ({channel.id})'
            else:
                loc_fmt = 'channel=#{channel.name} ({channel.id}) guild={guild.name} ({guild.id})'
            loc = loc_fmt.format(channel=ctx.channel, guild=ctx.guild)
            msg = 'Error in command <{0}> message={1.content!r} ({1.id}) {2}'.format(ctx.command.qualified_name,
                                                                                     ctx.message, loc)
            ctx.logger.error(msg + '\n' + ''.join(traceback.format_exception(type(exc), exc, exc.__traceback__)))
            await ctx.send('An error occurred while processing this command.\n```\n{}\n```',
                           ''.join(traceback.format_exception_only(type(exc), exc)))
