# coding=utf-8
import asyncio
import copy
import inspect
import io
import re
import sys
import textwrap
import traceback
import typing
from datetime import datetime, timedelta

import asyncpg
import discord
from discord.ext.commands import CheckFailure, BadArgument

from ..violetlib import VioletCommand, checks, types
from ..violetlib.coglocals import *


class Maintenance(Cog, hidden=True):
    config: 'MaintenanceConfig'
    indent_re = re.compile(r'\s*')
    eval_namespace: typing.Dict[str, typing.Any]
    tasks: 'typing.MutableMapping[int, Task]'
    _need_param_re = re.compile(r'the server expects (\d+) arguments? for this query, 0 were passed')

    def __init__(self, bot: Violet, config: 'MaintenanceConfig'):
        super().__init__(bot, config)
        self.eval_namespace = {'bot': self.bot}
        self.tasks = {}
        exec(self.config.eval_preload, self.eval_namespace)

        for cmd in self.commands:
            checks.owner_only()(cmd)

    @command()
    async def shutdown(self, ctx: VioletContext) -> None:
        await ctx.send(':wave: Goodbye!')
        await ctx.bot.logout()

    @command('eval')
    @checks.bot_permissions('embed_links')
    async def evaluate(self, ctx: VioletContext, *, code: str) -> None:
        result = await self._eval(ctx, code)
        if inspect.iscoroutine(result):
            result = await result

        embed = discord.Embed(title=f'Result ({type(result)!r})', color=0x00FF00)
        s_result = repr(result).replace('`', '`\N{ZERO WIDTH SPACE}')  # to not break code blocks
        if len(s_result) > 2040:  # len(f'```\n{result!r}\n```') > 2048  # embed description limit
            embed.description = f'Result too long ({len(s_result)} chars)\n```\n{s_result[:2000]}...\n```'
        else:
            embed.description = f'```\n{result!r}\n```'
        await ctx.send(embed=embed)

    @evaluate.error
    async def eval_error(self, ctx: VioletContext, exc: BaseException):
        if isinstance(exc, CheckFailure):
            await ctx.bot.get_cog('Events').display_error(ctx, exc)
            return

        exc = getattr(exc, 'original', exc)
        if isinstance(exc, RuntimeError) and exc.__cause__ is not None:
            exc = exc.__cause__
        await ctx.send(embed=discord.Embed(title='Error', description=f'```\n{exc!r}\n```', color=0xFF0000))

    @command()
    @checks.bot_permissions('embed_links')
    async def repl(self, ctx: VioletContext) -> None:
        def check(msg: discord.Message) -> bool:
            return msg.author.id == ctx.author.id and msg.channel.id == ctx.channel.id

        ns = self.eval_namespace.copy()
        ns['_'] = None

        await ctx.message.add_reaction('\N{WHITE HEAVY CHECK MARK}')
        while True:
            try:
                message: discord.Message = await self.bot.wait_for('message', check=check, timeout=600)
                if message.content in ('exit', 'quit'):
                    break
            except asyncio.TimeoutError:
                break

            ns['message'] = message
            try:
                result = await self._eval(ctx, message.content, ns)
                if inspect.iscoroutine(result):
                    result = await result
            except BaseException as exc:
                exc = getattr(exc, 'original', exc)
                if isinstance(exc, RuntimeError) and exc.__cause__ is not None:
                    exc = exc.__cause__
                await ctx.send(embed=discord.Embed(title='Error', description=f'```\n{exc!r}\n```', color=0xFF0000))
            else:
                embed = discord.Embed(title=f'Result ({type(result)!r})', color=0x00FF00)
                s_result = repr(result).replace('`', '`\N{ZERO WIDTH SPACE}')  # to not break code blocks
                if len(s_result) > 2040:  # len(f'```\n{result!r}\n```') > 2048  # embed description limit
                    embed.description = f'Result too long ({len(s_result)} chars)\n```\n{s_result[:2000]}...\n```'
                else:
                    embed.description = f'```\n{result!r}\n```'
                await ctx.send(embed=embed)
                ns['_'] = result
        await ctx.send('REPL terminated.')

    @group()
    @checks.bot_permissions('embed_links')
    async def task(self, ctx: VioletContext, id: int = None):
        if id is None:
            embed = discord.Embed(title='Current tasks', color=ctx.color)
            embed.description = '\n'.join(map(str, self.tasks.values())) or 'No tasks to show.'
            await ctx.send(embed=embed)
        elif id in self.tasks:
            task = self.tasks[id]
            embed = discord.Embed(title=f'Task #{id}', description=f'```py\n{task.code}\n```')
            if task.completed:
                embed.color = 0x00ff00
                result = task.future.result()
                embed.add_field(name=f'Result ({type(result)!r})', value=f'```\n{result!r}\n```',
                                inline=False)
                embed.add_field(name='Runtime', value=str(task.end_time - task.start_time))
            elif task.finished:
                embed.color = 0xff0000
                error = task.future.exception()
                embed.add_field(name='Error', value=f'```\n{type(error).__name__}: {error}\n```')
            else:
                embed.color = ctx.color
                embed.add_field(name='Start time', value=str(task.start_time))
            await ctx.send(embed=embed)
        else:
            raise BadArgument('That task does not exist')

    @task.command()
    async def spawn(self, ctx: VioletContext, *, code: str):
        func = self._compile(code, async_=False)
        task_id = max(self.tasks) + 1 if self.tasks else 0
        task = Task(task_id, self._cleanup_code(code), func)
        self.tasks[task_id] = task
        fut: asyncio.Future = self.bot.loop.run_in_executor(None, task.run, ctx)
        task.future = fut
        fut.add_done_callback(lambda _: self.bot.loop.create_task(
            ctx.send('{}, task #{} is finished.', ctx.author.mention, task_id, clean_member=False)))
        await ctx.send('Task spawned (ID {}).', task_id)

    @task.command()
    async def remove(self, ctx: VioletContext, task_id: int):
        task = self.tasks.pop(task_id, None)
        if task is None:
            raise BadArgument('That task does not exist.')

        if task.running:
            task.future.cancel()
        await ctx.send('Task removed.')

    def _cleanup_code(self, code: str) -> str:
        if code.startswith('```'):
            return code.split('\n', 1)[1].rstrip('`\n')
        else:
            return code.strip('`')

    def _compile(self, code: str, namespace: typing.Mapping[str, typing.Any] = None, *,
                 async_: bool = True) -> typing.Callable:
        code = self._cleanup_code(code)
        # Detect indentation by finding the first line that starts with space characters, with fallback to tab
        header = f'def evaluated_function(ctx):\n'
        if async_:
            header = 'async ' + header
        if '\n' in code:
            indent = next(filter(None, (self.indent_re.match(line).group(0) for line in code.splitlines())), '\t')
            code_obj = compile(f'{header}{textwrap.indent(code, indent)}', '<eval>', 'exec')
        else:  # Single-line code: implicit `return` at start of line
            try:
                code_obj = compile(f'{header}\treturn {code}', '<eval>', 'exec')
            except SyntaxError:  # If the one line is a return/del/global/assignment/otherwise not-returnable expression
                code_obj = compile(f'{header}\t{code}', '<eval>', 'exec')

        locals_ = {}
        exec(code_obj, self.eval_namespace if namespace is None else namespace, locals_)
        return locals_['evaluated_function']

    async def _eval(self, ctx: VioletContext, code: str, namespace: typing.Mapping[str, typing.Any] = None):
        func = self._compile(code, namespace)
        try:
            return await func(ctx)
        except Exception:
            raise
        except BaseException as exc:
            raise RuntimeError from exc  # ensure SystemExit and co don't actually shut down the bot

    @command()
    async def query(self, ctx: VioletContext, *, query: str) -> None:
        conn: asyncpg.Connection
        try:
            async with self.acquire_db() as conn:
                records: typing.Sequence[asyncpg.Record] = await conn.fetch(query)
        except asyncpg.InterfaceError as exc:
            param_match = self._need_param_re.match(exc.args[0]) if exc.args else None
            if param_match is None:
                raise

            needed_params = int(param_match.group(1))
            await ctx.send('Query arguments ({}):', needed_params)
            response = await ctx.bot.wait_for('message', timeout=600,
                                              check=lambda msg: msg.author.id == ctx.author.id
                                                                and msg.channel.id == ctx.channel.id)
            args = await self._eval(ctx, response.content)
            if not isinstance(args, tuple):
                args = (args,)
            if len(args) != needed_params:
                raise BadArgument(f'This query needs {needed_params} arguments but received {len(args)}') from None

            async with self.acquire_db() as conn:
                records: typing.Sequence[asyncpg.Record] = await conn.fetch(query, *args)

        if not records:
            await ctx.send('No results')
        else:
            columns = tuple(records[0].keys())
            s_records = [tuple(repr(val) for val in record) for record in records]
            column_widths = [max(len(record[i]) for record in [columns] + s_records) for i, col in enumerate(columns)]
            await ctx.send('```py\n{}\n{}\n{}\n```', self._format_row(columns, column_widths),
                           '-' * (sum(column_widths) + 3 * (len(columns) - 1)),  # 3 chars of space between each column
                           '\n'.join(self._format_row(record, column_widths) for record in s_records))

    @query.error
    async def query_error(self, ctx: VioletContext, exc: Exception) -> None:
        if isinstance(exc, CheckFailure):
            await ctx.bot.get_cog('Events').display_error(ctx, exc)
            return

        exc = getattr(exc, 'original', exc)
        await ctx.send('An error occurred while processing this query.\n```\n{}\n```',
                       ''.join(traceback.format_exception_only(type(exc), exc)))

    @staticmethod
    def _format_row(row: typing.Sequence[str], col_widths: typing.Sequence[int]) -> str:
        return ' | '.join(col.ljust(width) for col, width in zip(row, col_widths))

    @command()
    async def reload(self, ctx: VioletContext, name: str) -> None:
        ctx.bot.reload_extension(f'violet.commands.{name}')
        await ctx.send('Extension reloaded.')

    @command(aliases=['su'])
    async def impersonate(self, ctx: VioletContext, user: discord.Member, *, content: str) -> None:
        new_message = copy.copy(ctx.message)
        new_message.author = user
        new_message.content = content
        ctx.bot.dispatch('message', new_message)

    @command()
    async def bridge(self, ctx: VioletContext, destination: typing.Union[discord.TextChannel, discord.User]):
        if isinstance(destination, discord.User):
            dest = await destination.create_dm()
        else:
            dest = destination
        await ctx.message.add_reaction('\N{STUDIO MICROPHONE}')
        await ctx.message.add_reaction('\N{OCTAGONAL SIGN}')

        def stop_check(rxn: discord.Reaction, user: discord.User) -> bool:
            return user.id == ctx.author.id and rxn.message.id == ctx.message.id and rxn.emoji == '\N{OCTAGONAL SIGN}'

        bridge = Bridge(ctx, dest)
        fut = self.bot.loop.create_task(bridge.connect())
        await ctx.bot.wait_for('reaction_add', check=stop_check)
        fut.cancel()
        await ctx.message.remove_reaction('\N{OCTAGONAL SIGN}', ctx.me)


class MaintenanceConfig(Config):
    eval_preload: str = inspect.cleandoc("""
    global discord, violet, violetlib
    import discord, violet
    from violet import violetlib
    """)


class Task:
    __slots__ = ('id', 'code', 'func', 'future', 'start_time', 'end_time', 'running')
    id: int
    code: str
    func: typing.Callable[[VioletContext], typing.Any]
    future: typing.Optional[asyncio.Future]
    start_time: typing.Optional[datetime]
    end_time: typing.Optional[datetime]
    running: bool

    def __init__(self, id: int, code: str, func: typing.Callable[[VioletContext], typing.Any]):
        self.id = id
        self.code = code
        self.func = func
        self.future = None
        self.start_time = None
        self.end_time = None
        self.running = False

    def run(self, ctx: VioletContext):
        self.running = True
        self.start_time = datetime.utcnow()
        try:
            return self.func(ctx)
        finally:
            self.running = False
            self.end_time = datetime.utcnow()

    @property
    def started(self) -> bool:
        return self.future is not None

    @property
    def finished(self) -> bool:
        return self.started and self.future.done()

    @property
    def completed(self) -> bool:
        return self.finished and self.future.exception() is None

    def __str__(self):
        if self.completed:
            status = f'finished successfully in {self.end_time - self.start_time}'
        elif self.finished:
            status = 'finished with error'
        elif self.started:
            status = f'running for {datetime.utcnow() - self.start_time}'
        else:
            status = 'unstarted'
        return f'{self.id}: {status}'


class Bridge:
    __slots__ = ('ctx', 'source_channel', 'dest_channel', 'inbound_messages', 'outbound_messages')
    ctx: VioletContext
    source_channel: types.AnyTextChannel
    dest_channel: types.AnyTextChannel
    inbound_messages: typing.MutableMapping[int, discord.Message]  # Dest-channel message ID to source-channel message
    outbound_messages: typing.MutableMapping[int, discord.Message]  # Source-channel message ID to dest-channel message
    typing_duration = timedelta(seconds=10)

    def __init__(self, ctx: VioletContext, dest_channel: types.AnyTextChannel):
        self.ctx = ctx
        self.source_channel = ctx.channel
        self.dest_channel = dest_channel
        self.inbound_messages = {}
        self.outbound_messages = {}

    async def copy_inbound_messages(self):
        def check(message: discord.Message) -> bool:
            return message.channel.id == self.dest_channel.id

        while True:
            message: discord.Message = await self.ctx.bot.wait_for('message', check=check)
            embed = discord.Embed(color=getattr(message.author, 'color', discord.Color.default()),
                                  description=message.content)
            embed.set_author(name=message.author.display_name,
                             icon_url=message.author.avatar_url_as(format='png', size=64))
            response = await self.ctx.send('{0} ({0.id})\n{1}', message.author,
                                           '\n'.join(file.url for file in message.attachments), embed=embed)
            self.inbound_messages[message.id] = response

    async def copy_outbound_messages(self):
        def check(message: discord.Message) -> bool:
            return message.channel.id == self.source_channel.id and message.author.id == self.ctx.author.id

        while True:
            message = await self.ctx.bot.wait_for('message', check=check)
            if message.attachments:
                async with self.ctx.bot.http_session.get(message.attachments[0].url) as resp:
                    buf = io.BytesIO(await resp.read())
                buf.seek(0)
                file = discord.File(buf, message.attachments[0].filename)
            else:
                file = None
            response = await self.dest_channel.send(message.content, file=file)
            self.outbound_messages[message.id] = response

    async def copy_inbound_edits(self):
        def check(before: discord.Message, after: discord.Message) -> bool:
            return before.id in self.inbound_messages

        while True:
            _, after = await self.ctx.bot.wait_for('message_edit', check=check)
            response = self.inbound_messages[after.id]
            response.embeds[0].description = after.content
            await response.edit(content=response.content, embed=response.embeds[0])

    async def copy_outbound_edits(self):
        def check(before: discord.Message, after: discord.Message) -> bool:
            return before.id in self.outbound_messages

        while True:
            _, after = await self.ctx.bot.wait_for('message_edit', check=check)
            response = self.outbound_messages[after.id]
            await response.edit(content=after.content)

    async def copy_inbound_deletes(self):
        def check(message: discord.Message) -> bool:
            return message.id in self.inbound_messages

        while True:
            message = await self.ctx.bot.wait_for('message_delete', check=check)
            response = self.inbound_messages.pop(message.id)
            await response.delete()

    async def copy_outbound_deletes(self):
        def check(message: discord.Message) -> bool:
            return message.id in self.outbound_messages

        while True:
            message = await self.ctx.bot.wait_for('message_delete', check=check)
            response = self.outbound_messages.pop(message.id)
            await response.delete()

    async def copy_inbound_typing(self):
        def check(channel: types.AnyTextChannel, user: types.AnyUser, when: datetime) -> bool:
            return channel.id == self.dest_channel.id and user.id != self.ctx.me.id

        typing_end = datetime.min
        while True:
            _, _, start = await self.ctx.bot.wait_for('typing', check=check)
            if start + self.typing_duration > typing_end:
                await self.source_channel.trigger_typing()
                typing_end = datetime.utcnow()

    async def copy_outbound_typing(self):
        def check(channel: types.AnyTextChannel, user: types.AnyUser, when: datetime) -> bool:
            return channel.id == self.source_channel.id and user.id == self.ctx.author.id

        typing_end = datetime.min
        while True:
            _, _, start = await self.ctx.bot.wait_for('typing', check=check)
            if start + self.typing_duration > typing_end:
                await self.dest_channel.trigger_typing()
                typing_end = datetime.utcnow()

    async def copy_inbound_reaction_adds(self):
        def check(reaction: discord.Reaction, user: types.AnyUser) -> bool:
            # Ignore reactions with count > 1, as the reaction would've been copied when count == 1
            return reaction.message.id in self.inbound_messages and reaction.count == 1 and (
                    isinstance(reaction.emoji, str) or reaction.emoji in self.ctx.bot.emojis)

        while True:
            reaction, _ = await self.ctx.bot.wait_for('reaction_add', check=check)
            response = self.inbound_messages[reaction.message.id]
            await response.add_reaction(reaction.emoji)

    async def copy_outbound_reaction_adds(self):
        def check(reaction: discord.Reaction, user: types.AnyUser) -> bool:
            return reaction.message.id in self.outbound_messages and user.id == self.ctx.author.id and (
                    isinstance(reaction.emoji, str) or reaction.emoji in self.ctx.bot.emojis)

        while True:
            reaction, _ = await self.ctx.bot.wait_for('reaction_add', check=check)
            response = self.outbound_messages[reaction.message.id]
            await response.add_reaction(reaction.emoji)

    async def copy_inbound_reaction_removes(self):
        def check(reaction: discord.Reaction, user: types.AnyUser) -> bool:
            # Ignore reactions until the last one is removed
            return reaction.message.id in self.inbound_messages and reaction.count == 0 and (
                    isinstance(reaction.emoji, str) or reaction.emoji in self.ctx.bot.emojis)

        while True:
            reaction, _ = await self.ctx.bot.wait_for('reaction_remove', check=check)
            response = self.inbound_messages[reaction.message.id]
            await response.remove_reaction(reaction.emoji, self.ctx.me)

    async def copy_outbound_reaction_removes(self):
        def check(reaction: discord.Reaction, user: types.AnyUser) -> bool:
            return reaction.message.id in self.outbound_messages and user.id == self.ctx.author.id and (
                    isinstance(reaction.emoji, str) or reaction.emoji in self.ctx.bot.emojis)

        while True:
            reaction, _ = await self.ctx.bot.wait_for('reaction_remove', check=check)
            response = self.outbound_messages[reaction.message.id]
            await response.remove_reaction(reaction.emoji, self.ctx.me)

    async def connect(self):
        return await asyncio.gather(self.copy_inbound_messages(), self.copy_outbound_messages(),
                                    self.copy_inbound_edits(), self.copy_outbound_edits(),
                                    self.copy_inbound_deletes(), self.copy_outbound_deletes(),
                                    self.copy_inbound_typing(), self.copy_outbound_typing(),
                                    self.copy_inbound_reaction_adds(), self.copy_outbound_reaction_adds(),
                                    self.copy_inbound_reaction_removes(), self.copy_outbound_reaction_removes())
