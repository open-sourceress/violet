# coding=utf-8
import asyncio
import functools
import typing
from datetime import datetime, timedelta

import discord
from discord.ext.commands import BadArgument, Converter, MemberConverter

from violet.violetlib import checks, convert, format
from ..violetlib.coglocals import *


class BanTarget(Converter):
    member_converter = MemberConverter()

    async def convert(self, ctx: VioletContext, argument: str) -> typing.Union[discord.Member, discord.Object]:
        try:
            return await self.member_converter.convert(ctx, argument)
        except BadArgument:
            if argument.isdigit():
                return discord.Object(int(argument))
            else:
                raise


if typing.TYPE_CHECKING:
    BanTarget = typing.Union[discord.Member, discord.Object]


class Moderation(Cog):
    @command(aliases=['hackban'])
    @checks.guild_only()
    @checks.bot_permissions('ban_members')
    @checks.author_permissions('ban_members')
    async def ban(self, ctx: VioletContext, member: BanTarget, purge_days: typing.Optional[int] = 0, *,
                  reason: str = 'No reason provided'):
        """ Ban a member from this guild.
            This command allows banning users that aren't in the guild (known as hackbanning) by passing their user ID.
            No messages are deleted by default. Between 0 and 7 days of messages can be deleted.
            The audit log reason will include your username and the reason you provide.
            """
        if isinstance(member, discord.Member):
            self._check_interact_member(ctx, member, 'ban')
        await ctx.guild.ban(member, delete_message_days=purge_days, reason=f'Banned by {ctx.author}: {reason}')
        if isinstance(member, discord.Member):
            await ctx.send('{0} (ID {0.id}) has been banned.', member)
        else:
            await ctx.send('User {} has been banned.', member.id)

    ban.example_usage = """
    `{prefix}ban {ctx.author}`: attempt to ban yourself (not allowed)
    `{prefix}ban {ctx.me}`: attempt to ban me (not allowed)
    `{prefix}ban {ctx.author} example reason`: ban a member with a reason
    `{prefix}ban {ctx.author} 1`: ban a member and delete their messages within the last 24 hours
    `{prefix}ban {ctx.author} 4 example reason`: ban a member and delete 96 hours of messages, with a reason
    `{prefix}ban {ctx.author.id}`: ban a member by ID (works even if they aren't in the guild)
    """

    @command()
    @checks.guild_only()
    @checks.bot_permissions('ban_members')
    @checks.author_permissions('ban_members')
    async def unban(self, ctx: VioletContext, member: int, *, reason: str = 'No reason provided'):
        """ Unban a member from this guild.
            The audit log reason will include your username and the reason you provide.
            """
        # No need to _check_interact_member; users not in the guild are below any member permissions-wise
        await ctx.guild.unban(discord.Object(member), reason=f'Unbanned by {ctx.author}: {reason}')
        await ctx.send('User {} has been unbanned.', member)

    unban.example_usage = """
    `{prefix}unban {ctx.author.id}`: attempt to unban yourself (won't work, because you aren't banned)
    `{prefix}unban {ctx.author.id} example reason`: unban a member with a reason
    """

    @command()
    @checks.guild_only()
    @checks.bot_permissions('kick_members')
    @checks.author_permissions('kick_members')
    async def kick(self, ctx: VioletContext, member: discord.Member, *, reason: str = 'No reason provided'):
        """ Kick a member from this guild.
            The audit log reason will include your username and the reason you provide.
            """
        self._check_interact_member(ctx, member, 'kick')
        await ctx.guild.kick(member, reason=f'Kicked by {ctx.author}: {reason}')
        await ctx.send('{0} (ID {0.id}) has been kicked.', member)

    kick.example_usage = """
    `{prefix}kick {ctx.author}`: attempt to kick yourself (not allowed)
    `{prefix}kick {ctx.me}`: attempt to kick me (not allowed)
    `{prefix}kick {ctx.author} example reason`: kick a member with a reason
    """

    def _check_interact_member(self, ctx: VioletContext, target: discord.Member, verb: str):
        if target.id == ctx.author.id:
            raise BadArgument(f"You can't {verb} yourself")
        elif target.id == ctx.me.id:
            raise BadArgument(f"I can't {verb} myself")
        elif target.id == ctx.guild.owner.id:
            raise BadArgument(f"You can't {verb} the guild owner")
        elif target.top_role >= ctx.me.top_role and ctx.me.id != ctx.guild.owner.id:
            raise BadArgument(f"I can't {verb} this user (their top role must be below mine)")
        elif target.top_role >= ctx.author.top_role and ctx.author.id != ctx.guild.owner.id:
            raise BadArgument(f"You can't {verb} this user (their top role must be below yours)")

    @command()
    @checks.guild_only()
    @checks.author_permissions('manage_roles')
    @checks.bot_permissions('manage_roles')
    async def mute(self, ctx: VioletContext, member: discord.Member, *, reason: str = 'No reason provided'):
        """ Mute a member of this guild.
            This removes the member's permissions to send messages, add reactions, and speak.
            Muting yourself is allowed. You can give yourself send perms in a channel and call unmute to undo it.
            The audit log reason will include your username and the reason you provide.
            """
        self._check_interact_member(ctx, member, 'mute')
        await self._set_permissions(member, f'Muted by {ctx.author}: {reason}',
                                    send_messages=False, add_reactions=False, speak=False)
        await ctx.send('{0} (ID {0.id}) has been muted.', member)

    mute.example_usage = """
    `{prefix}mute {ctx.author}`: mute yourself
    `{prefix}mute {ctx.me}`: mute me
    `{prefix}mute {ctx.author} example reason`: mute a member with a reason
    """

    @command()
    @checks.guild_only()
    @checks.author_permissions('manage_roles')
    @checks.bot_permissions('manage_roles')
    async def unmute(self, ctx: VioletContext, member: discord.Member, *, reason: str = 'No reason provided'):
        """ Unmute a member of this guild.
            This restores the member's permissions to send messages, add reactions, and speak.
            The audit log reason will include your username and the reason you provide.
            """
        self._check_interact_member(ctx, member, 'unmute')
        await self._set_permissions(member, f'Unmuted by {ctx.author}: {reason}',
                                    send_messages=None, add_reactions=None, connect=None, speak=None)
        await ctx.send('{0} (ID {0.id}) has been unmuted.', member)

    unmute.example_usage = """
    `{prefix}unmute {ctx.author}`: unmute yourself
    `{prefix}unmute {ctx.me}`: unmute me
    `{prefix}unmute {ctx.author} example reason`: unmute a member with a reason
    """

    @command()
    @checks.guild_only()
    @checks.author_permissions('manage_roles')
    @checks.bot_permissions('manage_roles')
    async def deafen(self, ctx: VioletContext, member: discord.Member, *, reason: str = 'No reason provided'):
        """ Deafen a member of this guild.
            This removes the member's permissions to read messages and see channels.
            Deafening yourself is allowed. You will need another member to undeafen you.
            The audit log reason will include your username and the reason you provide.
            """
        self._check_interact_member(ctx, member, 'deafen')
        async with ctx.typing():
            await self._set_permissions(member, f'Deafened by {ctx.author}: {reason}', read_messages=False)
        await ctx.send('{0} (ID {0.id}) has been deafened.', member)

    deafen.example_usage = """
    `{prefix}deafen {ctx.author}`: deafen yourself
    `{prefix}deafen {ctx.me}`: deafen me
    `{prefix}deafen {ctx.author} example reason`: deafen a member with a reason
    """

    @command()
    @checks.guild_only()
    @checks.author_permissions('manage_roles')
    @checks.bot_permissions('manage_roles')
    async def undeafen(self, ctx: VioletContext, member: discord.Member, *, reason: str = 'No reason provided'):
        """ Undeafen a member of this guild.
            This restores the member's permissions to read messages and see channels.
            The audit log reason will include your username and the reason you provide.
            """
        self._check_interact_member(ctx, member, 'undeafen')
        async with ctx.typing():
            await self._set_permissions(member, f'Undeafened by {ctx.author}: {reason}', read_messages=None)
        await ctx.send('{0} (ID {0.id}) has been undeafened.', member)

    undeafen.example_usage = """
    `{prefix}undeafen {ctx.author}`: undeafen yourself
    `{prefix}undeafen {ctx.me}`: undeafen me
    `{prefix}undeafen {ctx.author} example reason`: undeafen a member with a reason
    """

    @command()
    @checks.guild_only()
    @checks.author_permissions('manage_roles')
    @checks.bot_permissions('manage_roles')
    async def timeout(self, ctx: VioletContext, duration: convert.timedelta, *, reason: str = 'No reason provided'):
        """ Put this channel in timeout.
            Members below the caller can't send messages or add reactions for the given duration.
            Sending "cancel" will cancel the timeout.
            The audit log reason will include your username and the reason you provide.
            """
        logs_reason = f'Timeout by {ctx.author}: {reason}'
        deny_kwargs = {perm: False for perm in ('send_messages', 'add_reactions')}

        def should_deny(target: typing.Union[discord.Member, discord.Role]) -> bool:
            top_role = target if isinstance(target, discord.Role) else target.top_role
            if top_role >= ctx.author.top_role:
                return False
            if isinstance(target, discord.Member):
                perms = ctx.channel.permissions_for(target)
            else:
                perms = discord.Permissions(target.permissions.value)
                perms.update(**ctx.channel.overwrites_for(target)._values)
            return all(getattr(perms, name) for name in deny_kwargs)

        to_deny = [(target, discord.PermissionOverwrite.from_pair(*overwrite.pair())) for target, overwrite in
                   ctx.channel.overwrites.items() if should_deny(target)]  # Defensive copy
        to_allow = [(target, ctx.channel.overwrites_for(target)) for target in (ctx.author, ctx.me)]
        async with ctx.typing():
            await asyncio.gather(
                *[self._update_perms(ctx.channel, target, logs_reason, **deny_kwargs) for target, _ in to_deny])
            await asyncio.gather(  # Set to_allow afterward to prevent a race when to_allow and to_deny intersect
                *[self._update_perms(ctx.channel, target, logs_reason, send_messages=True) for target, _ in to_allow])
        message = await ctx.send('This channel has been timed out for {}. Send `cancel` to end the timeout early.',
                                 format.timedelta(duration))

        try:
            await ctx.bot.wait_for('message', timeout=duration.total_seconds(), check=lambda message: (
                    message.channel.id == ctx.channel.id and message.content.casefold() == 'cancel'))
        except asyncio.TimeoutError:  # No cancel message
            cancelled = False
        else:
            cancelled = True

        await asyncio.gather(*[
            ctx.channel.set_permissions(target, overwrite=None if overwrite.is_empty() else overwrite,
                                        reason=logs_reason) for target, overwrite in to_allow + to_deny])

        await message.edit(content='~~{}~~\nThe timeout has {}.'.format(
            message.content, 'been cancelled' if cancelled else 'ended'))

    async def _set_permissions(self, target: typing.Union[discord.Member, discord.Role], reason: str,
                               **permissions: typing.Optional[bool]) -> typing.Collection[discord.abc.GuildChannel]:
        affected = {channel for channel in target.guild.channels if
                    channel.permissions_for(target.guild.me).manage_roles}
        await asyncio.gather(*[self._update_perms(channel, target, reason, **permissions) for channel in affected])
        return affected

    async def _update_perms(self, channel: discord.abc.GuildChannel, target: typing.Union[discord.Member, discord.Role],
                            reason: str, **permissions: typing.Optional[bool]):
        overwrite = channel.overwrites_for(target)
        overwrite.update(**permissions)
        await channel.set_permissions(target, overwrite=None if overwrite.is_empty() else overwrite, reason=reason)

    timeout.example_usage = """
    `{prefix}timeout 1m30s`: time out this channel for 90 seconds
    `{prefix}timeout 1d example reason`: time out this channel for 24 hours, with a reason
    """

    @command(aliases=['prune'])
    @checks.guild_only()
    @checks.bot_permissions('manage_messages', 'read_message_history')
    @checks.author_permissions('manage_messages')
    async def purge(self, ctx: VioletContext, count: int, *, author: discord.Member = None):
        """ Purge messages from this channel, from a given member or anyone.
            If a member is provided, their last `count` messages in this channel will be deleted.
            If not, the last `count` messages in this channel will be deleted.
            The message calling this command is always deleted, and doesn't contribute to the count.
            Deleting messages older than two weeks is possible, but significantly slower.
            The audit log reason will include your username and the reason you provide.
            """
        if author is None:
            def check(_: discord.Message) -> bool:
                return True
        else:
            def check(msg: discord.Message) -> bool:
                return msg.author.id == author.id

        async with ctx.typing():
            purger = MessagePurger(ctx.channel)
            exhausted = False  # True if there are less than `count` messages to delete
            tasks: typing.List[asyncio.Task] = []

            async for message in ctx.history(limit=None):
                if message.id == ctx.message.id or check(message):
                    purger.add(message)
                if purger.count > count:  # >= count + 1 (because ctx.message is included)
                    break
                coro = purger.next_task()
                if coro is not None:
                    tasks.append(ctx.bot.loop.create_task(coro))
            else:
                exhausted = True

            coro = purger.next_task(True)
            while coro is not None:
                tasks.append(ctx.bot.loop.create_task(coro))
                coro = purger.next_task(True)
            await asyncio.gather(*tasks)

        # "all {} message(s)" indicates why fewer messages were deleted than the user requested
        fmt = 'Purged {} message(s){}.'.format('all {}' if exhausted else '{}',
                                               '' if author is None else ' from {}')
        await ctx.send(fmt, purger.count - 1, author, delete_after=5)

    purge.example_usage = """
    `{prefix}purge 10`: delete this message and the 10 above it
    `{prefix}purge 10 {ctx.author}`: delete this message and your last 10 messages above it
    """

    @command('permissionscheck', aliases=['permissioncheck', 'permscheck', 'permcheck'])
    @checks.bot_permissions('embed_links')
    @checks.author_permissions('manage_roles')
    async def permissions_check(self, ctx: VioletContext, *permissions: convert.Permissions):
        """ Show all roles with the given permission(s). Useful for ensuring users don't have permissions they shouldn't.
            One page is shown for each permission, listing each role that grants the permission by name and ID.
            If no permissions are given, a default list of "dangerous" permissions is used.
            """
        if permissions:
            perms = discord.Permissions(functools.reduce(int.__or__, (perms.value for perms in permissions)))
        else:
            perms = discord.Permissions(0x79C2213E)  # Everything you probably don't want normal users doing

        pages = [(name, ' '.join(name.split('_')).replace('guild', 'server').title()) for name, has in perms if has]
        embeds = [discord.Embed(color=ctx.color, title=f'Permissions check: {clean}') for _, clean in pages]
        roles = [(f'{role.name} ({role.id})', role.permissions) for role in reversed(ctx.guild.roles[1:])
                 if role.permissions.value & perms.value]
        for (name, _), embed in zip(pages, embeds):
            embed.description = '\n'.join(role for role, perms in roles if getattr(perms, name)) or 'No roles'

        if len(embeds) == 1:
            await ctx.send(embed=embeds[0])
        else:
            await ctx.paginate(embeds)

    permissions_check.example_usage = """
    `{prefix}permissionscheck`: show all roles that have potentially-dangerous permissions
    `{prefix}permissions 2042765630`: show the permissions checked by default
    `{prefix}permissionscheck mention_everyone send_tts_messages`: show all roles with any of the listed permissions
    """


# ctx.channel.purge with check and limit deletes any in last `limit` that pass `check`, where we want to delete any that
# pass `check`, up to `limit` (which could require requesting more than `limit` messages)
class MessagePurger:
    bulk_limit = 100  # Up to 100 messages can be deleted at once
    bulk_threshold = timedelta(days=14)  # Messages older than 2 weeks can't be deleted

    _channel: discord.TextChannel
    _bulk_cutoff: timedelta
    _bulk_queue: typing.List[discord.Message]  # To be deleted in bulk
    _solo_queue: typing.List[discord.Message]  # To be deleted individually
    count: int

    def __init__(self, channel: discord.TextChannel):
        self._channel = channel
        self._bulk_cutoff = datetime.utcnow() - self.bulk_threshold
        self._bulk_queue = []
        self._solo_queue = []
        self.count = 0

    def add(self, message: discord.Message) -> None:
        """Schedule the given message for deletion in the purge."""
        if message.created_at >= self._bulk_cutoff:
            self._bulk_queue.append(message)
        else:
            self._solo_queue.append(message)
        self.count += 1

    def next_task(self, finished: bool = False) -> typing.Optional[typing.Awaitable]:
        """ Return the next task to await for this purge.
            If no more messages will be added to this purge, pass finished=True.
            If this returns None, awaiting all tasks previously returned will complete the purge."""
        if len(self._bulk_queue) >= self.bulk_limit or (finished and self._bulk_queue):
            ret = self._channel.delete_messages(self._bulk_queue[:self.bulk_limit])
            self._bulk_queue = self._bulk_queue[self.bulk_limit:]
            return ret
        elif self._solo_queue:
            return self._solo_queue.pop().delete()
        else:
            return None
