# coding=utf-8
import random

import discord
from discord.ext.commands import BadArgument

from violet.violetlib import format
from ..violetlib.coglocals import *


class Love(Cog):
    # Discord renders HEAVY BLACK HEART red as :heart:
    heart_reactions = '\N{HEAVY BLACK HEART}\N{YELLOW HEART}\N{GREEN HEART}\N{BLUE HEART}\N{PURPLE HEART}'
    # All Discord heart emoji
    heart_emoji = set(heart_reactions + '\N{BLACK HEART}\N{SPARKLING HEART}\N{HEART DECORATION}\N{TWO HEARTS}'
                                        '\N{HEAVY HEART EXCLAMATION MARK ORNAMENT}\N{BLACK HEART SUIT}'
                                        '\N{HEART WITH RIBBON}\N{REVOLVING HEARTS}\N{BEATING HEART}\N{GROWING HEART}')

    love_messages = ["I appreciate you.", "you are the most perfect you there is.", "you are enough.", "you're strong.",
                     "you deserve good things.", "you should be proud of yourself.", "you are good enough.",
                     "you're making a difference.", "colors seem brighter when you're around.", "you're one of a kind.",
                     "you're inspiring.", "the world is better because you're in it.", "you're awesome.",
                     "you're totally rad.", "it's going to be alright.", "you're doing great.", "you are loved.",
                     "it's going to be okay.", "no matter how far down you are, it's going to be just fine.",
                     "I know life can be hard, but you can get through it.", "I have total faith in you.",
                     "there are people who care about you.", "you're not alone.", "you deserve the world.",
                     "you deserve love.", "you matter even if you are at your lowest.", "you are irreplaceable.",
                     "you mean the world to someone. Don't take that away from them.", "you're an amazing person."]

    @Cog.listener()
    async def on_message(self, message: discord.Message):
        me = (message.guild or message.channel).me
        if message.author.bot or not (message.content and message.channel.permissions_for(me).add_reactions):
            return

        if set(message.content.split()) == {'<3'}:  # Any number of text hearts or spaces
            for heart in self.heart_reactions:
                await message.add_reaction(heart)
            return

        # Remove '<3', orange heart (can't use as a reaction), and spaces
        content = ''.join(message.content.replace('<3', '').replace('\U0001F9E1', '').split())
        if set(content) <= self.heart_emoji:  # Every character is a heart
            seen = set()
            to_react = [c for c in content if not (c in seen or seen.add(c))]
            for c in to_react[:5]:
                await message.add_reaction(c)
            return

    @command()
    async def hug(self, ctx: VioletContext, *members: discord.User):
        """Give people a hug."""
        # Remove duplicates, preserving order
        seen = set()
        targets = [member for member in members if not (member in seen or seen.add(member))]
        if len(targets) > 8:
            raise BadArgument('You can hug up to 8 people at once')
        elif targets:
            target = format.pluralize(["**{}**" for _ in targets])
            fmt_args = targets
        else:
            target, fmt_args = 'themselves', []
        await ctx.send(f'**{{}}** hugs {target}.', ctx.author, *fmt_args)

    hug.example_usage = """
    `{prefix}hug`: give yourself a hug. You've earned it.
    `{prefix}hug {ctx.me}`: give me a hug. Thank you. \N{PURPLE HEART}
    `{prefix}hug {ctx.author} {ctx.me}`: hug both of us. Group hugs are the best.
    """

    @command()
    async def love(self, ctx: VioletContext, *members: discord.Member):
        """Spread the love."""
        members = members or [ctx.author]
        seen = set()
        targets = [member for member in members if not (member in seen or seen.add(member))]
        if len(targets) > 4:
            raise BadArgument('You can love up to 4 people at once')

        messages = random.sample(self.love_messages, len(targets))  # Ensure no two members get the same message
        await ctx.send('\n'.join(f'**{{}}**, {message}' for message in messages), *targets)

    love.example_usage = """
    `{prefix}love`: give yourself solf some love. You deserve it.
    `{prefix}love {ctx.me}`: show me some love. It's much appreciated. \N{PURPLE HEART}
    `{prefix}love {ctx.author} {ctx.me}`: give some love for us both. Spread the love around.
    """
