# coding=utf-8
import random
import typing

import aiohttp
from collections import namedtuple

import discord
from discord.ext.commands import BadArgument

from violet.violetlib import checks
from ..violetlib.coglocals import *

Comic = namedtuple('Comic', 'number title alt_text year month day image_url')


class XKCD(Cog):
    xkcd_color = discord.Color(0x96A8C8)
    latest: int = 0  # Maximum number to pick random from

    def __init__(self, bot: Violet, config):
        super().__init__(bot, config)
        self.session = aiohttp.ClientSession(loop=bot.loop)

    async def _get_comic(self, number: int = None) -> typing.Optional[Comic]:
        if number is None:
            url = 'https://xkcd.com/info.0.json'
        else:
            url = f'https://xkcd.com/{number}/info.0.json'

        async with self.session.get(url) as resp:
            if resp.status == 404:
                return None
            comic_json = await resp.json()
            comic = Comic(comic_json['num'], comic_json['safe_title'], comic_json['alt'], int(comic_json['year']),
                          int(comic_json['month']), int(comic_json['day']), comic_json['img'])
            self.latest = max(self.latest, comic.number)
            return comic

    @group()
    @checks.bot_permissions('embed_links')
    async def xkcd(self, ctx: VioletContext, number: int = None):
        """ Show comics from xkcd, a popular webcomic.
            Pass a comic number to show a particular comic, or no arguments to show the latest.
            """
        comic = await self._get_comic(number)
        if comic is None:
            raise BadArgument("That comic doesn't exist")
        await self._show_comic(ctx, comic)

    xkcd.example_usage = """
    `{prefix}xkcd`: show the latest xkcd
    `{prefix}xkcd 327`: show xkcd #327, Exploits of a Mom
    """

    @xkcd.command('random')
    @checks.bot_permissions('embed_links')
    async def xkcd_random(self, ctx: VioletContext):
        """Show a random xkcd comic."""
        if self.latest == 0:
            await self._get_comic(None)

        await self._show_comic(ctx, await self._get_comic(random.randint(1, self.latest)))

    xkcd_random.example_usage = """
    `{prefix}xkcd random`: show a random xkcd
    """

    async def _show_comic(self, ctx: VioletContext, comic: Comic):
        embed = discord.Embed(color=self.xkcd_color, title=f'xkcd #{comic.number}: {comic.title}',
                              url=f'https://xkcd.com/{comic.number}',
                              description=f'{comic.year:04}-{comic.month:02}-{comic.day:02}\n{comic.alt_text}')
        embed.set_image(url=comic.image_url)
        await ctx.send(embed=embed)
