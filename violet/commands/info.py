# coding=utf-8
import collections
import colorsys
import functools
import inspect
import math
import string
import sys
import typing
import unicodedata
from difflib import SequenceMatcher

import discord
from discord.ext.commands import BadArgument, BadUnionArgument

from ..violetlib import checks, convert, format
from ..violetlib.coglocals import *
from ..violetlib.iterutils import segment


class Information(Cog):
    datetime_format = '%Y-%m-%d %H:%M:%S UTC'
    named_colors: typing.Sequence[typing.Tuple[str, discord.Color]]
    color_lookup: typing.Mapping[str, discord.Color]
    permission_bits: typing.Mapping[str, int]
    abc_emoji = [unicodedata.lookup(f'REGIONAL INDICATOR SYMBOL LETTER {letter}') for letter in string.ascii_uppercase]

    def __init__(self, bot: Violet, config):
        super().__init__(bot, config)
        named_colors = []
        for name in dir(discord.Color):
            attr = getattr(discord.Color, name)
            if inspect.ismethod(attr) and not inspect.signature(attr).parameters:
                named_colors.append((name, attr()))
        self.named_colors = sorted([(' '.join(name.split('_')).title(), color) for name, color in named_colors],
                                   key=lambda tup: tup[0])
        self.color_lookup = dict(self.named_colors)

        perms = discord.Permissions()
        bits = {}
        for name, _ in perms:
            setattr(perms, name, True)
            bits[name] = perms.value
            setattr(perms, name, False)
        self.permission_bits = collections.OrderedDict(sorted(bits.items(), key=lambda tup: tup[1]))  # Sort by bitflag

    @command(aliases=['botinfo', 'about'])
    @checks.bot_permissions('embed_links')
    async def info(self, ctx: VioletContext):
        """Show information about me."""
        embed = discord.Embed(title=f"Hello! I'm {ctx.me.name}!", color=ctx.default_color)
        embed.set_footer(text=str(ctx.me), icon_url=ctx.me.avatar_url_as(format='png', size=64))

        owner = ctx.bot.get_user(ctx.bot.owner_id)
        embed.description = f'{len(set(ctx.bot.walk_commands()))} commands\nAccount owner: {owner}'

        embed.add_field(name='Useful Commands', value=f'Show my prefixes in this guild: `{ctx.clean_prefix}prefix`\n'
                                                      f'List my commands: `{ctx.clean_prefix}help`\n'
                                                      f'Invite me to your guild: `{ctx.clean_prefix}invite`')

        embed.add_field(name='Environment',
                        value='Python v{}\ndiscord.py v{}'.format(sys.version.partition(' ')[0], discord.__version__))
        channel_count = sum(len(guild.channels) for guild in ctx.bot.guilds)
        text_count = sum(len(guild.text_channels) for guild in ctx.bot.guilds)
        voice_count = sum(len(guild.voice_channels) for guild in ctx.bot.guilds)
        # embed.add_field(name='Channels', value=f'{channel_count} total\n{text_count} text\n{voice_count} voice')
        human_count = sum(1 for user in ctx.bot.users if not user.bot)
        embed.add_field(name='Stats', value=f'{len(ctx.bot.guilds)} guilds\n'
                                            f'{len(ctx.bot.users)} users ({human_count} non-bot)\n'
                                            f'{channel_count} channels ({text_count} text / {voice_count} voice)')

        await ctx.send(embed=embed)

    info.example_usage = """
    `{prefix}info`: show information about me
    """

    @command(aliases=['colour', 'colorinfo', 'colourinfo'])
    @checks.bot_permissions('embed_links')
    async def color(self, ctx: VioletContext, *,
                    color: typing.Union[discord.Role, discord.Member, discord.Color, str] = None):
        """ Display a color by name or hex code.
            Three-digit and six-digit hex codes, named colors, members, and roles (will use their color) are allowed.
            To see what names are available, run this command with no arguments."""
        if color is None:  # List
            await self._list_colors(ctx)
            return

        if isinstance(color, (discord.Role, discord.Member)):
            color = color.color
        elif isinstance(color, str):
            color = self.color_lookup.get(color.casefold())
            if color is None:
                raise BadArgument('That color does not exist.')

        embed = discord.Embed(color=color, title=str(color).upper())
        embed.add_field(name='RGB', value=f'{color.r}, {color.g}, {color.b}')

        h, s, v = colorsys.rgb_to_hsv(color.r, color.g, color.b)
        embed.add_field(name='HSV', value=f'{int(h * 360)}deg, {int(s * 100)}%, {int(v / 255 * 100)}%')

        # CMYK
        k = 1 - max(color.r, color.g, color.b) / 255
        c, m, y = [(1 - comp / 255 - k) / (1 - k) if k < 1 else 0 for comp in (color.r, color.g, color.b)]
        embed.add_field(name='CMYK', value=f'{int(c * 100)}%, {int(m * 100)}%, {int(y * 100)}%, {int(k * 100)}%')
        await ctx.send(embed=embed)

    color.example_usage = """
    `{prefix}color`: list all available named colors
    `{prefix}color blurple`: show the color named blurple
    `{prefix}color #7289DA`: show the color with the given value
    `{prefix}color {ctx.author}`: show your color
    """

    @color.error
    async def color_error(self, ctx: VioletContext, exc: BaseException):
        if isinstance(exc, (BadArgument, BadUnionArgument)):
            await ctx.bot.get_cog('Events').display_error(ctx, BadArgument('That color does not exist'))
        else:
            await ctx.bot.get_cog('Events').display_error(ctx, exc)

    async def _list_colors(self, ctx):
        num_colors = len(self.named_colors)  # Number of colors without padding
        page_size = 6
        emoji = self.abc_emoji[:page_size]

        colors = list(self.named_colors)
        if num_colors % page_size:  # Pad to even multiple of page size, to prevent weird embed field misalignments
            colors += [('Default', discord.Color(0))] * (page_size - num_colors % page_size)

        pages: typing.List[discord.Embed] = []
        color_pages = segment(colors, page_size)
        for page_num, page in enumerate(color_pages):
            page_start, page_end = page_num * page_size + 1, min((page_num + 1) * page_size, num_colors)
            embed = discord.Embed(color=page[0][1], title=f'Colors {page_start}-{page_end} of {num_colors}')
            embed.set_footer(text=f'Currently showing {page[0][0]}')
            for i, (name, color) in enumerate(page):
                embed.add_field(name=f'{string.ascii_uppercase[i]}: {name}', value=str(color).upper())
            pages.append(embed)

        async with ctx.paginate(pages, reactions=[...] + emoji) as paginator:
            async for reaction, user in paginator:
                if reaction.emoji not in emoji:
                    continue
                idx = emoji.index(reaction.emoji)
                for embed, page in zip(pages, color_pages):
                    embed.color = page[idx][1]
                    embed.set_footer(text=f'Currently showing {page[idx][0]}')
                await paginator.next(0)  # Update the page

    @command('member', aliases=['user', 'memberinfo', 'userinfo'])
    @checks.guild_only()
    @checks.bot_permissions('embed_links')
    async def member_info(self, ctx: VioletContext, *, member: discord.Member = None):
        """ Show information on a member of this guild.
            If no member is specified, your information will be shown."""
        if member is None:
            member = ctx.author

        icon_url = member.avatar_url_as(static_format='png')

        embed = discord.Embed(title=member.display_name, description=f'{member!s} ({member.id})', color=member.color)
        embed.add_field(name='Bot Created' if member.bot else 'Account Created',
                        value=member.created_at.strftime(self.datetime_format), inline=True)
        embed.add_field(name='Member Joined', value=member.joined_at.strftime(self.datetime_format), inline=True)
        if member.premium_since is not None:
            embed.add_field(name='Member Boosted', value=member.premium_since.strftime(self.datetime_format), inline=True)
        embed.add_field(name='Color', value=str(member.color).upper(), inline=True)

        status = 'DND' if member.status is discord.Status.dnd else member.status.name.title()
        if member.status is not discord.Status.offline:
            platforms = format.pluralize([platform for platform in ('web', 'desktop', 'mobile') if
                                          getattr(member, f'{platform}_status') is not discord.Status.offline])
            status = f'{status} on {platforms}'
        activities = '\n'.join(self._format_activities(member.activities))
        embed.add_field(name='Status and Activity', value=f'{status}\n{activities}', inline=True)

        embed.add_field(name='Roles', value=', '.join(role.name for role in member.roles[:0:-1]) or 'None')
        embed.add_field(name='Icon URL', value=icon_url)
        embed.set_thumbnail(url=icon_url)
        await ctx.send(embed=embed)

    member_info.example_usage = """
    `{prefix}member`: show your member info
    `{prefix}member {ctx.me}`: show my member info
    """

    @staticmethod
    def _format_activities(activities: typing.Sequence[discord.Activity]) -> typing.List[str]:
        if not activities:
            return []

        def format_activity(activity: discord.Activity) -> str:
            if isinstance(activity, discord.CustomActivity):
                return f"{activity.emoji} {activity.name}"
            elif isinstance(activity, discord.Spotify):
                return f'Listening to {activity.title} by {activity.artist} on Spotify'
            elif activity.type is discord.ActivityType.listening:
                return f'Listening to {activity.name}'  # Special-cased to insert " to"
            else:
                return f'{activity.type.name.capitalize()} {activity.name}'

        # Some games show up twice in the list (e.g. "Rainbow Six Siege" and "Tom Clancy's Rainbow Six Siege") so we
        # need to dedup them by string similarity before displaying them
        matcher = SequenceMatcher(lambda c: not c.isalnum(), autojunk=False)
        filtered = [activities[0]]
        for activity in activities[1:]:
            matcher.set_seq2(activity.name)  # Expensive metadata is computed about seq2, so change it less frequently
            for filtered_activity in filtered:
                matcher.set_seq1(filtered_activity.name)
                if matcher.quick_ratio() < 0.6 and matcher.ratio() < 0.6:  # Use quick_ratio if we can as ratio is slow
                    filtered.append(activity)
                    break

        return [format_activity(activity) for activity in filtered]

    @group(aliases=['roleinfo'])
    @checks.guild_only()
    @checks.bot_permissions('embed_links')
    async def role(self, ctx: VioletContext, *, role: discord.Role):
        """ Show information on a role in this guild.
            This is an alias for `role info`. For information on a role named "info" or "members", use that command."""
        await ctx.invoke(self.role_info, role=role)

    role.example_usage = """
    `{prefix}role Members`: show information about the role named "Members"
    `{prefix}role info members`: show information about the role named "members"\
    (`{prefix}role members` is a separate command)
    """

    @role.command('info')
    @checks.guild_only()
    @checks.bot_permissions('embed_links')
    async def role_info(self, ctx: VioletContext, *, role: discord.Role):
        """Show information on a role in this guild."""
        embed = discord.Embed(title=role.name, description=role.id, color=role.color)
        embed.add_field(name='Role created', value=role.created_at.strftime(self.datetime_format), inline=True)
        embed.add_field(name='Color', value=str(role.color).upper(), inline=True)
        embed.add_field(name='Properties', value=f'{"Hoisted" if role.hoist else "Unhoisted"}, '
        f'{"mentionable" if role.mentionable else "unmentionable"}',
                        inline=True)
        embed.add_field(name='Members',
                        value=f'{len(role.members)} (use `{ctx.clean_prefix}role members {role.name}` to show them)',
                        inline=True)
        await ctx.send(embed=embed)

    role_info.example_usage = """
    `{prefix}role info Members`: show information about the role named "Members"
    """

    @role.command('members')
    @checks.guild_only()
    @checks.bot_permissions('embed_links')
    async def role_members(self, ctx: VioletContext, *, role: discord.Role):
        """Show the members of a role in this guild."""
        pages = []
        for i, members in enumerate(segment(sorted(role.members, key=lambda mem: mem.display_name.casefold()), 20)):
            page = discord.Embed(title=f'Members of {role.name}', color=role.color)
            page.description = '\n'.join(member.mention for member in members)
            page.set_footer(text=f'Page {i + 1} of {math.ceil(len(role.members) / 20)}')
            pages.append(page)

        if len(pages) == 1:
            await ctx.send(embed=pages[0])
        elif pages:
            await ctx.paginate(pages)
        else:
            await ctx.send('This role has no members.')

    role_members.example_usage = """
    `{prefix}role info Members`: list the members of the role named "Members"
    """

    @command(aliases=['server', 'guildinfo', 'serverinfo'])
    @checks.guild_only()
    @checks.bot_permissions('embed_links')
    async def guild(self, ctx: VioletContext):
        """Show information about this guild."""
        icon_url = ctx.guild.icon_url_as(static_format='png')
        static_emoji = sum(not e.animated for e in ctx.guild.emojis)
        animated_emoji = sum(e.animated for e in ctx.guild.emojis)
        embed = discord.Embed(color=ctx.color, title=f'{ctx.guild.name} ({ctx.guild.id})')
        embed.description = (f'{ctx.guild.member_count} members\n'
                             f'{len(ctx.guild.channels)} channels\n'
                             f'{len(ctx.guild.roles) - 1} roles\n'  # Exclude default role
                             f'{static_emoji}/{ctx.guild.emoji_limit} static emoji, '
                             f'{animated_emoji}/{ctx.guild.emoji_limit} animated\n' +
                             ''.join(str(e) for e in sorted(ctx.guild.emojis, key=lambda e: (e.animated, e.name))))
        embed.add_field(name='Created at', value=ctx.guild.created_at.strftime(self.datetime_format))
        embed.add_field(name='Owner', value=ctx.guild.owner)
        embed.add_field(name='Region', value=ctx.guild.region.name)
        embed.add_field(name='Nitro Boost', value=f'Level {ctx.guild.premium_tier}, '
                                                  f'{ctx.guild.premium_subscription_count} booster(s)\n'
                                                  f'{ctx.guild.filesize_limit // 1024**2}MiB files, '
                                                  f'{ctx.guild.bitrate_limit / 1000:0.1f}kbps voice')
        embed.add_field(name='Icon URL', value=icon_url or 'This guild has no icon', inline=False)
        embed.set_thumbnail(url=icon_url)
        await ctx.send(embed=embed)

    guild.example_usage = """
    `{prefix}guild`: show information about this guild
    """

    @command(aliases=['emote', 'emojiinfo', 'emojinfo', 'emoteinfo'])
    @checks.bot_permissions('embed_links')
    async def emoji(self, ctx: VioletContext, emoji: discord.Emoji):
        """ Show information about a custom emoji.
            You can use an emoji, or specify it by name or ID.
            For builtin emojis, use the charinfo command."""
        image_url = emoji.url
        description = '{} emoji, currently {}'.format('Animated' if emoji.animated else 'Static',
                                                      'available' if emoji.available else 'unavailable')
        embed = discord.Embed(title=f':{emoji.name}: ({emoji.id})', description=description)
        embed.add_field(name='Created at', value=emoji.created_at.strftime(self.datetime_format))
        embed.add_field(name='Image URL', value=image_url)
        embed.set_thumbnail(url=image_url)
        await ctx.send(embed=embed)

    # Any example usage would require knowing what emoji the bot has access to ahead of time

    @command(aliases=['char', 'chars'])
    async def charinfo(self, ctx: VioletContext, *, chars: str):
        if len(chars) > 25:
            raise BadArgument(f'You must specify at most 25 characters')
        await ctx.send('\n'.join(f'0x{ord(c):>06X} {{}}' for c in chars),
                       *[unicodedata.name(c, 'UNKNOWN') for c in chars])

    @command(aliases=['permission', 'perms'])
    async def permissions(self, ctx: VioletContext, *permissions: convert.Permissions):
        """ Compute Discord permissions, by name or integer value.
            This can be used, for example, to inspect or create permissions for bot invite links.
            All provided values will be combined. Each permission's value, and the sum, are shown.
            """
        if permissions:
            combined = discord.Permissions(functools.reduce(int.__or__, (perms.value for perms in permissions)))
        else:
            combined = discord.Permissions.all()  # Default to list all
        pages = segment([(name.replace('_', ' ').title(), value) for name, value in self.permission_bits.items()
                         if getattr(combined, name)], 10)
        embeds = [discord.Embed(color=ctx.color,
                                title=f'Permissions breakdown: {combined.value} (0x{combined.value:X})') for _ in pages]
        for i, (page, embed) in enumerate(zip(pages, embeds), 1):
            embed.description = '\n'.join(f'{name}: {value} (0x{value:X})' for name, value in page)
            embed.set_footer(text=f'Page {i} of {len(pages)}')
        if len(embeds) == 1:
            embeds[0].set_footer(text='')  # No page information when there's only 1 page
            await ctx.send(embed=embeds[0])
        else:
            await ctx.paginate(embeds)

    permissions.example_usage = """
    `{prefix}permissions`: list all permissions and their integer values
    `{prefix}permissions 268446726`: list the permissions given by the value 268,446,726
    `{prefix}permissions read_messages send_messages add_reactions`: get the integer value of the listed permissions
    `{prefix}permissions "Read Messages" "Send Messages" "Add Reactions"`: get the same value using different formatting
    `{prefix}permissions 3136 36701184 kick_members ban_members`: combine permissions into a new value
    """
