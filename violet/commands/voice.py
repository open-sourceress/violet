# coding=utf-8
import typing

import discord
from discord.ext.commands import BadArgument

from ..violetlib import checks, convert
from ..violetlib.coglocals import *


class Voice(Cog):
    voice_roles: typing.MutableMapping[int, typing.Set[int]]  # Voice channel ID -> role IDs

    def __init__(self, bot: Violet, config) -> None:
        super().__init__(bot, config)
        self.voice_roles = {}

    async def setup_db(self) -> None:
        async with self.acquire_db() as conn:
            await conn.execute('CREATE TABLE IF NOT EXISTS voice_roles (guild_id int8 NOT NULL, voice_id int8 NOT NULL,'
                               'role_id int8 NOT NULL, UNIQUE (voice_id, role_id));')

            self.voice_roles.clear()
            for channel_id, role_id in (await conn.fetch('SELECT voice_id, role_id FROM voice_roles;')):
                self.voice_roles.setdefault(channel_id, set()).add(role_id)

    @Cog.listener()
    async def on_ready(self):
        async with self.acquire_db() as conn:
            missing_guilds = [guild_id for (guild_id,) in await conn.fetch('SELECT DISTINCT guild_id FROM voice_roles;')
                              if self.bot.get_guild(guild_id) is None]
            if missing_guilds:
                self.logger.debug('Removing missing guilds: %r', missing_guilds)
                for (channel_id,) in await conn.fetch('DELETE FROM voice_roles WHERE guild_id = ANY($1)'
                                                      'RETURNING voice_id;', missing_guilds):
                    del self.voice_roles[channel_id]
            else:
                self.logger.debug('No missing guilds')

            missing_channels = {channel_id for guild_id, channel_id in
                                await conn.fetch('SELECT DISTINCT guild_id, voice_id FROM voice_roles;')
                                if self.bot.get_guild(guild_id).get_channel(channel_id) is None}
            if missing_channels:
                self.logger.debug('Removing missing voice channels: %r', missing_channels)
                for channel_id in missing_channels:
                    del self.voice_roles[channel_id]
                await conn.executemany('DELETE FROM voice_roles WHERE voice_id = $1;',
                                       [(channel_id,) for channel_id in missing_channels])
            else:
                self.logger.debug('No missing voice channels')

            missing_roles = {role_id for guild_id, role_id in
                             await conn.fetch('SELECT DISTINCT guild_id, role_id FROM voice_roles;')
                             if self.bot.get_guild(guild_id).get_role(role_id) is None}
            if missing_roles:
                self.logger.debug('Removing missing roles: %r', missing_roles)
                for (channel_id, role_id) in await conn.fetch('DELETE FROM voice_roles WHERE role_id = ANY($1)'
                                                              'RETURNING voice_id, role_id;', missing_roles):
                    self.voice_roles[channel_id].discard(role_id)
            else:
                self.logger.debug('No missing roles')

    @Cog.listener()
    async def on_voice_state_update(self, member: discord.Member, before: discord.VoiceState,
                                    after: discord.VoiceState) -> None:
        if before.channel is not None and after.channel is not None and before.channel.id == after.channel.id:
            return  # They didn't move voice channels, no reason to check
        if not member.guild.me.guild_permissions.manage_roles:
            return  # Can't give/take roles if there are any
        self.logger.debug('Voice-role update (user_id=%r, guild_id=%r, before_id=%r, after_id=%r)', member.id,
                          member.guild.id, getattr(before.channel, 'id', None), getattr(after.channel, 'id', None))
        new_roles = set(member.roles)
        if before.channel is not None:  # Left a channel, check for roles to take
            to_take = {member.guild.get_role(role_id) for role_id in self.voice_roles.get(before.channel.id, [])}
            to_take.discard(None)  # In case an ID of a deleted role ended up in the list
            can_take = {role for role in to_take if role < member.guild.me.top_role}
            if len(can_take) < len(to_take):
                self.logger.debug('Failed to take roles: %r', [role.id for role in to_take - can_take])
            self.logger.debug('Roles to take: %r', [role.id for role in can_take])
            new_roles -= can_take
        if after.channel is not None:  # Joined a channel, check for roles to give
            to_give = {member.guild.get_role(role_id) for role_id in self.voice_roles.get(after.channel.id, [])}
            to_give.discard(None)
            can_give = {role for role in to_give if role < member.guild.me.top_role}
            if len(can_give) < len(to_give):
                self.logger.debug('Failed to give roles: %r', [role.id for role in to_give - can_give])
            self.logger.debug('Roles to give: %r', [role.id for role in can_give])
            new_roles |= can_give
        if set(member.roles) != new_roles:
            self.logger.debug('Updating roles: %r', [role.id for role in new_roles])
            await member.edit(roles=new_roles, reason='Voice-role updates')
        else:
            self.logger.debug('No roles changed, skipping update')

    @Cog.listener()
    async def on_guild_remove(self, guild: discord.Guild):
        async with self.acquire_db() as conn:
            unlinked_channels = [channel_id for (channel_id,) in
                                 await conn.fetch('DELETE FROM voice_roles WHERE guild_id = $1 RETURNING voice_id;',
                                                  guild.id)]
        self.logger.debug('Unlinking channels from removed guild %r: %r', guild.id, unlinked_channels)
        for channel_id in unlinked_channels:
            del self.voice_roles[channel_id]

    @Cog.listener()
    async def on_guild_role_delete(self, role: discord.Role):
        async with self.acquire_db() as conn:
            unlinked_channels = [channel_id for (channel_id,) in
                                 await conn.fetch('DELETE FROM voice_roles WHERE role_id = $1 RETURNING voice_id;',
                                                  role.id)]
        self.logger.debug('Unlinking deleted role %r from channels: %r', role.id, unlinked_channels)
        for channel_id in unlinked_channels:
            roles = self.voice_roles.get(channel_id)
            if roles is not None:
                roles.discard(role.id)

    @Cog.listener()
    async def on_guild_channel_delete(self, channel: discord.abc.GuildChannel):
        if not isinstance(channel, discord.VoiceChannel):
            return
        async with self.acquire_db() as conn:
            unlinked_roles = await conn.fetch('DELETE FROM voice_roles WHERE voice_id = $1 RETURNING role_id;',
                                              channel.id)
        role_ids = [role_id for (role_id,) in unlinked_roles]
        self.logger.debug('Unlinking deleted channel %r from roles: %r', channel.id, role_ids)
        self.voice_roles.pop(channel.id, None)  # Delete or pass

    @group(aliases=['voiceroles'])
    @checks.guild_only()
    @checks.bot_permissions('embed_links')
    async def voicerole(self, ctx: VioletContext) -> None:
        """List and manage voice roles in this guild."""
        bindings = [(channel, [ctx.guild.get_role(role_id) for role_id in self.voice_roles[channel.id]]) for channel in
                    ctx.guild.voice_channels if self.voice_roles.get(channel.id)]  # Discard missing and empty sets
        embed = discord.Embed(color=ctx.color, title=f'Voice roles in {ctx.guild}')
        for channel, roles in sorted(bindings, key=lambda tup: tup[0].position):
            roles.sort(key=lambda role: role.name)
            embed.add_field(name=f'{channel.name} ({channel.id})',
                            value='\n'.join(f'{role.name} ({role.id})' for role in roles), inline=False)
        await ctx.send(embed=embed)

    voicerole.example_usage = """
    `{prefix}voiceroles`: list the voice roles in this guild, organized by channel name
    """

    @voicerole.command('link', aliases=['bind'])
    @checks.guild_only()
    @checks.bot_permissions('manage_roles')
    @checks.author_permissions('manage_roles')
    async def voicerole_link(self, ctx: VioletContext, voice_channel: discord.VoiceChannel, *,
                             role: convert.UsableRole):
        """ Link a role to a voice channel.
            This role will be given whenever a member enters the voice channel and taken when they leave.
            The role must be lower than my top role.
            """
        role_ids = self.voice_roles.setdefault(voice_channel.id, set())
        if role.id in role_ids:
            raise BadArgument('This role is already linked to this channel')

        role_ids.add(role.id)
        async with self.acquire_db() as conn:
            await conn.execute('INSERT INTO voice_roles (guild_id, voice_id, role_id) VALUES ($1, $2, $3);',
                               ctx.guild.id, voice_channel.id, role.id)
        await ctx.send('Role linked.')

    voicerole_link.example_usage = """
    `{prefix}voicerole link General Voice`: Link a role named "Voice" to the voice channel named "General"
    `{prefix}voicerole link "Voice #1" Voice 1`: Link a role named "Voice 1" to the voice channel named "Voice #1" 
    """

    @voicerole.command('unlink', aliases=['unbind'])
    @checks.guild_only()
    @checks.author_permissions('manage_roles')
    async def voicerole_unlink(self, ctx: VioletContext, voice_channel: discord.VoiceChannel, *, role: discord.Role):
        """ Unlink a role from a voice channel.
            The role won't be given to members as they join the channel or taken as they leave anymore.
            """
        role_ids = self.voice_roles.get(voice_channel.id, set())
        if role.id not in role_ids:
            raise BadArgument("This role isn't linked to this channel")
        role_ids.remove(role.id)
        async with self.acquire_db() as conn:
            await conn.execute('DELETE FROM voice_roles WHERE voice_id = $1 AND role_id = $2;',
                               voice_channel.id, role.id)
        await ctx.send('Role unlinked.')

    voicerole_unlink.example_usage = """
    `{prefix}voicerole unlink General Voice`: Unlink a role named "Voice" from the voice channel named "General"
    `{prefix}voicerole unlink "Voice #1" Voice 1`: Unlink a role named "Voice 1" from the voice channel named "Voice #1" 
    """
