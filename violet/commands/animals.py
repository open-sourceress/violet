# coding=utf-8
import asyncio
import sys
import typing
from datetime import datetime, timedelta

import aiohttp

from ..violetlib import VioletCommand, checks
from ..violetlib.coglocals import *
from ..violetlib.pagination import Reactor


def animal_command(subreddit: str, name: str, *aliases: str) -> VioletCommand:
    reload_emoji = '\N{CLOCKWISE RIGHTWARDS AND LEFTWARDS OPEN CIRCLE ARROWS}'  # :repeat:
    stop_emoji = '\N{BLACK SQUARE FOR STOP}'  # :stop_button:
    urls: typing.List[str] = []
    index = 0
    last_update: datetime = datetime.min

    async def next_url(config: 'AnimalsConfig', session: aiohttp.ClientSession) -> str:
        update_period = timedelta(minutes=config.update_period)
        nonlocal urls, index, last_update
        if last_update + update_period < datetime.utcnow():
            async with session.get(f'https://www.reddit.com/r/{subreddit}/hot.json?limit=100',
                                   headers={'User-Agent': f'{sys.platform}:violet:0.1.0'}) as resp:
                resp.raise_for_status()
                # reddit "thing" (contains ['data'] and ['type']) that wraps list of things that wrap posts
                posts = [thing['data'] for thing in (await resp.json())['data']['children']]
                urls = [post['url'] for post in posts if
                        post.get('post_hint') == 'image' and not post['spoiler'] and not post['over_18']]
                index = 0
                last_update = datetime.utcnow()
        ret = urls[index]
        index = (index + 1) % len(urls)
        return ret

    @command(name, aliases=aliases,
             help=f'Show a random cute {name} from /r/{subreddit}.\nUse the reactions to get new images without spam.')
    @checks.bot_permissions('embed_links')
    async def command_body(self, ctx: VioletContext):
        nonlocal urls, index, last_update
        async with ctx.typing():
            url = await next_url(self.config, self.bot.http_session)

        message = await ctx.send('{}', url)
        if ctx.bot_permissions.add_reactions and ctx.bot_permissions.read_message_history:
            async with Reactor(self.bot, message, [reload_emoji, stop_emoji], timeout=60) as reactor:
                async for reaction, user in reactor:
                    if user.bot:
                        continue
                    if reaction.emoji == reload_emoji:
                        url = await next_url(self.config, self.bot.http_session)
                        await message.edit(content=url)
                    elif reaction.emoji == stop_emoji:
                        break
        else:
            await asyncio.sleep(60)
        await message.edit(content=f'<{url}>')  # Hide preview to reduce spam

    command_body.example_usage = f"""
    `{{prefix}}{name}`: show a random {name}
    """

    return command_body


class Animals(Cog):
    config: 'AnimalsConfig'
    _commands = {
        'batty': ('bat', 'bats'),
        'cats': ('cat', 'kitty', 'kitten', 'catstop', 'cats', 'kitties', 'kittens', 'catstops'),
        'cows': ('cow', 'cows', *('m' + 'o' * i for i in range(2, 11))),
        'dogpictures': ('dog', 'puppy', 'doggo', 'dogs', 'puppies', 'doggos'),
        'ducklings': ('duck', 'duckling', 'ducks', 'ducklings', 'quack'),
        'goats': ('goat', 'goats'),
        'hamsters': ('hamster', 'hamsters'),
        'Otters': ('otter', 'otters'),
        'pandas': ('panda', 'pandas'),
        'Parakeets': ('parakeet', 'parakeets'),
        'seals': ('seal', 'seals'),
        'sloths': ('sloth', 'sloths'),
        'turtles': ('turtle', 'turtles'),
    }
    subreddit = names = None  # satisfy linter that these names always exist
    for subreddit, names in _commands.items():
        locals()[names[0]] = animal_command(subreddit, *names)
    del subreddit, names, _commands


class AnimalsConfig(Config):
    update_period: float = 8.0 * 60  # How often, in minutes, to fetch new images
