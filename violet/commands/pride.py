# coding=utf-8
import enum
import inspect
import io
import itertools
import operator as op
import typing

import discord
from PIL import Image, ImageDraw
from discord.ext.commands import BadArgument, Converter, MissingRequiredArgument, MemberConverter

from ..violetlib import VioletCommand, checks
from ..violetlib.coglocals import *


class ImageURL(Converter):
    member_conv = MemberConverter()

    async def convert(self, ctx: VioletContext, argument: str) -> str:
        try:
            member: discord.Member = await self.member_conv.convert(ctx, argument)
        except BadArgument:
            return argument[1:-1] if argument.startswith('<') and argument.endswith('>') else argument
        else:
            return str(member.avatar_url_as(format='png'))


if typing.TYPE_CHECKING:
    ImageURL = str  # Output type for converter


def append_mirror(*colors: typing.Tuple[int, int, int]) -> typing.List[typing.Tuple[int, int, int]]:
    """Add the reversed list of colors to the end of itself, without duplicating the last item.

    This is used to create symmetrical pride flags without repeating the color values. For example, the transgender
    pride flag (blue, pink, white, pink, blue) can be created with `append_mirror(blue, pink, white)`. The white in the
    middle will not be duplicated; the output contains `white` only once.
    """
    return [*colors, *colors[1::-1]]  # Reverse, drop last (now first item), and concat


class PrideFlag(enum.Enum):
    """A pride flag and its color stripes.

    Values of this enum are lists of colors in (R, G, B) format. Items in these lists represent the stripes in each
    pride flag, drawn from top to bottom.
    Each item in the list should be drawn with equal height; for stripes with different-height stripes, e.g. the
    bisexual pride flag, the colors of the wider stripes are included consecutively multiple times to create a single
    visible stripe of the correct height.
    """

    rainbow = [(243, 28, 28), (255, 196, 0), (255, 247, 0), (0, 188, 108), (0, 149, 255), (181, 46, 193)]
    lgbt = lgbtq = locals()['lgbtq+'] = rainbow

    rainbow8 = [(255, 105, 180), *rainbow[0:4], (0, 192, 192), *rainbow[4:]]
    lgbt8 = lgbtq8 = locals()['lgbtq+8'] = rainbow8

    transgender = append_mirror((91, 206, 250), (245, 169, 184), (255, 255, 255))
    trans = transgender

    demiboy = append_mirror((127, 127, 127), (195, 195, 195), (153, 217, 234), (255, 255, 255))
    demimale = demiboy

    demigirl = append_mirror((127, 127, 127), (195, 195, 195), (255, 174, 201), (255, 255, 255))
    demifemale = demigirl

    agender = [(0, 0, 0), (186, 186, 186), (255, 255, 255), (182, 245, 131)]

    nonbinary = [(255, 244, 51), (255, 255, 255), (155, 89, 208), (45, 45, 45)]
    enby = nonbinary

    genderqueer = [(74, 129, 35), (255, 255, 255), (181, 126, 220)]
    gq = genderqueer

    genderfluid = [(255, 117, 162), (255, 255, 255), (190, 24, 214), (0, 0, 0), (51, 62, 189)]
    gf = genderfluid

    genderflux = [(244, 118, 148), (242, 162, 185), (206, 206, 206), (124, 224, 247), (62, 205, 249), (255, 244, 141)]

    lesbian = [(213, 45, 0), (255, 154, 86), (255, 255, 255), (211, 98, 164), (163, 2, 98)]
    lesbiab = lesbean = lesbian

    bisexual = [*[(215, 2, 112)] * 2, (115, 79, 150), *[(0, 56, 168)] * 2]  # Pink and blue stripes are twice as wide
    bi = bisexual

    pansexual = [(255, 33, 140), (255, 216, 0), (33, 177, 255)]
    pan = pansexual

    polysexual = [(246, 22, 186), (1, 214, 105), (21, 147, 246)]
    poly = polysexual

    asexual = [(0, 0, 0), (163, 163, 163), (255, 255, 255), (128, 0, 128)]
    ace = asexual

    aromantic = [(61, 165, 66), (167, 211, 121), (255, 255, 255), (169, 169, 169), (0, 0, 0)]
    aro = aromantic

    @classmethod
    async def convert(cls, ctx: VioletContext, arg: str) -> 'PrideFlag':
        try:
            return cls[arg.casefold()]
        except KeyError:
            raise BadArgument(f'Pride flag "{arg}" not found')


class Pride(Cog):
    @command()
    @checks.bot_permissions('attach_files', 'embed_links')
    async def pride(self, ctx: VioletContext, flag: PrideFlag, *, image: ImageURL = None):
        """Overlay a pride flag on an image.
        You can pass an image URL to use, a member (to use their avatar), or send an image as an attachment. If none
        of these are provided, your avatar will be used.
        To see what flags are available, run this command with no arguments.
        """
        if image is None:
            image = next((att.url for att in ctx.message.attachments if att.width),
                         str(ctx.author.avatar_url_as(format='png')))

        async with self.bot.http_session.get(image) as resp, ctx.typing():
            image_bytes: bytes = await resp.read()

        async with ctx.typing():
            buf: io.BytesIO = await self.bot.loop.run_in_executor(None, self._apply_pride_overlay, flag, image_bytes)

        await ctx.send(file=discord.File(buf, filename='pride.png'))

    pride.example_usage = """
    `{prefix}pride`: list available pride flags
    `{prefix}pride rainbow`: overlay the rainbow flag on your avatar
    `{prefix}pride bi {ctx.me}`: overlay the bisexual pride flag on my avatar
    `{prefix}pride rainbow8 http://placekitten.com/300/300`: overlay the original 8-stripe rainbow flag on a cat
    """

    @pride.error
    async def pride_error(self, ctx: VioletContext, err: Exception):
        if isinstance(err, MissingRequiredArgument):  # No pride flag specified, list them
            await self.send_flag_list(ctx)
        else:
            events = ctx.bot.get_cog('Events')
            await events.display_error(ctx, err)

    @command()
    @checks.bot_permissions('attach_files', 'embed_links')
    async def prideborder(self, ctx: VioletContext, flag: PrideFlag, *, image: ImageURL = None):
        """Add a circular pride flag border around an image.
        You can pass an image URL to use, a member (to use their avatar), or send an image as an attachment. If none
        of these are provided, your avatar will be used.
        To see what flags are available, run this command with no arguments.
        """
        if image is None:
            image = next((att.url for att in ctx.message.attachments if att.width),
                         str(ctx.author.avatar_url_as(format='png')))

        async with self.bot.http_session.get(image) as resp, ctx.typing():
            image_bytes: bytes = await resp.read()

        async with ctx.typing():
            buf: io.BytesIO = await self.bot.loop.run_in_executor(None, self._apply_pride_border, flag, image_bytes)

        await ctx.send(file=discord.File(buf, filename='pride.png'))

    prideborder.example_usage = """
    `{prefix}prideborder`: list available pride flags
    `{prefix}prideborder rainbow`: add a rainbow flag border to your avatar
    `{prefix}prideborder bi {ctx.me}`: add a bisexual flag border to my avatar
    `{prefix}prideborder rainbow8 http://placekitten.com/300/300`: add an 8-stripe rainbow flag border to a cat
    """

    @prideborder.error
    async def prideborder_error(self, ctx: VioletContext, err: Exception):
        if isinstance(err, MissingRequiredArgument):  # No pride flag specified, list them
            await self.send_flag_list(ctx)
        else:
            events = ctx.bot.get_cog('Events')
            await events.display_error(ctx, err)

    async def send_flag_list(self, ctx: VioletContext):
        embed = discord.Embed(title='Available pride flag filters', color=ctx.color)
        # __members__ contains all members and aliases in definition order ({name: enum object})
        # Since aliases are defined immediately after their aliased member, grouping by enum value results in each
        # name and its aliases being grouped together, name first
        grouped_flags = itertools.groupby(PrideFlag.__members__.items(), op.itemgetter(1))
        # [(enum, [(name, enum), (alias, enum), ...]), ...]

        # Get the name+aliases out of the group
        flag_names = [tuple(name for name, _ in group) for _, group in grouped_flags]

        # Sort alphabetically by name (case-sensitivity doesn't matter because they're all lowercase)
        flag_names.sort(key=op.itemgetter(0))

        # Present as "{name} ({alias}, {alias}...)" if there are aliases, just name if not
        lines = ('{} ({})'.format(name.capitalize(), ', '.join(sorted(aliases))) if aliases else name.capitalize()
                 for name, *aliases in flag_names)

        embed.description = '\n'.join(lines)
        await ctx.send(embed=embed)

    @staticmethod
    def _apply_pride_overlay(flag: PrideFlag, im_bytes: bytes) -> io.BytesIO:
        stripes = flag.value
        im: Image.Image = Image.open(io.BytesIO(im_bytes))
        im.load()
        im.convert(mode='RGB')
        stripe_height = im.height // len(stripes)
        mask = Image.new('L', (im.width, stripe_height), (123,))
        for i, stripe_color in enumerate(stripes):
            im.paste(stripe_color, (0, i * stripe_height), mask)
        buf = io.BytesIO()
        im.save(buf, 'png')
        buf.seek(0)
        return buf

    @staticmethod
    def _apply_pride_border(flag: PrideFlag, im_bytes: bytes) -> io.BytesIO:
        stripes = flag.value
        im: Image.Image = Image.open(io.BytesIO(im_bytes))
        im.load()
        im.convert(mode='RGB')

        flag_im = Image.new('RGBA', im.size, (0, 0, 0, 0))

        stripe_height = im.height // len(stripes)
        for i, stripe_color in enumerate(stripes):
            flag_im.paste(stripe_color, (0, i * stripe_height, im.width, (i + 1) * stripe_height))

        spacing_x, spacing_y = int(im.width * 0.07), int(im.height * 0.07)
        mask_im = Image.new('L', im.size, (0,))
        mask_canvas = ImageDraw.Draw(mask_im)
        mask_canvas.ellipse((spacing_x, spacing_y, im.width - spacing_x, im.height - spacing_y), fill=(255,))

        flag_im.paste(im, (0, 0), mask=mask_im)

        buf = io.BytesIO()
        flag_im.save(buf, 'png')
        buf.seek(0)
        return buf
