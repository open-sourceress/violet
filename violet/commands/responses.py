# coding=utf-8
import asyncio
import enum
import io
import random
import re
from typing import BinaryIO, MutableMapping, Optional, Sequence, Tuple, Type, TypeVar

import discord
from PIL import Image
from discord.ext.commands import BadArgument

from ..violetlib.coglocals import *

T = TypeVar('T', bound='AnyResponses')


class Responses(Cog, hidden=True):
    _apostrophes = re.escape(''.join("'\N{RIGHT SINGLE QUOTATION MARK}"))  # iOS smart quotes
    dadjoke_re = re.compile(rf"i([{_apostrophes}]?m|\s+am)\s+(.+)", re.DOTALL | re.IGNORECASE)
    long_animal_re = re.compile(r"l(o*)ng\s*(cat|snek|snake)", re.IGNORECASE)
    hat_re = re.compile(r'\bhat\s+hat\b', re.IGNORECASE)
    guild_settings: MutableMapping[int, 'GuildResponses']
    guild_defaults: 'GuildResponses'
    channel_settings: MutableMapping[int, 'ChannelResponses']
    channel_defaults: 'ChannelResponses'
    user_settings: MutableMapping[int, 'UserResponses']
    user_defaults: 'UserResponses'
    long_animal_images: MutableMapping[str, Tuple[Image.Image, Image.Image, Image.Image]]  # name => (tail, body, head)
    long_animal_emoji: MutableMapping[str, Sequence[discord.Emoji]]  # name => (tail, *body, head)

    hat_quotes = [
        'The hat takes their hat and delivers it to the Poofs!',
        'The hat enters the ball into play!',
        'Gimme my hat!',
        'I got my hat!',
        'Take the hat!',
        'Here comes the hat!',
        'Hat hat hat!',
        'The hat finds a way!',
        'Here comes 4525, the hat!',
        'The hat delivers us.',
        'The hat is perched, ready to shoot!',
        'Where\'s my hat? I gotta put on my hat. There it is! I feel better now.',
    ]

    def __init__(self, bot: Violet, config):
        super().__init__(bot, config)

        animals = ('cat', 'snake')
        parts = ('tail', 'body', 'head')
        self.long_animal_images = {}
        for animal in animals:
            images = []
            for part in parts:
                with open(self.asset_dir / f'long_{animal}_{part}.png', 'rb') as f:
                    im: Image.Image = Image.open(f)
                    im.load()  # Eagerly load so we don't wait for file I/O on the first use
                images.append(im)
            self.long_animal_images[animal] = tuple(images)
        self.long_animal_emoji = {}

    async def setup_db(self):
        self.guild_defaults = GuildResponses(*[ResponseSetting.disabled] * 3)
        self.channel_defaults = ChannelResponses(*[None] * 3)
        self.user_defaults = UserResponses(*[ResponseSetting.enabled_messages] * 3)
        async with self.acquire_db() as conn:
            await conn.execute('''
            DO $response_setting$ BEGIN
                CREATE TYPE response_setting AS ENUM ('disabled', 'enabled_reactions', 'enabled_messages');
            EXCEPTION WHEN duplicate_object THEN null;
            END $response_setting$;
            ''')  # CREATE TYPE IF NOT EXISTS
            await conn.execute("CREATE TABLE IF NOT EXISTS guild_responses (guild_id int8 PRIMARY KEY NOT NULL,"
                               "dad_jokes response_setting NOT NULL DEFAULT 'disabled',"
                               "long_cats response_setting NOT NULL DEFAULT 'disabled',"
                               "hats      response_setting NOT NULL DEFAULT 'disabled');")
            # DMs enable everything, so user settings have sole control
            await conn.execute("INSERT INTO guild_responses VALUES "
                               "(0, 'enabled_messages', 'enabled_messages','enabled_messages') ON CONFLICT DO NOTHING;")
            self.guild_settings = {guild_id: self._deserialize_responses(GuildResponses, settings)
                                   for guild_id, *settings in await conn.fetch('SELECT * FROM guild_responses;')}
            self.logger.debug('Loaded guild settings: %r', self.guild_settings)

            await conn.execute('CREATE TABLE IF NOT EXISTS channel_responses (channel_id int8 PRIMARY KEY NOT NULL,'
                               'dad_jokes response_setting DEFAULT NULL,'
                               'long_cats response_setting DEFAULT NULL,'
                               'hats      response_setting DEFAULT NULL);')
            self.channel_settings = {channel_id: self._deserialize_responses(ChannelResponses, settings)
                                     for channel_id, *settings in await conn.fetch('SELECT * FROM channel_responses;')}
            self.logger.debug('Loaded channel settings: %r', self.channel_settings)

            await conn.execute("CREATE TABLE IF NOT EXISTS user_responses (user_id int8 PRIMARY KEY NOT NULL,"
                               "dad_jokes response_setting NOT NULL DEFAULT 'enabled_messages',"
                               "long_cats response_setting NOT NULL DEFAULT 'enabled_messages',"
                               "hats      response_setting NOT NULL DEFAULT 'enabled_messages');")
            self.user_settings = {user_id: self._deserialize_responses(UserResponses, settings)
                                  for user_id, *settings in await conn.fetch('SELECT * FROM user_responses;')}
            self.logger.debug('Loaded user settings: %r', self.user_settings)

    @staticmethod
    def _deserialize_responses(cls: Type[T], record: Sequence[str]) -> T:
        return cls(*[None if value is None else ResponseSetting[value] for value in record])

    @Cog.listener()
    async def on_ready(self):
        for animal, (tail_im, body_im, head_im) in self.long_animal_images.items():
            self.long_animal_emoji[animal] = await asyncio.gather(
                self._create_emoji(f'long{animal}_tail', tail_im),
                *[self._create_emoji(f'long{animal}_body_{i}', body_im) for i in range(18)],
                self._create_emoji(f'long{animal}_head', head_im))

    async def _create_emoji(self, name: str, image: Image.Image):
        emoji = discord.utils.get(self.bot.home_guild.emojis, name=name)
        if emoji is not None:
            self.logger.debug('Found existing emoji %r for %r', emoji, name)
        else:
            buf = io.BytesIO()
            image.save(buf, 'png')
            buf.seek(0)
            emoji = await self.bot.home_guild.create_custom_emoji(name=name, image=buf.getvalue(),
                                                                  reason=f'Creating long-animal emoji {name}')
            self.logger.debug('Created emoji %r for %r', emoji, name)
        return emoji

    def get_settings(self, kind: str, target_id: int) -> 'AnyResponses':
        settings_map: MutableMapping[int, AnyResponses] = getattr(self, f'{kind}_settings')
        defaults: AnyResponses = getattr(self, f'{kind}_defaults')
        return settings_map.get(target_id, defaults)

    async def set_settings(self, kind: str, target_id: int, **settings: 'Optional[ResponseSetting]'):
        settings_map: MutableMapping[int, AnyResponses] = getattr(self, f'{kind}_settings')

        table = f'{kind}_responses'
        key_column = f'{kind}_id'
        columns, values = zip(*settings.items())  # (k, v), (k, v),... -> (k, k,...), (v, v,...)
        params = ['DEFAULT' if value is None else f'${i + 2}' for i, value in enumerate(values)]
        values = [i.name for i in values if i is not None]
        update = ', '.join(f'{col} = EXCLUDED.{col}' for col in columns)
        async with self.acquire_db() as conn:
            record = await conn.fetchrow(f'INSERT INTO {table} ({key_column}, {", ".join(columns)}) VALUES '
                                         f'($1, {", ".join(params)}) ON CONFLICT ({key_column}) DO UPDATE SET '
                                         f'{update} RETURNING {", ".join(AnyResponses.__slots__)};', target_id, *values)
        if kind == 'guild':
            cls = GuildResponses
        elif kind == 'channel':
            cls = ChannelResponses
        elif kind == 'user':
            cls = UserResponses
        else:
            cls = None  # Would have errored by now
        settings_map[target_id] = self._deserialize_responses(cls, record)

    @Cog.listener()
    async def on_context(self, ctx: VioletContext):
        if ctx.author.bot or not ctx.message.content:
            return
        guild = self.guild_settings.get(0 if ctx.guild is None else ctx.guild.id, self.guild_defaults)
        channel = self.channel_settings.get(ctx.channel.id, self.channel_defaults)
        user = self.user_settings.get(ctx.author.id, self.user_defaults)

        if await self._dad_joke(ctx, self._merge_settings(guild.dad_jokes, channel.dad_jokes, user.dad_jokes)):
            return
        if await self._long_animal(ctx, self._merge_settings(guild.long_cats, channel.long_cats, user.long_cats)):
            return
        if await self._hats(ctx, self._merge_settings(guild.hats, channel.hats, user.hats)):
            return

    def _merge_settings(self, guild_setting: 'ResponseSetting', channel_setting: Optional['ResponseSetting'],
                        user_setting: 'ResponseSetting') -> 'ResponseSetting':
        return min(user_setting, channel_setting or guild_setting)  # Channel overrides guild if it exists

    async def _dad_joke(self, ctx: VioletContext, setting: 'ResponseSetting') -> bool:
        if setting is ResponseSetting.disabled:
            return False

        match = self.dadjoke_re.match(ctx.message.content)
        if match is None:
            return False

        if setting is ResponseSetting.enabled_reactions:
            if ctx.bot_permissions.add_reactions:
                await ctx.message.add_reaction('\N{WAVING HAND SIGN}')
                return True
            else:
                return False

        if not ctx.bot_permissions.send_messages:
            return False
        subject = ' '.join(match.group(2).strip(',.?! \r\n\t').split()[:4])
        if not subject:  # Trimming spacing and punctuation removed the entire subject
            return False
        if subject.casefold() == 'violet':
            await ctx.send(":thinking: You're not {}, I'm Violet!", subject)
        else:
            await ctx.send("Hello **{}**, I'm {}!", subject, ctx.me.display_name)
        return True

    async def _long_animal(self, ctx: VioletContext, setting: 'ResponseSetting') -> bool:
        if setting is ResponseSetting.disabled:
            return False

        match = self.long_animal_re.search(ctx.message.content)
        if match is None:
            return False

        animal = match.group(2).casefold()  # first is o's, second is animal
        if animal == 'snek':
            animal = 'snake'

        if setting is ResponseSetting.enabled_reactions:
            if ctx.bot_permissions.add_reactions:
                tail, *body, head = self.long_animal_emoji[animal]
                for emoji in [tail, *body[:len(match.group(1))], head]:
                    await ctx.message.add_reaction(emoji)
                return True
            else:
                return False

        if not ctx.bot_permissions.send_messages or not ctx.bot_permissions.attach_files:
            return False
        im = await ctx.bot.loop.run_in_executor(None, self._create_long_animal, len(match.group(1)), animal)
        await ctx.send(file=discord.File(im, f'{match.group(0)}.png'))
        return True

    def _create_long_animal(self, length: int, animal: str) -> BinaryIO:
        tail, body, head = self.long_animal_images[animal]
        result: Image.Image = Image.new('RGBA', (tail.width + body.width * length + head.width, head.height))
        result.paste(tail, (0, 0))

        for i in range(length):
            result.paste(body, (tail.width + body.width * i, 0))

        result.paste(head, (tail.width + body.width * length, 0))

        dest = io.BytesIO()
        result.save(dest, 'png')
        dest.seek(0)
        return dest

    async def _hats(self, ctx: VioletContext, setting: 'ResponseSetting') -> bool:
        if setting is ResponseSetting.disabled:
            return False

        match = self.hat_re.search(ctx.message.content)
        if match is None:
            return False

        if setting is ResponseSetting.enabled_reactions:
            if ctx.bot_permissions.add_reactions:
                await ctx.message.add_reaction('\N{LARGE RED CIRCLE}')
                await ctx.message.add_reaction('\N{TOP HAT}')
                await ctx.message.add_reaction('\N{LARGE BLUE CIRCLE}')
                return True
            else:
                return False

        if not ctx.bot_permissions.send_messages:
            return False
        await ctx.send('{} <https://youtu.be/vyVkyakC6xk>', random.choice(self.hat_quotes))
        return True


class AnyResponses:
    __slots__ = ('dad_jokes', 'long_cats', 'hats')
    dad_jokes: 'ResponseSetting'
    long_cats: 'ResponseSetting'
    hats: 'ResponseSetting'

    def __init__(self, dad_jokes: 'ResponseSetting', long_cats: 'ResponseSetting', hats: 'ResponseSetting'):
        self.dad_jokes = dad_jokes
        self.long_cats = long_cats
        self.hats = hats

    def __copy__(self):
        return type(self)(self.dad_jokes, self.long_cats)


class GuildResponses(AnyResponses):
    __slots__ = ()


class ChannelResponses(AnyResponses):
    __slots__ = ()
    dad_jokes: Optional['ResponseSetting']
    long_cats: Optional['ResponseSetting']
    hats: Optional['ResponseSetting']


class UserResponses(AnyResponses):
    __slots__ = ()


class ResponseSetting(enum.IntEnum):
    disabled = enum.auto()
    enabled_reactions = enum.auto()
    enabled_messages = enum.auto()

    @classmethod
    async def convert(cls, ctx: VioletContext, argument: str) -> 'ResponseSetting':
        try:
            return cls[argument]
        except KeyError:
            raise BadArgument('Illegal setting value')
