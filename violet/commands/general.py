# coding=utf-8
import asyncio
import inspect
import time
import typing
from datetime import datetime, timezone

import discord
import pint
from discord.ext import commands
from discord.ext.commands import BadArgument, BadUnionArgument, GroupMixin, UserInputError

from . import events
from ..violetlib import Command, Group, VioletCommand, checks, cleaners, convert, format, iterutils
from ..violetlib.coglocals import *


class General(Cog):
    config: 'GeneralConfig'
    afk_users: typing.MutableMapping[int, typing.Tuple[str, bool, typing.Optional[datetime]]]
    units: pint.UnitRegistry

    def __init__(self, bot: Violet, config: 'GeneralConfig'):
        super().__init__(bot, config)
        self.afk_users = {}
        bot.help_command = help_cmd = VioletHelp()
        help_cmd.cog = self
        self.units = pint.UnitRegistry()

    async def setup_db(self):
        async with self.acquire_db() as conn:
            await conn.execute("CREATE TABLE IF NOT EXISTS afk_users (user_id int8 PRIMARY KEY NOT NULL,"
                               "reason varchar(2000) NOT NULL DEFAULT 'no reason provided',"
                               "sticky bool NOT NULL DEFAULT FALSE, return_at timestamptz NULL);")
            self.afk_users = dict(await conn.fetch('SELECT user_id, (reason, sticky, return_at) FROM afk_users;'))
            self.logger.debug('Initial AFK status loaded: %r', self.afk_users)

    @Cog.listener()
    async def on_context(self, ctx: VioletContext):
        if ctx.valid and ctx.command.root_parent is self.afk:
            # This includes subcommands, since those aren't detected until invoke() is called
            return

        if ctx.author.id in self.afk_users and not self.afk_users[ctx.author.id][1]:
            self.afk_users.pop(ctx.author.id)
            async with self.acquire_db() as conn:
                await conn.execute('DELETE FROM afk_users WHERE user_id = $1;', ctx.author.id)
            await ctx.send('{} is no longer AFK.', ctx.author, delete_after=20)

        mentioned_afk = {user.id for user in ctx.message.mentions}
        mentioned_afk.intersection_update(self.afk_users)
        if not mentioned_afk:
            return
        elif len(mentioned_afk) == 1:
            afk_id = mentioned_afk.pop()
            afk_settings = self.afk_users[afk_id]
            if afk_settings[2] is None or afk_settings[2] < datetime.utcnow().replace(tzinfo=timezone.utc):
                await ctx.send('{} is {}: {}.', ctx.get_member(afk_id), 'away' if afk_settings[1] else 'AFK',
                               afk_settings[0])
            else:
                await ctx.send('{} is {} ({}) and should return in {}.', ctx.get_member(afk_id),
                               'away' if afk_settings[1] else 'AFK', afk_settings[0],
                               format.timedelta(afk_settings[2] - datetime.utcnow().replace(tzinfo=timezone.utc)))
        else:
            afk_users = [user for user in ctx.message.mentions if user.id in mentioned_afk]
            if len(afk_users) == 2:
                await ctx.send('{} and {} are AFK.', afk_users[0], afk_users[1])
            else:
                await ctx.send(', '.join('{}' for _ in range(1, len(afk_users))) + ', and {} are AFK.', *afk_users)

    @command()
    async def ping(self, ctx: VioletContext) -> None:
        """Check my response time."""
        latency = int(ctx.bot.latency * 1000)
        start = time.perf_counter()
        await ctx.trigger_typing()
        delta = int((time.perf_counter() - start) * 1000)
        await ctx.send(':ping_pong: Pong! Latency={}ms. Round-trip={}ms.', latency, delta)

    ping.example_usage = """
    `{prefix}ping`: ping me and check my response time
    """

    @command()
    @checks.guild_only()
    @checks.bot_permissions('manage_nicknames')
    @checks.author_permissions('change_nickname')
    async def nick(self, ctx, *, nick: str = ''):
        """Set or remove your nickname."""
        if ctx.author.id == ctx.guild.owner.id:
            raise BadArgument("I can't change the guild owner's nickname")
        elif ctx.author.top_role >= ctx.me.top_role:
            raise BadArgument('Your top role must be below mine')
        elif len(nick) > 32:
            raise BadArgument('Your nickname must be 32 characters or shorter')
        else:
            await ctx.author.edit(nick=nick or None)
            await ctx.send('Nickname {}.', 'set' if nick else 'removed')

    nick.example_usage = """
    `{prefix}nick` - remove your nickname
    `{prefix}nick example nickname` - set your nickname to "example nickname"
    """

    @command(aliases=['git', 'github', 'gitlab', 'src'])
    async def source(self, ctx: VioletContext, *, target: typing.Union[VioletCommand, Cog] = None):
        """ Link to my source code online.
            If a command or cog name is provided, the link will jump directly to that command/cog."""
        if not self.config.git_url:
            raise BadArgument("The bot owner hasn't provided a remote URL")

        if isinstance(target, VioletCommand):
            lines, start = inspect.getsourcelines(target.callback)
            await ctx.send('<{}/blob/{}/{}.py#L{}-{}>', self.config.git_url, self.config.git_branch,
                           target.callback.__module__.replace('.', '/'), start, start + len(lines) - 1)
        elif isinstance(target, Cog):
            await ctx.send('<{}/blob/{}/{}.py>', self.config.git_url, self.config.git_branch,
                           target.__module__.replace('.', '/'))
        else:
            await ctx.send('<{}/tree/{}>', self.config.git_url, self.config.git_branch)

    source.example_usage = """
    `{prefix}source`: link to my Git repository, if any
    `{prefix}source source`: link to the source for this command
    `{prefix}source General`: link to the source of the General cog
    """

    @command('convert')  # `convert` as an attribute conflicts with converters
    async def convert_(self, ctx: VioletContext, quantity: float, source_unit: str, dest_unit: str) -> None:
        """ Convert a quantity from one unit to another.
            Compound units are allowed. Use `*` for multiplication, `/` for division, and `^` for exponents.
            For non-multiplicative elements (i.e. temperature), add `delta_` before the unit name to omit the offset.
            Pounds (lb) are defined as a unit of mass. The equivalent force (on Earth's surface) is 1 `force_pound`.
            """
        source_qty = self.units.Quantity(quantity, source_unit)
        dest_qty = source_qty.to(dest_unit)
        await ctx.send('{} converts to {}.', self._clean_units(f'{source_qty:.4}'), self._clean_units(f'{dest_qty:.4}'))

    convert_.example_usage = """
    `{prefix}convert 3 km mi`: convert 3 kilometers to miles
    `{prefix}convert 3 kilometers miles`: do the same conversion with long names
    `{prefix}convert 20 mph m/s`: convert a quantity of speed in miles per hour to meters per second
    `{prefix}convert 5 N*m ft*force_pound`: convert a quantity of torque in newton-meters to foot-pounds
    `{prefix}convert 1 N/m^2 psi`: convert a quantity of pressure in newtons per square meter to pounds per square inch
    `{prefix}convert 22 degC degF`: convert 22 degrees Celsius (as in "It's 22 degrees outside") to Fahrenheit
    `{prefix}convert 10 delta_degC delta_degF`: convert Celsius (as in "It's 10 degrees warmer now") to Fahrenheit
    """

    @convert_.error
    async def convert_error(self, ctx: VioletContext, exc: BaseException):
        show_error = ctx.bot.get_cog('Events').display_error
        exc = getattr(exc, 'original', exc)
        if isinstance(exc, pint.DimensionalityError):
            msg = f"Can't convert {self._clean_units(exc.units1)} ({self._clean_units(exc.dim1)}) " \
                f"to {self._clean_units(exc.units2)} ({self._clean_units(exc.dim2)})"
            await show_error(ctx, BadArgument(msg))
        elif isinstance(exc, pint.UndefinedUnitError):
            await show_error(ctx, BadArgument(f'Unknown unit `{exc.unit_names}`'))
        else:
            await show_error(ctx, exc)

    @staticmethod
    def _clean_units(units) -> str:
        return str(units).replace('**', '^').replace('[', '').replace(']', '')

    @command()
    async def invite(self, ctx: VioletContext, *commands: typing.Union[VioletCommand, Cog]) -> None:
        """ Create a link to invite me to your guild.
            Passing commands or cogs will provide me just enough permissions for those commands to work.
            If no commands/cogs are specified, the link will provide the required permissions for all of them.
            """
        if commands:
            commands = ([cmd for cmd in commands if isinstance(cmd, VioletCommand)] +
                        [cmd for cog in commands if isinstance(cog, Cog) for cmd in cog.commands])
        else:
            commands = ctx.bot.walk_commands()
        permissions = discord.Permissions()
        permissions.update(read_messages=True, send_messages=True)  # Permissions all commands require
        for command in commands:
            permissions.value |= command.bot_permissions.value
        await ctx.send('<{}>', discord.utils.oauth_url(ctx.me.id, permissions))

    invite.example_usage = """
    `{prefix}invite`: create a link to invite me to your guild, with enough permissions for all of my commands
    `{prefix}invite help mute kick giveme`: create an invite link with just enough permissions for those four commands
    `{prefix}invite help Moderation giveme`: create a link with permissions for the Moderation cog and those commands
    """

    @group(aliases=['away'])
    async def afk(self, ctx: VioletContext, *, reason: str = 'no reason'):
        """ Mark yourself as AFK with the given reason.
            Using this command will mark you AFK if you aren't already, and preserve the other settings if you are.
            If your reason starts with a subcommand, it will not work as intended. In that case, use `afk reason`."""
        await self.afk_reason.callback(self, ctx, reason=reason)

    afk.example_usage = """
    `{prefix}afk`: marks you AFK with no reason
    `{prefix}afk gone fishing`: marks you AFK with reason "gone fishing"
    `{prefix}afk sticky things on my shoe`: causes an error, use `{prefix}afk reason` instead
    """

    @afk.command('reason')
    async def afk_reason(self, ctx: VioletContext, *, reason: str = 'no reason'):
        """ Mark yourself as AFK with the given reason.
            Using this command will mark you AFK if you aren't already, and preserve the other settings if you are."""
        reason = cleaners.format(ctx, '{}', reason)
        if len(reason) > 100:
            raise BadArgument('AFK reasons must be at most 100 characters long')
        was_afk = ctx.author.id in self.afk_users
        async with self.acquire_db() as conn:
            record = await conn.fetchrow('INSERT INTO afk_users (user_id, reason) VALUES ($1, $2) ON CONFLICT (user_id)'
                                         'DO UPDATE SET reason = $2 RETURNING reason, sticky, return_at;',
                                         ctx.author.id, reason)
            self.afk_users[ctx.author.id] = tuple(record)
        if was_afk:
            await ctx.send('AFK reason updated: {}.', reason)
        else:
            await ctx.send('You are now AFK: {}.', reason)

    afk_reason.example_usage = """
    `{prefix}afk reason`: marks you AFK with no reason
    `{prefix}afk reason gone fishing`: marks you AFK with reason "gone fishing"
    `{prefix}afk reason sticky things on my shoe`: sets your AFK message to "sticky things on my shoe"
    """

    @afk.command('sticky', aliases=['stick'])
    async def afk_sticky(self, ctx: VioletContext, sticky: bool = True):
        """ Enables/disables stickiness when you are AFK.
            Using this command will mark you AFK if you aren't already, and preserve the other settings if you are.
            If your AFK is sticky, you won't automatically un-AFK when you send a message.
            This is useful, for instance, if you will be AFK most of the time, but online occasionally."""

        was_afk = ctx.author.id in self.afk_users
        async with self.acquire_db() as conn:
            record = await conn.fetchrow('INSERT INTO afk_users (user_id, sticky) VALUES ($1, $2) ON CONFLICT (user_id)'
                                         'DO UPDATE SET sticky = $2 RETURNING reason, sticky, return_at;',
                                         ctx.author.id, sticky)
            self.afk_users[ctx.author.id] = tuple(record)
        if was_afk:
            await ctx.send('Your AFK is now {}.', 'sticky' if sticky else 'not sticky')
        else:
            await ctx.send('You are now AFK, and it is {}.', 'sticky' if sticky else 'not sticky')

    afk_sticky.example_usage = """
    `{prefix}afk sticky`: marks your AFK as sticky
    `{prefix}afk sticky on`: marks your AFK as sticky explicitly (`true`, `yes`, `enable`, etc work too)
    `{prefix}afk sticky off`: marks your AFK as not sticky (`false`, `no`, `disable`, etc work too)
    """

    @afk.command('duration', aliases=['returntime', 'returnat', 'returnin', 'return'])
    async def afk_duration(self, ctx: VioletContext, *, duration: convert.timedelta = None):
        """ Set/reset the duration you expect to be AFK.
            Using this command will mark you AFK if you aren't already, and preserve the other settings if you are.
            If no argument is passed, I'll remove the expected duration."""
        was_afk = ctx.author.id in self.afk_users
        async with self.acquire_db() as conn:
            # This behaves correctly (sets return_at to NULL) when duration is NULL
            record = await conn.fetchrow('INSERT INTO afk_users (user_id, return_at) VALUES ($1, now() + $2)'
                                         'ON CONFLICT (user_id) DO UPDATE SET return_at = now() + $2'
                                         'RETURNING reason, sticky, return_at;', ctx.author.id, duration)
            self.afk_users[ctx.author.id] = tuple(record)
        if was_afk:
            if duration is None:
                await ctx.send('AFK return time reset.')
            else:
                await ctx.send('AFK return time set to {}.', format.timedelta(duration))
        else:
            if duration is None:
                await ctx.send('You are now AFK, with no expected return time.')
            else:
                await ctx.send('You are now AFK, and expect to return in {}.', format.timedelta(duration))

    afk_duration.example_usage = """
    `{prefix}afk duration`: removes your expected return time
    `{prefix}afk duration 2d`: marks you AFK, expecting to return in two days
    `{prefix}afk duration 2 days, 1 hour`: marks you AFK, expecting to return in a bit more than two days
    """


class VioletHelp(commands.HelpCommand):
    __help_msg = """Show this message, or helpful information for a specific command.
    You can pass a command name, a category name (see its commands), or no argument (see all commands)."""

    def __init__(self) -> None:
        super().__init__(command_attrs={'help': self.__help_msg}, verify_checks=False, sort=True)

    def _add_to_bot(self, bot):
        # Replaced to use custom command impl
        command = _VioletHelpImpl(self, **self.command_attrs)
        bot.add_command(command)
        self._command_impl = command

    async def send_error_message(self, error: str) -> None:
        events = self.context.bot.get_cog('Events')
        await events.display_error(self.context, BadArgument(error))

    async def send_bot_help(self, mapping: typing.Mapping[typing.Optional[Cog], typing.List[VioletCommand]]) -> None:
        await self.__send_help('Violet Commands', None,
                               [cmd for _, cog_commands in mapping.items() for cmd in cog_commands])

    async def send_command_help(self, command: Command) -> None:
        await self.__send_help('', command, [])

    async def send_group_help(self, group: Group) -> None:
        await self.__send_help(f'{self.context.clean_prefix}{group.qualified_name} Subcommands', group, group.commands)

    async def send_cog_help(self, cog: Cog) -> None:
        await self.__send_help(f'{cog.name} Commands', None, cog.commands)

    async def __send_help(self, page_title: str, command: typing.Optional[VioletCommand],
                          subcommands: typing.Iterable[VioletCommand]) -> None:
        info_page: typing.Optional[discord.Embed] = None
        pages: typing.List[discord.Embed] = []
        prefix = self.context.clean_prefix
        if command is not None:
            info_page = discord.Embed(title=self.get_command_signature(command), description=command.help,
                                      color=self.context.color)
            if command.example_usage:
                info_page.add_field(name='Usage', value=command.example_usage.format(ctx=self.context, prefix=prefix),
                                    inline=False)
                visible_checks = [check for check in command.checks if isinstance(check, checks.CheckABC)]
                if visible_checks:
                    lines = await asyncio.gather(*[self._check_info(self.context, check) for check in visible_checks])
                    info_page.add_field(name='Checks', value='\n'.join(lines))
        commands_paged = iterutils.segment(await self.filter_commands(subcommands, sort=True), 5)
        for page_num, page_commands in enumerate(commands_paged, start=1):
            page = discord.Embed(title=f'{page_title} page {page_num}/{len(commands_paged)}', color=self.context.color)
            for cmd in page_commands:
                if cmd.short_doc:
                    help_msg = cmd.short_doc
                elif cmd.example_usage:
                    help_msg = f'Run `{prefix}{self.context.invoked_with} {cmd.qualified_name}` for more information.'
                else:
                    help_msg = 'No information available.'
                page.add_field(name=self.get_command_signature(cmd), value=help_msg, inline=False)
            pages.append(page)

        if len(pages) + bool(info_page) == 1:
            await self.get_destination().send(embed=next(iter(pages), info_page))
        else:
            info_emoji = '\N{INFORMATION SOURCE}'
            # TODO paginate to get_destination() without this hack
            try:
                self.context.channel = self.get_destination()
                # await ctx.paginate(pages, None if info_page is None else {info_emoji: info_page}, (info_emoji, ...),
                #                    starting_page=0 if info_page is None else info_emoji)

                if info_page is None:
                    await self.context.paginate(pages, None, starting_page=0)
                else:
                    await self.context.paginate(pages, {info_emoji: info_page}, (info_emoji, ...),
                                                starting_page=info_emoji)
            finally:
                self.context.channel = self.context.message.channel

    async def _check_info(self, ctx: VioletContext, check: checks.CheckABC) -> str:
        try:
            passed = check.can_run(ctx)
            if inspect.iscoroutine(passed):
                passed = await passed
        except commands.CheckFailure:
            passed = False

        info = check.pass_info(ctx)
        if inspect.iscoroutine(info):
            info = await info
        return f'{":white_check_mark:" if passed else ":no_entry_sign:"} {info}'


class _VioletHelpImpl(commands.help._HelpCommandImpl, Command):
    pass

class GeneralConfig(Config):
    git_url: str = ''  # Base URL of GitHub/GitLab/any Git remote, i.e. https://github.com/:user/:repo (no end slash)
    git_branch: str = 'master'  # Branch to link to
