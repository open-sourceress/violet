# coding=utf-8
import asyncio
import difflib
import random
import typing
from datetime import datetime

import aiohttp
import asyncpg
import discord
from discord.ext.commands import BadArgument, Converter

from ..violetlib import checks, format, iterutils
from ..violetlib.coglocals import *


class Team(Converter):
    __slots__ = ('columns',)
    columns: str

    def __init__(self, *columns: str):
        self.columns = ', '.join(columns) or '*'

    async def convert(self, ctx: VioletContext, argument: str) -> asyncpg.Record:
        if argument.isdigit():
            team_num = int(argument)
        elif argument.startswith('frc') and argument[3:].isdigit():
            team_num = int(argument[3:])
        else:
            raise BadArgument(f'`{argument}` is not a team number')
        async with ctx.cog.acquire_db() as conn:
            record = await conn.fetchrow(f'SELECT {self.columns} FROM tba.teams WHERE number = $1;', team_num)
        if record is None:
            raise BadArgument(f"Team {team_num} doesn't exist")
        return record


if typing.TYPE_CHECKING:
    Team = Event = asyncpg.Record


class TBA(Cog):
    tba_color = discord.Color(0x3f51b5)  # Color of site banner
    config: 'TBAConfig'
    _pg_ext = ['fuzzystrmatch', 'pg_trgm']

    def __init__(self, bot: Violet, config: 'TBAConfig'):
        super().__init__(bot, config)
        self.session = aiohttp.ClientSession(loop=bot.loop, headers={'X-TBA-Auth-Key': config.api_key})

    async def setup_db(self):
        async with self.acquire_db() as conn:
            loaded_exts = await conn.fetch("SELECT extname FROM pg_extension WHERE extname = ANY($1);", self._pg_ext)
            if len(loaded_exts) < len(self._pg_ext):
                loaded_names = [name for name, in loaded_exts]
                raise RuntimeError(f'Missing postgres extensions: {set(self._pg_ext) - set(loaded_names)}')

            await conn.execute('CREATE SCHEMA IF NOT EXISTS tba;')
            await conn.execute('CREATE TABLE IF NOT EXISTS tba.requests (url text PRIMARY KEY NOT NULL,'
                               'response jsonb, etag text NOT NULL, expires_at timestamp NOT NULL);')

            # Teams use number as primary key because it's faster to index
            await conn.execute('CREATE TABLE IF NOT EXISTS tba.teams (number int PRIMARY KEY NOT NULL,'
                               'key text NOT NULL, name text NOT NULL, fullname text NOT NULL, motto text,'
                               'website text, rookie_year int2, city text, state_prov text, country text,'
                               'postal_code text, address text, home_championship jsonb);')

            await conn.execute('CREATE TABLE IF NOT EXISTS tba.events (key text PRIMARY KEY NOT NULL,'
                               'event_name text NOT NULL, year int2 NOT NULL, event_type text NOT NULL,'
                               'division_keys text[] NOT NULL, parent_key text, timezone text, week int2,'
                               'start_date date, end_date date, city text, state_prov text, country text,'
                               'postal_code text, address text);')

            await conn.execute('CREATE TABLE IF NOT EXISTS tba.districts (key text PRIMARY KEY NOT NULL,'
                               'abbreviation text NOT NULL, display_name text NOT NULL, year int2 NOT NULL);')

        self.bot.loop.create_task(self._preload_caches())

    async def _request(self, url: str) -> typing.Tuple[typing.Union[typing.List, typing.Mapping, None], bool]:
        async with self.acquire_db() as conn:
            record = await conn.fetchrow('SELECT response, etag, expires_at >= now() FROM tba.requests '
                                         'WHERE url = $1;', url)
        if record is not None and record[2]:
            return (record[0], False)

        headers = None if record is None else {'If-None-Match': record[1]}
        async with self.session.get(self.config.api_base_url + url, headers=headers) as resp:
            resp.raise_for_status()
            modified = resp.status != 304
            if modified:
                response = await resp.json()
                etag = resp.headers['ETag']
            else:
                # 304 is only served if Is-None-Match is provided, which is only true if record is not None
                response, etag = record[:2]

        cache_directives = list(map(str.strip, resp.headers.get('Cache-Control', '').split(',')))
        age = next((int(directive[8:]) for directive in cache_directives if directive.startswith('max-age=')), 0)
        async with self.acquire_db() as conn:
            await conn.execute("INSERT INTO tba.requests VALUES ($1, $2, $3, now() + $4 * '1 second'::interval)"
                               "ON CONFLICT (url) DO UPDATE SET response = $2, etag = $3,"
                               "expires_at = now() + $4 * '1 second'::interval;",
                               url, response, etag, age)

        return (response, modified)

    async def _preload_caches(self) -> None:
        status, _ = await self._request('/status')
        self.logger.debug('Spawning cache preload tasks')
        self.bot.loop.create_task(self._preload_teams())
        self.bot.loop.create_task(self._preload_events(status))
        self.bot.loop.create_task(self._preload_districts(status))

    async def _preload_teams(self) -> None:
        page_num = 0
        page = True
        team_count = 0

        to_update = []  # Page numbers that didn't return 304 Not Modified and therefore need to be updated in tba.teams

        while page:  # Keep downloading until there are no results, aka the end
            url = f'/teams/{page_num}'
            page, modified = await self._request(url)
            if modified:
                to_update.append((page_num, url))
            page_num += 1
            team_count += len(page)

        self.logger.debug('Downloaded data for %r teams', team_count)

        async with self.acquire_db() as conn, conn.transaction():
            await conn.execute('CREATE TEMP TABLE temp_teams (data jsonb, etag text,'
                               'expires_at timestamp) ON COMMIT DROP;')
            await conn.executemany('INSERT INTO temp_teams SELECT jsonb_array_elements(response), etag,'
                                   'expires_at FROM tba.requests WHERE url = $1;',
                                   [(f'/teams/{i}',) for i in range(page_num)])
            await conn.execute("INSERT INTO tba.requests (url, response, etag, expires_at)"
                               "SELECT '/team/' || (data ->> 'key'), data, etag, expires_at FROM temp_teams "
                               "ON CONFLICT (url) DO UPDATE SET response = EXCLUDED.response,"
                               "etag = EXCLUDED.etag, expires_at = EXCLUDED.expires_at WHERE "
                               "EXCLUDED.expires_at > tba.requests.expires_at;")
        self.logger.debug('Placeholder individual team URLs inserted')

        if not to_update:
            self.logger.debug('Skipping unpacking teams, no data has changed')
            return

        rows_updated = 0
        async with self.acquire_db() as conn, conn.transaction():
            for page_num, url in to_update:
                await conn.execute('DELETE FROM tba.teams WHERE 500 * $1 <= number '
                                   'AND number < 500 * ($1 + 1);', page_num)  # Purging is easier than upserting
                # Unpack the JSON array from page {i} into a set of records, then insert them into tba.teams
                result = await conn.execute('''
                INSERT INTO tba.teams (
                  number, key, name, fullname, motto, website, rookie_year,
                  city, state_prov, country, postal_code, address, home_championship
                ) SELECT * FROM jsonb_to_recordset((SELECT response FROM tba.requests WHERE url = $1))
                AS tba_json_team (
                  team_number int, key text, nickname text, name text, motto text, website text, rookie_year int2,
                  city text, state_prov text, country text, postal_code text, address text, home_championship jsonb
                );
                ''', url)
                rows_updated += int(result.rpartition(' ')[2])
        self.logger.debug('Unpacked updated data for %r teams', rows_updated)

    async def _preload_events(self, status) -> None:
        max_season = status['max_season']
        event_count = 0
        to_update = []
        for year in range(1992, max_season + 1):
            url = f'/events/{year}'
            page, modified = await self._request(url)
            if modified:
                to_update.append((year, url))
            event_count += len(page)

        self.logger.debug('Downloaded data for %r events', event_count)

        async with self.acquire_db() as conn, conn.transaction():
            await conn.execute('CREATE TEMP TABLE temp_events (data jsonb, etag text,'
                               'expires_at timestamp) ON COMMIT DROP;')
            await conn.executemany('INSERT INTO temp_events SELECT jsonb_array_elements(response), etag,'
                                   'expires_at FROM tba.requests WHERE url = $1;',
                                   [(f'/events/{year}',) for year in range(1992, max_season + 1)])
            await conn.execute("INSERT INTO tba.requests (url, response, etag, expires_at)"
                               "SELECT '/events/' || (data ->> 'year'), data, etag, expires_at "
                               "FROM temp_events ON CONFLICT (url) DO UPDATE SET response = EXCLUDED.response,"
                               "etag = EXCLUDED.etag, expires_at = EXCLUDED.expires_at WHERE "
                               "EXCLUDED.expires_at > tba.requests.expires_at;")
        self.logger.debug('Placeholder individual event URLs inserted')

        if not to_update:
            self.logger.debug('Skipping unpacking events, no data has changed')
            return

        rows_updated = 0
        async with self.acquire_db() as conn, conn.transaction():
            for year, url in to_update:
                await conn.execute('DELETE FROM tba.events WHERE year = $1;', year)
                result = await conn.execute('''
                INSERT INTO tba.events (
                  key, event_name, year, event_type, division_keys, parent_key, timezone, week,
                  start_date, end_date, city, state_prov, country, postal_code, address
                ) SELECT * FROM jsonb_to_recordset((SELECT response FROM tba.requests WHERE url = $1))
                AS tba_json_event (
                  key text, name text, year int2, event_type_string text, division_keys text[], parent_event_key text,
                  timezone text, week int2, start_date date, end_date date, city text, state_prov text, country text,
                  postal_code text, address text
                );
                ''', f'/events/{year}')
                rows_updated += int(result.rpartition(' ')[2])
        self.logger.debug('Unpacked updated data for %r events', rows_updated)

    async def _preload_districts(self, status) -> None:
        max_year = year = status['max_season']
        district_count = 0
        page = True  # Just has to be truey to start
        to_update = []
        while page:
            url = f'/districts/{year}'
            page, modified = await self._request(url)
            if modified:
                to_update.append((year, url))
            district_count += len(page)
            year -= 1

        self.logger.debug('Downloaded data for %r districts', district_count)
        if not to_update:
            self.logger.debug('Skipping unpacking districts, no data has changed')
            return

        rows_updated = 0
        async with self.acquire_db() as conn, conn.transaction():
            for year, url in to_update:
                await conn.execute('DELETE FROM tba.districts WHERE year = $1;', year)
                result = await conn.execute('''
                INSERT INTO tba.districts (
                  key, abbreviation, display_name, year
                ) SELECT * FROM jsonb_to_recordset((SELECT response FROM tba.requests WHERE url = $1))
                AS tba_json_district (
                  key text, abbreviation text, display_name text, year int2
                );''', url)
                rows_updated += int(result.rpartition(' ')[2])
        self.logger.debug('Unpacked updated data for %r districts', rows_updated)

    async def _validate_year(self, year: typing.Optional[int], allow_future: bool = True) -> int:
        status, _ = await self._request('/status')
        min_allowed = 1992  # First year of FIRST
        max_allowed = status['max_season'] if allow_future else status['current_season']
        if year is None:
            return status['current_season']
        elif min_allowed <= year <= max_allowed:
            return year
        else:
            raise BadArgument(f"Years must be between {min_allowed} and {max_allowed}")

    def _format_location(self, record: typing.Mapping[str, typing.Any]) -> str:
        address = record['address']
        if address:
            return address

        city, state_prov, country = record['city'], record['state_prov'], record['country']
        if not (city or state_prov or country):
            return 'Unknown'

        if state_prov and country and state_prov.endswith(country):
            country = None  # Hide country if it is redundant
        location = ', '.join(filter(None, [city, state_prov, country]))

        postal_code = record['postal_code']
        if postal_code:
            location = f'{location} {postal_code}'

        return location

    def _format_dates(self, start: typing.Optional[datetime], end: typing.Optional[datetime]) -> str:
        if start is None:
            if end is None:
                return 'Unknown'
            else:
                return str(end)
        elif end is None or start == end:
            return str(start)
        else:
            return f'{start} to {end}'

    @group()
    @checks.bot_permissions('embed_links')
    async def tba(self, ctx: VioletContext, team_number: int = None):
        """ Fetch FIRST Robotics Competition data from The Blue Alliance.
            If no arguments are passed, TBA's status is shown.
            If one argument is passed, the team with that number is shown.
            """
        if team_number is not None:
            await ctx.invoke(self.tba_team, team_number)
            return
        status, _ = await self._request('/status')
        embed = discord.Embed(color=self.tba_color, title='The Blue Alliance')
        year_line = f"Current season: {status['current_season']}"
        if status['current_season'] != status['max_season']:
            year_line = f"{year_line} (data available for {status['max_season']})"
        datafeed_line = f"Datafeed is {'offline' if status['is_datafeed_down'] else 'online'}"
        if status['down_events']:
            down_line = f"{len(status['down_events'])} event(s) offline: {', '.join(status['down_events'])}"
        else:
            down_line = 'No events offline'
        embed.description = f'{year_line}\n{datafeed_line}\n{down_line}'
        await ctx.send(embed=embed)

    tba.example_usage = """
    `{prefix}tba`: show TBA's current season and status
    `{prefix}tba 488`: show information about team 488
    """

    @tba.command('team')
    @checks.bot_permissions('embed_links')
    async def tba_team(self, ctx: VioletContext, team_number: int):
        """Fetch data on an FRC team by number."""
        _, modified = await self._request(f'/team/frc{team_number}')
        async with self.acquire_db() as conn:
            if modified:
                async with conn.transaction():
                    await conn.execute('DELETE FROM tba.teams WHERE number = $1;', team_number)
                    team = await conn.fetchrow('''
                    INSERT INTO tba.teams (
                      number, key, name, fullname, motto, website, rookie_year,
                      city, state_prov, country, postal_code, address, home_championship
                    ) SELECT * FROM jsonb_to_record((SELECT response FROM tba.requests WHERE url = $1))
                    AS tba_json_team (
                      team_number int, key text, nickname text, name text, motto text, website text, rookie_year int2,
                      city text, state_prov text, country text, postal_code text, address text, home_championship jsonb
                    ) RETURNING *;
                    ''', f'/team/frc{team_number}')
            else:
                team = await conn.fetchrow('SELECT * FROM tba.teams WHERE number = $1;', team_number)

        embed = discord.Embed(color=self.tba_color, title=f"FRC Team {team['number']}: {team['name']}",
                              url=f"https://thebluealliance.com/team/{team['number']}",
                              description=team['fullname'])

        # The only teams that have no city are the ones that only have a number and "Team {number}" as their name
        if team['city'] is None:
            embed.description += '\nThis team exists, but has no information.'
            await ctx.send(embed=embed)
            return

        embed.add_field(name='Rookie Year', value=team['rookie_year'] or 'Unknown')
        embed.add_field(name='Location', value=self._format_location(team))

        if team['website']:
            embed.add_field(name='Website', value=team['website'])
        championships = team['home_championship'] or {}
        champs_cities = set(championships.values())
        if not champs_cities:
            embed.add_field(name='Championship', value='Unknown')
        elif len(champs_cities) == 1:
            embed.add_field(name='Championship', value=champs_cities.pop())
        else:
            embed.add_field(name='Championship',
                            value='\n'.join(f'{championships[year]} {year}' for year in sorted(championships, key=int)))
        if team['motto']:
            embed.add_field(name='Motto', value=team['motto'], inline=False)
        await ctx.send(embed=embed)

    tba_team.example_usage = """
    `{prefix}tba team 488`: show information about team 488
    """

    @tba.error  # Needed for when tba <team_number> delegates to tba_team
    @tba_team.error
    async def tba_team_error(self, ctx: VioletContext, exc: BaseException):
        exc = getattr(exc, 'original', exc)
        if isinstance(exc, aiohttp.ClientResponseError) and exc.status == 404:
            display_exc = BadArgument("That team doesn't exist")
        else:
            display_exc = exc
        await ctx.bot.get_cog('Events').display_error(ctx, display_exc)

    @tba.command('teams')
    @checks.bot_permissions('embed_links', 'add_reactions')
    async def tba_teams(self, ctx: VioletContext, district: str, year: int = None):
        """ Fetch names and numbers of all teams in a district, or anywhere, in the given or current year.
            Provide `all` as the district to show teams from everywhere, even teams not in a district.
            """
        if district.isnumeric():
            district, year = None, int(district)
        year = await self._validate_year(year)
        if district == 'all':
            team_keys = []
            page_num = 0
            page = True
            while page:
                page, _ = await self._request(f'/teams/{year}/{page_num}/keys')
                page_num += 1
                team_keys.extend(page)
            embed_title = ''
        else:
            async with self.acquire_db() as conn:
                record = await conn.fetchrow('SELECT key, display_name FROM tba.districts '
                                             'WHERE abbreviation = $1 AND year = $2;', district, year)
                if record is None:
                    raise BadArgument("That district doesn't exist that year")
            team_keys, _ = await self._request(f"/district/{record['key']}/teams/keys")
            embed_title = f" in {record['display_name']}"

        async with self.acquire_db() as conn:
            teams = [line for line, in await conn.fetch("SELECT 'Team ' || number || ', ' || name FROM tba.teams "
                                                        "WHERE key = ANY($1) ORDER BY number;", team_keys)]

        embeds = [discord.Embed(color=self.tba_color, title=f'Teams{embed_title} in {year}',
                                description='\n'.join(page)) for page in iterutils.segment(teams, 20)]
        for i, embed in enumerate(embeds, 1):
            embed.set_footer(text=f'Page {i} of {len(embeds)}')
        if len(embeds) == 1:
            embeds[0].set_footer(text='')
            await ctx.send(embed=embeds[0])
        else:
            await ctx.paginate(embeds)

    tba_teams.example_usage = """
    `{prefix}tba teams in`: list teams in the Indiana district in the current year
    `{prefix}tba teams fnc 2019`: list teams in the FIRST North Carolina district in 2019
    `{prefix}tba teams all 1992`: list teams that existed in the first year of FIRST
    `{prefix}tba teams all`: list every team that currently exists
    """

    @tba.command('awards')
    @checks.bot_permissions('embed_links')
    async def tba_awards(self, ctx: VioletContext, team: "Team('key', 'name')", when: typing.Union[int, str] = None):
        """ Fetch a team's awards for a given year or event.
            If no values are given, awards from the current year are shown.
            If a year is given, all awards from that year are shown.
            If an event key is given, only awards from that event are shown. The current year is inferred if not given.
            """
        if isinstance(when, int):  # Year: given year all events
            year = when
            event = None
        elif when is None:  # Nothing: current year all events
            event = year = None
        elif when[:4].isdigit():  # Event key with year: given year given event
            year = await self._validate_year(int(when[:4]), False)
            event = when[4:]
        else:  # Event code without year: current year given event
            year = None
            event = when

        if year is None:
            year = await self._validate_year(year, False)

        embed = discord.Embed(color=self.tba_color, title=f"Awards for {team['name']}")

        def str_award(award: dict) -> str:
            recips = ', '.join(recip['awardee'] for recip in award['recipient_list']
                                     if recip['awardee'] is not None and recip['team_key'] == team['key'])
            if recips:
                return f"{recips}: {award['name']}"
            else:
                return award['name']

        if event is None:
            embed.title += f' in {year}'
            awards, _ = await self._request(f"/team/{team['key']}/awards/{year}")
            event_keys = { award['event_key'] for award in awards }
            async with self.acquire_db() as conn:
                events = dict(await conn.fetch(
                    'SELECT key, event_name FROM tba.events WHERE key = ANY($1) ORDER BY start_date, end_date;',
                    tuple(event_keys),  # asyncpg doesn't map sets to postgres arrays so cast to tuple first
                ))
            # this preserves the query ORDER BY, sorting events by date ascending. Requires dictionaries to maintain
            # insertion order (CPython 3.7+, Python spec 3.8+)
            per_event = { key: [] for (i, key) in enumerate(events.keys()) }
            for award in awards:
                per_event[award['event_key']].append(str_award(award))

            for (event_key, event_awards) in per_event.items():
                embed.add_field(name=f"{events[event_key]} ({event_key})", value='\n'.join(event_awards), inline=False)
        else:
            async with self.acquire_db() as conn:
                event_name = await conn.fetchval('SELECT event_name FROM tba.events WHERE key = $1;', f'{year}{event}')
            if event_name is None:
                raise BadArgument("That event doesn't exist or wasn't held that year")

            embed.title += f' at {event_name} {year}'
            awards, _ = await self._request(f"/team/{team['key']}/event/{year}{event}/awards")
            embed.description = '\n'.join(str_award(award) for award in awards)

        if not awards:
            embed.description = 'No awards to show.'

        await ctx.send(embed=embed)

    tba_awards.example_usage = """
    `{prefix}tba awards 1294`: show awards earned by team 1294 Top Gun this year
    `{prefix}tba awards 1294 2017`: show awards earned by Top Gun in 2017
    `{prefix}tba awards 1294 waamv`: show awards earned by Top Gun at Auburn Mountainview this year
    `{prefix}tba awards 1294 2017waamv`: show awards earned by Top Gun at Auburn Mountainview in 2017
    """

    @tba.command('events')
    @checks.bot_permissions('embed_links')
    async def tba_events(self, ctx: VioletContext, team: "Team('key', 'name')", year: int = None):
        """Fetch the events a team attended in the given or current year."""
        year = await self._validate_year(year)
        events, _ = await self._request(f"/team/{team['key']}/events/{year}")
        events.sort(key=lambda e: (e['start_date'], e['end_date'] or e['start_date']))
        embed = discord.Embed(color=self.tba_color, title=f"Events for {team['name']} in {year}")
        for event in events:
            embed.add_field(name=f"{event['name']} ({event['key']})",
                            value=f"{self._format_dates(event['start_date'], event['end_date'])}\n"
                                  f"{self._format_location(event)}", inline=False)
        await ctx.send(embed=embed)

    tba_events.example_usage = """
    `{prefix}tba events 4982`: show which events team 4982 Olympus Robotics attended
    `{prefix}tba events 4982 2017`: show which events Olympus Robotics attended in 2017
    """

    @tba.command('media')
    @checks.bot_permissions('embed_links')
    async def tba_media(self, ctx: VioletContext, team: "Team('key', 'name')", year: int = None):
        """Fetch a team's media for the given or current year."""
        year = await self._validate_year(year, False)

        media, _ = await self._request(f"/team/{team['key']}/media/{year}")
        embed = discord.Embed(color=self.tba_color, title=f"Media for {team['name']} in {year}")
        images_displayed = 0
        for media_item in media:
            title, url, is_image = self._media_info(media_item)
            if not url:
                continue
            embed.add_field(name=title, value=url, inline=False)
            if is_image and images_displayed < 2:
                if images_displayed == 0:
                    embed.set_image(url=url)
                elif images_displayed == 1:
                    embed.set_thumbnail(url=url)
                images_displayed += 1
        await ctx.send(embed=embed)

    tba_media.example_usage = """
    `{prefix}tba media 254`: show the Cheezy Poofs' media from this year
    `{prefix}tba media 254 2017`: show the Cheezy Poofs' media from 2017
    """

    @staticmethod
    def _media_info(media: typing.Mapping[str, typing.Any]) -> typing.Tuple[str, str, bool]:
        if media['type'] == 'youtube':
            return ('YouTube', f"https://www.youtube.com/watch?v={media['foreign_key']}", False)
        elif media['type'] == 'imgur':
            return ('Imgur', f"http://i.imgur.com/{media['foreign_key']}.jpg", True)
        elif media['type'] == 'cdphotothread':
            return ('ChiefDelphi', f"https://www.chiefdelphi.com/media/img/{media['details']['image_partial']}", True)
        elif media['type'] == 'instagram-image':
            return ('Instagram', media['details']['thumbnail_url'], True)
        elif media['type'] == 'avatar':
            return ('', '', False)  # No easy way to turn avatars into displayable image URLs
        else:
            return (media['type'].title(), f"{media['foreign_key']} on {media['type'].title()}", False)

    @tba.command('robots')
    @checks.bot_permissions('embed_links')
    async def tba_robots(self, ctx: VioletContext, team: "Team('key', 'name')"):
        """Fetch a team's robot names."""
        robots, _ = await self._request(f"/team/{team['key']}/robots")
        embed = discord.Embed(color=self.tba_color, title=f"Robot names for {team['name']}")
        embed.description = '\n'.join(f"{robot['year']}: {robot['robot_name']}" for robot in robots)
        await ctx.send(embed=embed)

    tba_robots.example_usage = """
    `{prefix}tba robots 2046`: show team 2046's robot names
    """

    @tba.command('event')
    @checks.bot_permissions('embed_links')
    async def tba_event(self, ctx: VioletContext, code: str, year: int = None):
        """Fetch data on an FRC event by event code and year."""
        year = await self._validate_year(year)
        if not code.isalpha():
            raise BadArgument('Invalid event code')  # Prevent dropping arbitrary text into the TBA url
        event, modified = await self._request(f'/event/{year}{code}')
        event_keys = [event['key']] + event['division_keys']
        if modified:  # Modified data for this event, so there's probably modified data to get for divisions
            await asyncio.gather(*[self._request(f'/event/{division_key}') for division_key in event['division_keys']])
        async with self.acquire_db() as conn:
            if modified:
                async with conn.transaction():
                    await conn.execute('DELETE FROM tba.events WHERE key = ANY($1);', event_keys)
                    await conn.executemany('''
                    INSERT INTO tba.events (
                      key, event_name, year, event_type, division_keys, parent_key, timezone, week,
                      start_date, end_date, city, state_prov, country, postal_code, address
                    ) SELECT * FROM jsonb_to_record((SELECT response FROM tba.requests WHERE url = $1))
                    AS tba_json_event (
                      key text, name text, year int2, event_type_string text, division_keys text[],
                      parent_event_key text, timezone text, week int2, start_date date, end_date date, city text,
                      state_prov text, country text, postal_code text, address text
                    );''', [(f'/event/{key}',) for key in event_keys])

            records = await conn.fetch('SELECT event.key, btrim(event.event_name) AS event_name,'
                                       'event.week, event.event_type, event.year,'
                                       'least(event.start_date, division.start_date) AS start_date,'
                                       'greatest(event.end_date, division.end_date) AS end_date,'
                                       'event.city, event.state_prov, event.country, event.postal_code, event.address,'
                                       'division.key AS division_key, division.event_name AS division_name '
                                       'FROM tba.events AS event LEFT OUTER JOIN tba.events AS division '
                                       'ON event.key = division.parent_key WHERE event.key = $1;',
                                       f'{year}{code.lower()}')
        if not records:
            raise BadArgument("That event doesn't exist or wasn't held that year")

        event = records[0]

        embed = discord.Embed(color=self.tba_color,
                              title=f"{event['event_name']} {event['year']} ({event['key']})",
                              url=f"https://thebluealliance.com/event/{event['key']}")
        if event['week']:
            embed.description = f"Week {event['week']} {event['event_type']} event"
        else:
            embed.description = f"{event['event_type'].capitalize()} event"

        embed.add_field(name='Dates', value=self._format_dates(event['start_date'], event['end_date']))
        embed.add_field(name='Location', value=self._format_location(event))

        alliances = (await self._request(f"/event/{event['key']}/alliances"))[0] or []
        winner = next((alliance['picks'] for alliance in alliances if alliance['status']['level'] == 'f' and alliance['status']['status'] == 'won'), None)
        if winner is not None:
            embed.add_field(name='Winners', value='\n'.join(f'Team {team[3:]}' for team in winner))

        if event['division_key'] is not None:  # Multiple records for all divisions
            embed.add_field(name='Divisions',
                            value='\n'.join(f"{div['division_name']} ({div['division_key']})" for div in records))

        await ctx.send(embed=embed)

    tba_event.example_usage = """
    `{prefix}tba event cmp 1992`: show information about the first FIRST competition
    """

    @tba_event.error
    async def tba_event_error(self, ctx: VioletContext, exc: BaseException):
        exc = getattr(exc, 'original', exc)
        if isinstance(exc, aiohttp.ClientResponseError) and exc.status == 404:
            display_exc = BadArgument("That event doesn't exist or wasn't held that year")
        else:
            display_exc = exc
        await ctx.bot.get_cog('Events').display_error(ctx, display_exc)

    @tba.command('district')
    @checks.bot_permissions('embed_links')
    async def tba_district(self, ctx: VioletContext, key: str, year: int = None):
        """Fetch information about a district by abbreviation, for the given or current year."""
        year = await self._validate_year(year)
        districts_url = f'/districts/{year}'
        _, modified = await self._request(districts_url)
        async with self.acquire_db() as conn:
            if modified:
                await conn.execute('''
                INSERT INTO tba.districts (key, abbreviation, display_name, year)
                SELECT * FROM jsonb_to_recordset((SELECT response FROM tba.requests WHERE url = $1))
                AS tba_json_district (key text, abbreviation text, display_name text, year int2)
                ON CONFLICT (key) DO UPDATE SET abbreviation = EXCLUDED.abbreviation,
                display_name = EXCLUDED.display_name, year = EXCLUDED.year;''', districts_url)
            district = await conn.fetchrow('SELECT * FROM tba.districts WHERE key = $1', f'{year}{key}')
        if district is None:
            raise BadArgument("That district doesn't exist that year")
        team_nums = [int(key[3:]) for key in (await self._request(f"/district/{district['key']}/teams/keys"))[0]]
        async with self.acquire_db() as conn:
            oldest, newest = await conn.fetch('SELECT number, name FROM tba.teams '
                                              'WHERE number = $1 OR number = $2 ORDER BY number;',
                                              min(team_nums), max(team_nums))
        embed = discord.Embed(color=self.tba_color, title=f"{district['abbreviation'].upper()} District {year}",
                              description=f"{district['display_name']}\n{len(team_nums)} teams")
        embed.add_field(name='Oldest Team', value=f"{oldest['number']}, {oldest['name']}", inline=False)
        embed.add_field(name='Newest Team', value=f"{newest['number']}, {newest['name']}", inline=False)
        await ctx.send(embed=embed)

    tba_district.example_usage = """
    `{prefix}tba district pnw`: show information about the Pacific Northwest in the current year
    `{prefix}tba district fma 2019`: show information about First Mid-Atlantic in 2019
    """

    @tba.command('districts')
    @checks.bot_permissions('embed_links')
    async def tba_districts(self, ctx: VioletContext, year: int = None):
        """List the districts that exist in the given or current year."""
        year = await self._validate_year(year)
        url = f'/districts/{year}'
        _, modified = await self._request(url)
        async with self.acquire_db() as conn:
            if modified:
                districts = await conn.fetch('''
                INSERT INTO tba.districts (key, abbreviation, display_name, year)
                SELECT * FROM jsonb_to_recordset((SELECT response FROM tba.requests WHERE url = $1))
                AS tba_json_district (key text, abbreviation text, display_name text, year int2)
                ON CONFLICT (key) DO UPDATE SET abbreviation = EXCLUDED.abbreviation,
                display_name = EXCLUDED.display_name, year = EXCLUDED.year RETURNING *;''', url)
            else:
                districts = await conn.fetch('SELECT * FROM tba.districts WHERE year = $1;', year)

        embed = discord.Embed(color=self.tba_color, title=f'FRC Districts in {year}')
        embed.description = '\n'.join(f"{district['display_name']} ({district['abbreviation'].upper()})"
                                      for district in sorted(districts, key=lambda dist: dist['display_name']))
        await ctx.send(embed=embed)

    tba_districts.example_usage = """
    `{prefix}tba districts`: list all districts that currently exist
    `{prefix}tba districts 2009`: list all districts that existed in 2009
    """

    @tba.command('rankings')
    @checks.bot_permissions('embed_links', 'add_reactions')
    async def tba_rankings(self, ctx: VioletContext, district: str, year: int = None):
        """Fetch rankings for all teams in a district, for the given or current year."""
        year = await self._validate_year(year, False)
        async with self.acquire_db() as conn:
            district = await conn.fetchrow('SELECT * FROM tba.districts WHERE key = $1', f'{year}{district}')
        if district is None:
            raise BadArgument("That district doesn't exist that year")
        rankings, _ = await self._request(f"/district/{district['key']}/rankings")
        if rankings is None:  # when current_season < year <= max_season
            raise BadArgument("I can't see the future")

        pages = iterutils.segment(rankings, 20)
        embeds = [discord.Embed(color=self.tba_color, title=f"Rankings for {district['display_name']} {year}")
                  for _ in pages]
        for page_num, (page, embed) in enumerate(zip(pages, embeds), 1):
            embed.description = '\n'.join(f"#{ranking['rank']}: Team {ranking['team_key'][3:]}, "
                                          f"{ranking['point_total']} point(s)" for ranking in page)
            embed.set_footer(text=f'Page {page_num} of {len(pages)}')
        await ctx.paginate(embeds)

    tba_rankings.example_usage = """
    `{prefix}tba rankings isr`: show Israel's district rankings for this year
    `{prefix}tba rankings isr 2017`: show Israel's district rankings in 2018
    """

    @tba.command('search')
    @checks.bot_permissions('embed_links')
    async def tba_search(self, ctx: VioletContext, *, name: str):
        """ Search for teams by name.
            Teams whose names contain the given text (or something similar) are listed.
            """
        async with self.acquire_db() as conn:
            records = await conn.fetch('SELECT number, name FROM tba.teams '
                                       'WHERE word_similarity(lower($1), lower(name)) > 0.6 '
                                       'ORDER BY word_similarity(lower($1), lower(name)) DESC, number;', name)
        if not records:
            await ctx.send('No matching teams found.')
            return

        pages = iterutils.segment(enumerate(records, 1), 10)
        embeds = [discord.Embed(color=self.tba_color, title='Team Search Results',
                                description='\n'.join(f'#{i}: Team #{num}, {name}' for i, (num, name) in page))
                  for page in pages]
        if len(embeds) == 1:
            await ctx.send(embed=embeds[0])
        else:
            await ctx.paginate([em.set_footer(text=f'Page {i} of {len(pages)}') for i, em in enumerate(embeds, 1)])

    tba_search.example_usage = """
    `{prefix}tba search eagles`: list teams with "eagles" (or "Eagles" or "Eagle" etc.) in their name
    """

    @tba.command('quiz')
    async def tba_quiz(self, ctx: VioletContext, area: str = 'all', year: int = None):
        """ Quiz yourself on your team knowledge.
            I'll send a random team's name or number, and you have 3 tries to pick the matching number/name.
            If I send a name, any team whose name is fairly similar will count as correct.
            Only teams from the given event/district (use `all` for any) in the given or current year can be chosen.
            """
        if area.isnumeric():
            area, year = 'all', int(area)
        year = await self._validate_year(year)
        keys = await self._find_teams(area, year)

        async with self.acquire_db() as conn:
            num, name = await conn.fetchrow('SELECT number, name FROM tba.teams WHERE key = $1;', random.choice(keys))
            if random.randrange(2):  # Random boolean, 0 == show name, 1 == show number
                query = f"What is team #{num}'s name?"
                reference_answers = [name]

                def check_answer(ans: str) -> bool:
                    return difflib.SequenceMatcher(' \r\n.?!'.__contains__, name.casefold(),
                                                   ans.casefold()).ratio() >= 0.9
            else:
                query = f"What is \"{name}\"'s number?"
                correct_numbers = await conn.fetch('SELECT number FROM tba.teams WHERE '
                                                   'levenshtein(lower(name), lower($1))::real / length(name) < 0.1 '
                                                   'ORDER BY number;', name)  # SQL levenshtein is distance
                reference_answers = [str(num) for (num,) in correct_numbers]
                check_answer = set(reference_answers).__contains__

        def check(m: discord.Message) -> bool:
            return m.channel.id == ctx.channel.id and m.author.id == ctx.author.id and m.content

        await ctx.send('{}', query)
        try:
            for i in range(3, 0, -1):
                response: discord.Message = await ctx.bot.wait_for('message', check=check, timeout=60)
                if response.content.casefold() in {'cancel', 'quit', 'exit', 'end'}:
                    await ctx.send('Quiz cancelled.')
                    break
                elif check_answer(response.content):
                    await ctx.send('Correct! You win!')
                    break
                elif i == 1:
                    if len(reference_answers) == 1:
                        await ctx.send('Incorrect. The correct answer was {}.', *reference_answers)
                    else:
                        await ctx.send('Incorrect. The correct answers were {}.', format.pluralize(reference_answers))
                else:
                    await ctx.send('Incorrect.  You have {} {} left.', i - 1, 'try' if i == 2 else 'tries')
        except asyncio.TimeoutError:
            await ctx.send(':alarm_clock: Out of time.')

    tba_quiz.example_usage = """
    `{prefix}tba quiz`: start a quiz from all teams that exist this year
    `{prefix}tba quiz cave`: start a quiz from only teams that competed in the Ventura Regional this year
    `{prefix}tba quiz 1992`: start a quiz from only teams that existed in 1992
    `{prefix}tba quiz cmptx 2018`: start a quiz from only teams that competed on Houston Einstein 2018
    """

    async def _find_teams(self, area: str, year: int) -> typing.List[str]:
        if area == 'all':
            page_num = 0
            page = True
            teams = []
            while page:
                page, _ = await self._request(f'/teams/{year}/{page_num}/keys')
                teams.extend(page)
                page_num += 1
            return teams

        district = event = None  # Small indirection to release the connection before doing more I/O
        async with self.acquire_db() as conn:
            district = await conn.fetchval('SELECT key FROM tba.districts WHERE abbreviation = $1 AND year = $2;',
                                           area, year)
            if district is None:
                event = await conn.fetchval('SELECT key FROM tba.events WHERE key = $1;', f'{year}{area}')

        if district is not None:
            return (await self._request(f'/district/{district}/teams/keys'))[0]
        elif event is not None:
            return (await self._request(f'/event/{event}/teams/keys'))[0]
        else:
            raise BadArgument("That event/district doesn't exist that year")

class TBAConfig(Config):
    api_key: str
    api_base_url: str = 'https://www.thebluealliance.com/api/v3'
