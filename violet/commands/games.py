# coding=utf-8
import collections
import random
import string
import typing
import unicodedata

import discord
from discord.ext.commands import BadArgument

from ..violetlib import checks
from ..violetlib.coglocals import *
from ..violetlib.pagination import Reactor


class MinesweeperBoard:
    __slots__ = ('tiles', 'mines', 'flags')

    # tile values
    MINE = -1
    FLAG = -2
    UNKNOWN = -3

    size = 9  # for reactions: 9 row pickers + 9 column pickers + flag + stop == 20 (20 is hard max)
    row_emoji = [f'{i}\N{COMBINING ENCLOSING KEYCAP}' for i in range(1, size + 1)]
    column_emoji = [unicodedata.lookup(f'REGIONAL INDICATOR SYMBOL LETTER {c}') for c in string.ascii_uppercase[:size]]
    tile_emoji = {MINE: '\N{BOMB}', FLAG: '\N{TRIANGULAR FLAG ON POST}', UNKNOWN: '\N{WHITE LARGE SQUARE}'}
    tile_emoji.update((i, f'{i}\N{COMBINING ENCLOSING KEYCAP}') for i in range(8))

    def __init__(self, mines: int):
        self.tiles = [self.UNKNOWN] * (self.size ** 2)
        self.mines = set(random.sample(range(len(self.tiles)), mines))
        self.flags = set()

    def render(self, flag_mode: bool) -> str:
        lines = [' '.join(self.tile_emoji[self.tiles[row * self.size + col]] for col in range(self.size))
                 for row in range(self.size)]
        header = '{} {}\n'.format(self.tile_emoji[self.FLAG if flag_mode else self.MINE], ' '.join(self.column_emoji))
        return header + '\n'.join(f'{col} {tiles}' for col, tiles in zip(self.row_emoji, lines))

    def flag(self, row: int, col: int) -> bool:
        ind = row * self.size + col
        if ind in self.flags:
            self.tiles[ind] = self.UNKNOWN
            self.flags.remove(ind)
        elif self.tiles[ind] == self.UNKNOWN:
            self.tiles[ind] = self.FLAG
            self.flags.add(ind)

        return self.flags == self.mines

    def show(self, row: int, col: int) -> bool:
        to_update = collections.deque([row * self.size + col])
        hit_mine = False
        while to_update:
            ind = to_update.popleft()
            self.flags.discard(ind)
            if self.tiles[ind] not in {self.FLAG, self.UNKNOWN}:
                continue

            edges, diags = self._neighbors(ind)
            if ind in self.mines:
                hit_mine = True
                self.tiles[ind] = self.MINE
            else:
                self.tiles[ind] = len((edges | diags) & self.mines)

            if self.tiles[ind] == 0:
                to_update.extend(edges - self.mines)
        return hit_mine

    def show_mines(self):
        for mine in self.mines:
            self.tiles[mine] = self.MINE

    def _neighbors(self, ind: int) -> typing.Tuple[typing.Set[int], typing.Set[int]]:
        row, col = divmod(ind, self.size)
        has_up = row > 0
        has_down = row < self.size - 1
        has_left = col > 0
        has_right = col < self.size - 1

        edges = set()

        if has_right:
            edges.add(ind + 1)
        if has_up:
            edges.add(ind - self.size)
        if has_left:
            edges.add(ind - 1)
        if has_down:
            edges.add(ind + self.size)

        diagonals = set()
        if has_right and has_up:
            diagonals.add(ind - self.size + 1)
        if has_up and has_left:
            diagonals.add(ind - self.size - 1)
        if has_left and has_down:
            diagonals.add(ind + self.size - 1)
        if has_down and has_right:
            diagonals.add(ind + self.size + 1)

        return (edges, diagonals)


class Games(Cog):
    flag_emoji = MinesweeperBoard.tile_emoji[MinesweeperBoard.FLAG]
    stop_emoji = '\N{BLACK SQUARE FOR STOP}'
    minesweeper_emoji = [flag_emoji, *MinesweeperBoard.column_emoji, *MinesweeperBoard.row_emoji, stop_emoji]

    @command()
    @checks.bot_permissions('add_reactions', 'embed_links')
    async def minesweeper(self, ctx: VioletContext, mine_count: int = 5):
        """ Play a game of minesweeper.
            Minesweeper is played on a 9x9 grid. The goal is to find the mines scattered randomly across the board.
            React with a column letter and row number to open a tile, which shows how many of its neighbors are mines.
            If you open a tile and it's a mine, you lose the game.
            React with the flag before the column and row to flag a tile. Flag it again to remove the flag.
            If you flag all the mines (and only the mines), you win the game.
            React with the stop button to stop the game. The game will automatically stop after 5 minutes of inactivity.
            """
        if mine_count <= 0:
            raise BadArgument("mine_count must be positive")
        elif mine_count > MinesweeperBoard.size ** 2:
            raise BadArgument("mine_count must be less than the number of tiles")

        board = MinesweeperBoard(mine_count)
        embed = discord.Embed(color=ctx.color, title='Minesweeper: Loading...',
                              description=f'Use `{ctx.clean_prefix}help minesweeper` for instructions.')
        message = await ctx.send(embed=embed)

        choice_col = choice_row = None
        flag_mode = False
        async with Reactor(self.bot, message, self.minesweeper_emoji, whitelist_caller=ctx.author,
                           timeout=300) as reactor:
            embed.title = 'Minesweeper'
            embed.description = board.render(flag_mode)
            await message.edit(embed=embed)  # Show board only when we're listening for reactions
            rerender = False
            async for reaction, _ in reactor:
                emoji = reaction.emoji
                if emoji in MinesweeperBoard.column_emoji:
                    choice_col = MinesweeperBoard.column_emoji.index(emoji)
                elif emoji in MinesweeperBoard.row_emoji:
                    choice_row = MinesweeperBoard.row_emoji.index(emoji)
                elif emoji == self.flag_emoji:
                    flag_mode = not flag_mode
                    rerender = True
                elif emoji == self.stop_emoji:
                    break

                if choice_row is not None and choice_col is not None:
                    if flag_mode:
                        won = board.flag(choice_row, choice_col)
                        lost = False
                    else:
                        lost = board.show(choice_row, choice_col)
                        won = False
                    choice_col = choice_row = None
                    flag_mode = False
                    rerender = True

                    if lost:
                        board.show_mines()
                        embed.color = 0xFF0000
                        embed.title = 'Minesweeper: you lost.'
                    elif won:
                        embed.color = 0x00FF00
                        embed.title = 'Minesweeper: you won!'

                    if won or lost:
                        embed.description = board.render(flag_mode)  # Rerender manually before breaking
                        await message.edit(embed=embed)
                        break

                if rerender:
                    embed.description = board.render(flag_mode)
                    await message.edit(embed=embed)
                    rerender = False

    minesweeper.example_usage = """
    `{prefix}minesweeper`: start a game of minesweeper
    """
