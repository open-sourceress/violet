# coding=utf-8
import itertools
import typing

import asyncpg
import discord
from discord.ext.commands import BadArgument

from ..violetlib import checks, VioletCommand
from ..violetlib.coglocals import *
from . import responses


def make_responses(parent: VioletCommand, command_names: typing.List[str], setting_kind: str, kind_message: str,
                   get_target_id: typing.Callable[[VioletContext], int], setting_key: str) -> VioletCommand:
    setting_name = ' '.join(setting_key.capitalize().split('_'))

    base_command_help = f"""
    Show and manage {setting_name.lower()} settings {kind_message}.
    Each value this setting can have is a subcommand. The subcommand pages list all possible values.
    If called without a subcommand, the current setting value is shown.
    """

    @parent.group(command_names[0], aliases=command_names[1:], help=base_command_help)
    async def base_command(self, ctx: VioletContext):
        await self._show_response(ctx, setting_kind, get_target_id(ctx), setting_key, kind_message)

    base_command.example_usage = f"""
    `{{prefix}}{base_command.qualified_name}`: show the current {setting_name.lower()} setting
    `{{prefix}}{base_command.qualified_name} messages`: send messages for {setting_name.lower()}
    `{{prefix}}{base_command.qualified_name} reactions`: add reactions for {setting_name.lower()}
    `{{prefix}}{base_command.qualified_name} disable`: disable {setting_name.lower()}
    `{{prefix}}{base_command.qualified_name} reset`: reset {setting_name.lower()} settings to default
    """

    enable_messages_help = f"""
    Enable {setting_name.lower()} {kind_message} and let it send messages.
    Messages in channels where I don't have permission to send messages will be ignored.
    """

    @base_command.command('messages', aliases=['enable', 'enabled', 'on', 'send', 'message'], help=enable_messages_help)
    async def enable_messages(self, ctx: VioletContext):
        await self._response_setting(ctx, setting_kind, get_target_id(ctx), setting_key,
                                     responses.ResponseSetting.enabled_messages)

    enable_messages.example_usage = f"""
    `{{prefix}}{enable_messages.qualified_name}`: send messages for {setting_name.lower()}
    """

    enable_reactions_help = f"""
    Enable {setting_name.lower()} {kind_message} and let it add reactions.
    Messages in channels where I don't have permission to add reactions will be ignored.
    """

    @base_command.command('reactions', aliases=['react'], help=enable_reactions_help)
    async def enable_reactions(self, ctx: VioletContext):
        await self._response_setting(ctx, setting_kind, get_target_id(ctx), setting_key,
                                     responses.ResponseSetting.enabled_reactions)

    enable_reactions.example_usage = f"""
    `{{prefix}}{enable_reactions.qualified_name}`: add reactions for {setting_name.lower()}
    """

    disable_help = f"""
    Disable {setting_name.lower()} {kind_message}.
    """

    @base_command.command('disable', aliases=['disabled', 'off'], help=disable_help)
    async def disable(self, ctx: VioletContext):
        await self._response_setting(ctx, setting_kind, get_target_id(ctx), setting_key,
                                     responses.ResponseSetting.disabled)

    disable.example_usage = f"""
    `{{prefix}}{disable.qualified_name}`: disable {setting_name.lower()}
    """

    reset_help = f"""
    Reset {setting_name.lower()} settings {kind_message} to the default value.
    The default is disabled for guilds, enabled for users, and no override for channels.
    """

    @base_command.command('reset', aliases=['default'], help=reset_help)
    async def reset(self, ctx: VioletContext):
        await self._response_setting(ctx, setting_kind, get_target_id(ctx), setting_key, None)

    reset.example_usage = f"""
    `{{prefix}}{reset.qualified_name}`: reset {setting_name.lower()} to default
    """

    return base_command


class Settings(Cog):
    prefix_len = 16
    _str_response_values = ('not set', 'disabled', 'enabled (reactions)', 'enabled (messages)')  # 0 is channel not set

    @property
    def _responses(self) -> 'responses.Responses':
        return self.bot.get_cog('Responses')

    @group(aliases=['prefixes'])
    @checks.bot_permissions('embed_links')
    async def prefix(self, ctx: VioletContext) -> None:
        """ List my prefixes in this guild.
            By default, my prefixes are `v!` and `V!`. Those can be removed, and others can be added.
            You can also use my mention as a prefix, regardless of what the configured prefixes are."""
        guild_name, guild_id = ('DMs', ctx.author.id) if ctx.guild is None else (ctx.guild.name, ctx.guild.id)
        embed = discord.Embed(title=f'Prefixes in {guild_name}', color=ctx.color,
                              description='\n'.join(prefix or '<no prefix>' for prefix in ctx.bot.prefixes[guild_id]))
        embed.set_author(name=str(ctx.author), icon_url=ctx.author.avatar_url_as(format='png', size=64))
        embed.set_footer(text='You can also mention me to use my commands',
                         icon_url=ctx.me.avatar_url_as(format='png', size=64))
        await ctx.send(embed=embed)

    prefix.example_usage = """
    `{prefix}prefix`: list my prefixes
    """

    @prefix.command('add')
    @checks.author_permissions('manage_messages')
    async def prefix_add(self, ctx: VioletContext, *, prefix: str) -> None:
        """ Add a prefix for this guild.
            Prefixes must be at most 16 characters, and one prefix can't be added multiple times."""
        guild_id = ctx.author.id if ctx.guild is None else ctx.guild.id
        prefixes: typing.MutableSequence[str] = ctx.bot.prefixes[guild_id]
        if prefix in prefixes:
            raise BadArgument("That's already my prefix")
        elif len(prefix) > self.prefix_len:
            raise BadArgument(f'Prefixes must be at most {self.prefix_len} characters')

        prefixes.append(prefix)
        conn: asyncpg.Connection
        async with self.acquire_db() as conn:
            await conn.execute('INSERT INTO prefixes VALUES ($1, $2)'
                               'ON CONFLICT (guild_id) DO UPDATE SET prefixes = $2;', guild_id, prefixes)
        await ctx.send('Prefix added!')

    prefix_add.example_usage = """
    `{prefix}prefix add boop`: add "boop" as a prefix
    """

    @prefix.command('remove')
    @checks.author_permissions('manage_messages')
    async def prefix_remove(self, ctx: VioletContext, *, prefix: str) -> None:
        """ Remove a prefix for this guild.
            If you remove all my prefixes, you can still mention me to use my commands."""
        guild_id = ctx.author.id if ctx.guild is None else ctx.guild.id
        prefixes: typing.MutableSequence[str] = ctx.bot.prefixes[guild_id]
        if prefix not in prefixes:
            raise BadArgument('That isn\'t my prefix.')

        prefixes.remove(prefix)
        conn: asyncpg.Connection
        async with self.acquire_db() as conn:
            await conn.execute('INSERT INTO prefixes VALUES ($1, $2)'
                               'ON CONFLICT (guild_id) DO UPDATE SET prefixes = $2;', guild_id, prefixes)
        await ctx.send('Prefix removed!')

    prefix_remove.example_usage = """
    `{prefix}prefix remove boop`: remove "boop" from my prefixes
    """

    @checks.guild_only()
    @checks.bot_permissions('embed_links')
    @group('guildsettings', aliases=['settings', 'gsettings'])
    async def guild_settings(self, ctx: VioletContext):
        """ Show and manage settings for this guild.
            Each setting has its own subcommand. The subcommand pages list every guild setting.
            When called without a setting name, the guild's settings are listed.
            """
        embed = self._response_embed(ctx, f'in {ctx.guild}', self._responses.get_settings('guild', ctx.guild.id))
        embed.description = f'Prefixes: {", ".join(self.bot.prefixes[ctx.guild.id])}\n{embed.description}'
        await ctx.send(embed=embed)

    @checks.guild_only()
    @checks.bot_permissions('embed_links')
    @group('channelsettings', aliases=['csettings'])
    async def channel_settings(self, ctx: VioletContext):
        """ Show and manage settings for this channel.
            Each setting has its own subcommand. The subcommand pages list every channel setting.
            Channel settings override guild settings. When not set for a channel, the guild's value is used.
            For example, if the guild has longcat disabled and the channel has it send messages, messages will be sent.
            """
        embed = self._response_embed(ctx, f'in #{ctx.channel}', self._responses.get_settings('channel', ctx.channel.id))
        await ctx.send(embed=embed)

    @checks.bot_permissions('embed_links')
    @group('usersettings', aliases=['usettings'])
    async def user_settings(self, ctx: VioletContext):
        """ Show and manage your settings.
            Each setting has its own subcommand. The subcommand pages list every user setting.
            User settings are combined with channel/guild settings so that the lowest level of the two is used.
            For example, if a user disables dad jokes and the channel enables them, that user won't be dad-joked.
            """
        embed = self._response_embed(ctx, f'for {ctx.author}', self._responses.get_settings('user', ctx.author.id))
        await ctx.send(embed=embed)

    # (guild|channel|user) settings, (dad jokes|longcats), (messages|reactions|disabled|<no subcmd>) - 24 commands total
    # The only sane thing to do is generate them dynamically
    command_groups = [
        (guild_settings, 'guild', 'in this guild', lambda ctx: ctx.guild.id),
        (channel_settings, 'channel', 'in this channel', lambda ctx: ctx.channel.id),
        (user_settings, 'user', 'for you', lambda ctx: ctx.author.id),
    ]
    setting_kinds = [
        (['dadjokes', 'dadjoke'], 'dad_jokes'),
        (['longcats', 'longcat'], 'long_cats'),
        (['hats', 'hat', 'hathat', 'hathats'], 'hats'),
    ]

    # Build subcommands for each root command
    for (parent, kind, kind_msg, get_target), (command_names, key) in itertools.product(command_groups, setting_kinds):
        make_responses(parent, command_names, kind, kind_msg, get_target, key)

    # Add subcommands to class attribute dictionary as if they were created normally
    for cmd in {cmd for parent in (guild_settings, channel_settings, user_settings) for cmd in parent.walk_commands()}:
        locals()[cmd.qualified_name.replace(' ', '_')] = cmd

    # Add guild-only check for commands that only make sense in guilds
    for cmd in {cmd for parent in (guild_settings, channel_settings) for cmd in parent.walk_commands()}:
        checks.guild_only()(cmd)

    # Add Manage Messages check for commands that affect more than one user (setters aside from usersettings)
    for cmd in {cmd for root in (guild_settings, channel_settings) for kind in root.commands for cmd in kind.commands}:
        checks.author_permissions('manage_messages')(cmd)

    # At the end of iteration, these variables still exist in the class namespace
    del command_groups, setting_kinds, parent, kind, kind_msg, get_target, command_names, key, cmd

    def _response_embed(self, ctx: VioletContext, embed_title: str, settings: responses.AnyResponses) -> discord.Embed:
        setting_keys = [name for cls in reversed(type(settings).__mro__) for name in getattr(cls, '__slots__', ())]
        setting_names = [' '.join(key.capitalize().split('_')) for key in setting_keys]
        embed = discord.Embed(color=ctx.color, title=f'Settings {embed_title}')
        embed.description = '\n'.join(f'{name}: {self._str_response_values[getattr(settings, key) or 0]}'
                                      for name, key in zip(setting_names, setting_keys))
        return embed

    async def _show_response(self, ctx: VioletContext, kind: str, target_id: int, key: str, kind_message: str):
        name = ' '.join(key.capitalize().split('_'))
        value = getattr(self._responses.get_settings(kind, target_id), key)
        await ctx.send('{} are {} {}.', name, self._str_response_values[value or 0], kind_message)

    async def _response_setting(self, ctx: VioletContext, kind: str, target_id: int, key: str,
                                value: typing.Optional[responses.ResponseSetting]):
        await self._responses.set_settings(kind, target_id, **{key: value})
        await ctx.send('Settings configured.')
