# coding=utf-8
import json
import typing

import aiohttp
import discord
from discord.ext.commands import BadArgument

from ..violetlib import checks, iterutils
from ..violetlib.coglocals import *


class TOA(Cog):
    toa_color = discord.Color(0xF89808)
    config: 'TOAConfig'

    def __init__(self, bot: Violet, config: 'TOAConfig'):
        super().__init__(bot, config)
        self.session = aiohttp.ClientSession(loop=bot.loop,
                                             headers={
                                                 'X-TOA-Key': config.api_key,
                                                 'X-Application-Origin': config.api_origin,
                                                 'Content-Type': 'application/json'
                                             })

    async def setup_db(self):
        async with self.acquire_db() as conn:
            await conn.execute('CREATE SCHEMA IF NOT EXISTS toa;')
            await conn.execute('CREATE TABLE IF NOT EXISTS toa.teams (number int PRIMARY KEY NOT NULL,'
                               'key text NOT NULL, short_name text, long_name text, region_key text NOT NULL,'
                               'rookie_year int NOT NULL, latest_year int NOT NULL, city text NOT NULL,'
                               'state_prov text NOT NULL, country TEXT NOT NULL, website text);')
        self.bot.loop.create_task(self._preload_caches())

    async def _request(self, url: str) -> typing.Union[typing.List, typing.Mapping, None]:
        async with self.session.get(self.config.api_base_url + url, headers=None) as resp:
            resp.raise_for_status()
            response = json.loads(await resp.text())

        return response

    async def _preload_caches(self):
        self.logger.debug('Spawning cache preload tasks')
        self.bot.loop.create_task(self._preload_teams())

    async def _preload_teams(self):
        team_data = await self._request('/team/')
        self.logger.debug('Downloaded data for %r teams', len(team_data))
        async with self.acquire_db() as conn, conn.transaction():
            await conn.execute('TRUNCATE toa.teams;')  # Purge and reinsert is simpler than upsert
            status = await conn.execute('''
            INSERT INTO toa.teams (
                number, key, short_name, long_name, region_key, rookie_year, latest_year, city, state_prov, country,
                website
            ) SELECT team_number, team_key, team_name_short, team_name_long, region_key, rookie_year,
                     2000 + right(last_active, 2)::int, city, state_prov, country, website 
            FROM jsonb_to_recordset($1) AS toa_team (
                team_number int, team_key text, team_name_short text, team_name_long text, region_key text,
                rookie_year int, last_active text, city text, state_prov text, country text, website text
            );
            ''', team_data)
        rows_updated = int(status.rpartition(' ')[2])
        self.logger.debug('Unpacked updated data for %r teams', rows_updated)

    @group()
    @checks.bot_permissions('embed_links')
    async def toa(self, ctx: VioletContext, team_number: int = None):
        """ Fetch FIRST Tech Challenge data from The Orange Alliance.
            If no arguments are passed, TOA's status is shown.
            If one argument is passed, the team with that key/number is shown.
            """
        if team_number is not None:
            await ctx.invoke(self.toa_team, team_number)
            return
        status = await self._request('/')
        embed = discord.Embed(color=self.toa_color, title='The Orange Alliance')
        embed.description = f'Version {status["version"]}'
        await ctx.send(embed=embed)

    toa.example_usage = """
    `{prefix}toa`: show TOA's current status
    `{prefix}toa 15286`: show information about team 15286, Shark Byte Robotics
    """

    @toa.command('team')
    @checks.bot_permissions('embed_links')
    async def toa_team(self, ctx: VioletContext, team_number: int):
        """Fetch data on an FTC team by number."""
        async with self.acquire_db() as conn:
            team = await conn.fetchrow("""
            -- Include short name in the title if it exists
            SELECT key, 'FTC Team ' || number || coalesce(': ' || short_name, '') AS embed_title,
            -- long_name if it exists, NULL if it doesn't but short_name does, 'Unknown name' if neither do
            nullif(coalesce(long_name, short_name, 'Unknown name'), short_name) AS long_name,
            rookie_year || '-' || latest_year AS years,
            format('%s, %s, %s (%s region)', city, state_prov, country, region_key) AS location, website
            FROM toa.teams WHERE number = $1;""", team_number)
        if team is None:
            raise BadArgument("That team doesn't exist")
        embed = discord.Embed(color=self.toa_color, title=team['embed_title'], description=team['long_name'],
                              url=f"https://theorangealliance.org/teams/{team['key']}")
        embed.add_field(name='Years', value=team['years'])
        embed.add_field(name='Location', value=team['location'])
        if team['website']:
            embed.add_field(name='Website', value=team['website'])
        await ctx.send(embed=embed)

    toa_team.example_usage = """
    `{prefix}toa team 3231`: show information about team 3231, CONTINUUM
    """

    @toa.command('search')
    @checks.bot_permissions('embed_links')
    async def toa_search(self, ctx: VioletContext, *, name: str):
        """ Search for teams by name.
            Teams whose names contain the given text (or something similar) are listed.
            """
        async with self.acquire_db() as conn:
            records = await conn.fetch('SELECT number, coalesce(short_name, long_name) FROM toa.teams '
                                       'WHERE word_similarity(lower($1), lower(coalesce(short_name, long_name))) > 0.6 '
                                       'ORDER BY word_similarity(lower($1), lower(coalesce(short_name, long_name))) '
                                       'DESC, number;', name)
        if not records:
            await ctx.send('No matching teams found.')
            return

        pages = iterutils.segment(enumerate(records, 1), 10)
        embeds = [discord.Embed(color=self.toa_color, title='Team Search Results',
                                description='\n'.join(f'#{i}: Team #{num}, {name}' for i, (num, name) in page))
                  for page in pages]
        if len(embeds) == 1:
            await ctx.send(embed=embeds[0])
        else:
            await ctx.paginate([em.set_footer(text=f'Page {i} of {len(pages)}') for i, em in enumerate(embeds, 1)])

    toa_search.example_usage = """
    `{prefix}toa search cube`: list teams with "cube" (or "cubed" or "cuber" etc.) in their name
    """


class TOAConfig(Config):
    api_key: str
    api_origin: str = 'Violet'
    api_base_url: str = 'https://theorangealliance.org/api'
