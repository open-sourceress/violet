# coding=utf-8
import asyncio
import typing
from datetime import timedelta

import discord
from discord.ext.commands import BadArgument

from ..violetlib import checks, cleaners, convert, format
from ..violetlib.coglocals import *


class Timers(Cog):
    handles: typing.MutableMapping[int, asyncio.Task]

    def __init__(self, bot: Violet, config):
        super().__init__(bot, config)
        self.handles = {}
        bot.loop.create_task(self.start_timers())

    async def setup_db(self):
        async with self.acquire_db() as conn:
            await conn.execute('CREATE TABLE IF NOT EXISTS timers (id serial PRIMARY KEY NOT NULL,'
                               'user_id int8 NOT NULL, channel_id int8 NOT NULL, end_time timestamptz NOT NULL,'
                               'message varchar(2000) NOT NULL);')

    async def start_timers(self):
        await self.bot.wait_until_ready()
        loop: asyncio.AbstractEventLoop = self.bot.loop
        async with self.acquire_db() as conn:
            for (timer_id, user_id, channel_id, remaining, message) in await conn.fetch(
                    'SELECT id, user_id, channel_id, end_time - now(), message FROM timers;'):
                self.handles[timer_id] = loop.create_task(
                    self.show_timer(timer_id, user_id, channel_id, remaining, message))

    async def show_timer(self, timer_id: int, user_id: int, channel_id: int, delay: timedelta, message: str):
        seconds = delay.total_seconds()
        self.logger.debug('Starting timer #%d (user_id=%d, channel_id=%d, remaining=%d, message=%r)',
                          timer_id, user_id, channel_id, seconds, message)
        if seconds > 0:
            await asyncio.sleep(seconds)
        self.logger.debug('Timer #%d elapsed', timer_id)

        elapsed_str = 'has elapsed' if seconds >= 0 else f'is {format.timedelta(-delay)} overdue'

        author: discord.User = self.bot.get_user(user_id)
        if author is None:
            return

        dest = self.bot.get_channel(channel_id)
        if dest is None:
            dest = author

        await dest.send(f'{author.mention}, timer #{timer_id} {elapsed_str}: {message}')

        async with self.acquire_db() as conn:
            await conn.execute('DELETE FROM timers WHERE id = $1;', timer_id)

    @group(aliases=['reminder', 'rem'])
    async def timer(self, ctx: VioletContext, duration: convert.timedelta, *, message: str = 'no message'):
        """ Set a timer, which will mention you when it ends.
            See the example usage for how to specify the duration.
            Every timer has an ID, which is unique to that timer. The ID lets you cancel a timer later.
            If I'm offline when your timer ends, I'll notify you when I start up and say how long ago the timer ended.
            """
        message = cleaners.format(ctx, '{}', message)
        async with self.acquire_db() as conn:
            timer_id: int = await conn.fetchval('INSERT INTO timers (user_id, channel_id, end_time, message)'
                                                'VALUES ($1, $2, now() + $3, $4) RETURNING id;', ctx.author.id,
                                                ctx.channel.id, duration, message)
        loop: asyncio.AbstractEventLoop = self.bot.loop
        self.handles[timer_id] = loop.create_task(
            self.show_timer(timer_id, ctx.author.id, ctx.channel.id, duration, message))
        await ctx.send('Timer set for {} (ID {}).', format.timedelta(duration), timer_id)

    timer.example_usage = """
    `{prefix}timer 2s`: set a timer for 2 seconds, with no message
    `{prefix}timer 5m example message`: set a timer for 5 minutes, with the message "example message"
    `{prefix}timer 3m30s another example message`: set a 3.5 minute timer with the message "another example message"
    `{prefix}timer 1.5d10h120m I like complicated durations`: set a 2-day timer, oddly, with an accurate message
    `{prefix}timer "2 minutes, 30 seconds" long time format`: set a timer for 2.5 minutes, using the longer format
    """

    @timer.command('list')
    @checks.bot_permissions('embed_links')
    async def timer_list(self, ctx: VioletContext):
        """List all your running timers."""
        async with self.acquire_db() as conn:
            timers = await conn.fetch('SELECT id, end_time - now(), message FROM timers WHERE user_id = $1 '
                                      'ORDER BY id;', ctx.author.id)
        embed = discord.Embed(color=ctx.color)
        if timers:
            embed.description = '\n'.join(
                f'#{timer_id}: {message} (in {format.timedelta(remaining)})' for timer_id, remaining, message in
                timers)
        else:
            embed.description = 'No timers set.'
        await ctx.send(embed=embed)

    timer_list.example_usage = """
    `{prefix}timer list`: list your timers
    """

    @timer.command('cancel')
    async def timer_cancel(self, ctx: VioletContext, timer_id: int):
        """ Cancel a timer.
            Cancelling a timer will prevent the message when it ends, and remove it from the list."""
        async with self.acquire_db() as conn:
            status: str = await conn.execute('DELETE FROM timers WHERE id = $1 AND user_id = $2;',
                                             timer_id, ctx.author.id)
        deleted = int(status.rpartition(' ')[2])
        if deleted:
            self.handles.pop(timer_id).cancel()
            await ctx.send('Timer cancelled')
        else:
            raise BadArgument("That timer isn't yours")

    timer_cancel.example_usage = """
    `{prefix}timer cancel 1`: cancel timer #1, if it's yours
    """

    @timer.command('edit', aliases=['editmessage', 'editmsg'])
    async def timer_edit(self, ctx: VioletContext, timer_id: int, *, message: str = 'no message'):
        """Edit a timer's message."""
        message = cleaners.format(ctx, '{}', message)
        async with self.acquire_db() as conn:
            record: typing.Optional[typing.Tuple[int, timedelta]] = await conn.fetchrow(
                'UPDATE timers SET message = $3 WHERE id = $1 AND user_id = $2 RETURNING channel_id, end_time - now();',
                timer_id, ctx.author.id, message)
        if record is not None:
            channel_id, duration = record
            self.handles.pop(timer_id).cancel()
            self.handles[timer_id] = self.bot.loop.create_task(
                self.show_timer(timer_id, ctx.author.id, channel_id, duration, message))
            await ctx.send('Message edited.')
        else:
            raise BadArgument("That timer isn't yours")

    timer_edit.example_usage = """
    `{prefix}timer edit 1 updated message`: set timer #1's message to "updated message", if it's yours
    """

    @timer.command('reschedule', aliases=['editduration', 'edittime'])
    async def timer_reschedule(self, ctx: VioletContext, timer_id: int, *, duration: convert.timedelta):
        """ Reschedule a timer for a new time.
            The new duration is based on the current time, not the time the timer was originally set."""
        async with self.acquire_db() as conn:
            record: typing.Optional[typing.Tuple[int, str]] = await conn.fetchrow(
                'UPDATE timers SET end_time = now() + $3 WHERE id = $1 AND user_id = $2 RETURNING channel_id, message;',
                timer_id, ctx.author.id, duration)
        if record is not None:
            channel_id, message = record
            self.handles.pop(timer_id).cancel()
            self.handles[timer_id] = self.bot.loop.create_task(
                self.show_timer(timer_id, ctx.author.id, channel_id, duration, message))
            await ctx.send('Timer rescheduled for {} from now.', format.timedelta(duration))
        else:
            raise BadArgument("That timer isn't yours")

    timer_reschedule.example_usage = """
    `{prefix}timer reschedule 1 4h`: update timer #1 to end in 4 hours, if it's yours
    `{prefix}timer reschedule 2 6d23.25h44m60s`: reschedule timer #2 for a week from now, in a strange but valid way
    `{prefix}timer reschedule 3 4 days, 5 seconds`: reschedule timer #3 using long duration format
    """
