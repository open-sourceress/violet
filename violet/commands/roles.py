# coding=utf-8
import asyncio
import typing

import discord
from discord.ext.commands import BadArgument, Converter, Greedy, MemberConverter, RoleConverter

from ..violetlib import checks, convert
from ..violetlib.coglocals import *


class RoleTarget(Converter):
    member_converter = MemberConverter()
    role_converter = RoleConverter()

    async def convert(self, ctx: VioletContext, argument: str) -> typing.Collection[discord.Member]:
        try:
            member = await self.member_converter.convert(ctx, argument)
        except BadArgument:
            pass
        else:
            return [member]

        if argument.startswith('*'):
            try:
                role = await self.role_converter.convert(ctx, argument[1:])
            except BadArgument:
                raise BadArgument("That member/role doesn't exist")
            else:
                return role.members
        else:
            raise BadArgument("That member doesn't exist")


class GivemeRole(convert.BotUsableRole):
    async def convert(self, ctx: VioletContext, argument: str) -> discord.Role:
        role = await super().convert(ctx, argument)
        is_giveme: int = await ctx.bot.pool.fetchval('SELECT count(*) FROM giveme_roles WHERE role_id = $1;', role.id)
        if not is_giveme:
            raise BadArgument(f'`{role.name}` is not a giveme role')
        else:
            return role


if typing.TYPE_CHECKING:
    RoleTarget = typing.Collection[discord.Member]
    GivemeRole = discord.Role


class Roles(Cog):
    async def setup_db(self):
        async with self.acquire_db() as conn:
            await conn.execute('CREATE TABLE IF NOT EXISTS giveme_roles (role_id int8 PRIMARY KEY NOT NULL,'
                               'guild_id int8 NOT NULL, role_name varchar(32) NOT NULL);')

    @command()
    @checks.guild_only()
    @checks.bot_permissions('manage_roles')
    @checks.author_permissions('manage_roles')
    async def give(self, ctx: VioletContext, members: Greedy[RoleTarget], *roles: convert.UsableRole):
        """ Give one or more roles to one or more members.
            You can put multiple users, as well as an asterisk and a role name to specify all of that role's members.
            If members or roles contain spaces, you must put the name in quotes or use the user/role's ID instead.
            All roles must be below your top role and my top role.
            """
        all_members: typing.Set[discord.Member] = {member for member_set in members for member in member_set}
        async with ctx.typing():
            await asyncio.gather(
                *[member.add_roles(*roles, reason=f'Given by {ctx.author}', atomic=False) for member in all_members])
        await ctx.send('{} role(s) given to {} member(s).', len(roles), len(all_members))

    give.example_usage = """
    `{prefix}give {ctx.author} Example Role`: give yourself two roles, named "Example" and "Role"
    `{prefix}give {ctx.author} "Example Role"`: give yourself one role named "Example Role"
    `{prefix}give {ctx.author} {ctx.me} "Example Role"`: give both of us a role named "Example Role"
    `{prefix}give *Role1 Role2`: give every member of the role "Role1" a role named "Role2"
    `{prefix}give *Role1 *Role2 {ctx.me} Role3`: give every member in "Role1" or "Role2", and me, a role named "Role3"
    `{prefix}give "*Role 1" "Role 2"`: give every member of the role "Role 1" a role named "Role 2"
    """

    @command()
    @checks.guild_only()
    @checks.bot_permissions('manage_roles')
    @checks.author_permissions('manage_roles')
    async def take(self, ctx: VioletContext, members: Greedy[RoleTarget], *roles: convert.UsableRole):
        """ Take one or more roles from one or more members.
            You can put multiple users, as well as an asterisk and a role name to specify all of that role's members.
            If members or roles contain spaces, you must put the name in quotes or use the user/role's ID instead.
            All roles must be below your top role and my top role.
            """
        all_members: typing.Set[discord.Member] = {member for member_set in members for member in member_set}
        async with ctx.typing():
            await asyncio.gather(
                *[member.remove_roles(*roles, reason=f'Taken by {ctx.author}', atomic=False) for member in all_members])
        await ctx.send('{} role(s) taken from {} member(s).', len(roles), len(all_members))

    take.example_usage = """
    `{prefix}take {ctx.author} Example Role`: take two roles, named "Example" and "Role", from yourself
    `{prefix}take {ctx.author} "Example Role"`: take one role named "Example Role" from yourself
    `{prefix}take {ctx.author} {ctx.me} "Example Role"`: take a role named "Example Role" from both of us
    `{prefix}take *Role1 Role2`: take a role named "Role2" from every member of the role "Role1"
    `{prefix}take *Role1 *Role2 {ctx.me} Role3`: take Role3 from everyone in Role1 or Role2, and me
    `{prefix}take "*Role 1" "Role 2"`: take "Role 2" from everyone in "Role 1" 
    """

    @group(aliases=['optin'])
    @checks.guild_only()
    @checks.bot_permissions('manage_roles')
    async def giveme(self, ctx: VioletContext, *roles: GivemeRole):
        """ Give you the specified giveme roles.
            All specified roles must be listed as giveme roles and below my top role.
            """
        roles = set(roles)
        await ctx.author.add_roles(*roles, reason='Giveme request')
        await ctx.send('Gave you {} role(s).', len(roles))

    giveme.example_usage = """
    `{prefix}giveme foo bar`: give yourself two giveme roles, "foo" and "bar"
    `{prefix}giveme "foo bar"`: give yourself one giveme role, "foo bar"
    `{prefix}giveme "foo bar" baz`: give yourself two giveme roles, "foo bar" and "baz"
    """

    @giveme.command('take')
    @checks.guild_only()
    @checks.bot_permissions('manage_roles')
    async def giveme_take(self, ctx: VioletContext, *roles: GivemeRole):
        """ Take the specified giveme roles from you.
            All specified roles must be listed as giveme roles and below my top role.
            """
        roles = set(roles)
        await ctx.author.remove_roles(*roles, reason='Giveme take request')
        await ctx.send('Took {} role(s).', len(roles))

    giveme_take.example_usage = """
    `{prefix}giveme take foo bar`: take two giveme roles, "foo" and "bar", from yourself
    `{prefix}giveme take "foo bar"`: take one giveme role, "foo bar", from yourself
    `{prefix}giveme take "foo bar" baz`: take two giveme roles, "foo bar" and "baz", from yourself
    """

    @giveme.command('add')
    @checks.guild_only()
    @checks.author_permissions('manage_roles')
    async def giveme_add(self, ctx: VioletContext, *, role: convert.UsableRole):
        """ Make an existing role a giveme role.
            The role must be below your top role and my top role.
            """
        async with self.acquire_db() as conn:
            already_exists = await conn.fetchval('SELECT count(*) FROM giveme_roles '
                                                 'WHERE guild_id = $1 AND role_name = $2;', ctx.guild.id, role.name)
            if already_exists:
                raise BadArgument('A giveme role with that name already exists')
            await conn.execute('INSERT INTO giveme_roles VALUES ($1, $2, $3);', role.id, ctx.guild.id, role.name)
        await ctx.send('Giveme role added. Use `{}giveme "{}"` to get it.', ctx.prefix, role.name)

    giveme_add.example_usage = """
    `{prefix}giveme add foo bar`: make a role named "foo bar" a giveme role
    """

    @giveme.command('create')
    @checks.guild_only()
    @checks.bot_permissions('manage_roles')
    @checks.author_permissions('manage_roles')
    async def giveme_create(self, ctx: VioletContext, *, role_name: str):
        """ Create a role and make it a giveme role.
            This is a shortcut for creating a role and calling `giveme add`.
            """
        async with self.acquire_db() as conn:
            already_exists = await conn.fetchval('SELECT count(*) FROM giveme_roles '
                                                 'WHERE guild_id = $1 AND role_name = $2;', ctx.guild.id, role_name)
            if already_exists:
                raise BadArgument('A giveme role with that name already exists')

            role = await ctx.guild.create_role(name=role_name, reason=f'Giveme created by {ctx.author}')

            await conn.execute('INSERT INTO giveme_roles VALUES ($1, $2, $3);', role.id, ctx.guild.id, role.name)
        await ctx.send('Giveme role created. Use `{}giveme "{}"` to get it.', ctx.prefix, role.name)

    giveme_create.example_usage = """
    `{prefix}giveme create foo bar`: create a role named "foo bar" and make it a giveme role
    """

    @giveme.command('remove', aliases=['unlist', 'unadd'])
    @checks.guild_only()
    @checks.author_permissions('manage_roles')
    async def giveme_remove(self, ctx: VioletContext, *, role: GivemeRole):
        """ Remove a role from this guild's givemes.
            This will not delete the role or remove it from any members.
            """
        async with self.acquire_db() as conn:
            await conn.execute('DELETE FROM giveme_roles WHERE guild_id = $1 AND role_id = $2;', ctx.guild.id, role.id)
        await ctx.send('Giveme removed from the list.')  # Clarify that you didn't just take the role from yourself

    giveme_remove.example_usage = """
    `{prefix}giveme remove foo bar`: remove a giveme role named "foo bar" from the list
    """

    @giveme.command('delete')
    @checks.guild_only()
    @checks.bot_permissions('manage_roles')
    @checks.author_permissions('manage_roles')
    async def giveme_delete(self, ctx: VioletContext, *, role: GivemeRole):
        """Delete a role and remove it from this guild's givemes."""
        async with self.acquire_db() as conn:
            existed = await conn.fetchrow('DELETE FROM giveme_roles WHERE guild_id = $1 AND role_id = $2'
                                          'RETURNING role_id;', ctx.guild.id, role.id)
        if existed:
            await role.delete(reason=f'Giveme deleted by {ctx.author}')
            await ctx.send('Role deleted.')
        else:
            raise BadArgument(f'`{role.name}` is not a giveme role')

    giveme_delete.example_usage = """
    `{prefix}giveme delete foo bar`: delete a giveme role named "foo bar" and remove it from the list
    """

    @giveme.command('list', aliases=['show'])
    @checks.guild_only()
    @checks.bot_permissions('embed_links')
    async def giveme_list(self, ctx: VioletContext):
        """List the giveme roles in this guild."""
        role_names: typing.Sequence[typing.Tuple[str]]
        async with self.acquire_db() as conn:
            role_names = await conn.fetch('SELECT role_name FROM giveme_roles WHERE guild_id = $1'
                                          'ORDER BY lower(role_name) ASC, role_name DESC;', ctx.guild.id)
        if role_names:
            await ctx.send(embed=discord.Embed(color=ctx.color, title='Giveme roles',
                                               description='\n'.join(name for (name,) in role_names)))
        else:
            await ctx.send('This guild has no giveme roles.')

    giveme_list.example_usage = """
    `{prefix}giveme list`: list the available givemes
    """
