# coding=utf-8
import importlib
import logging
import pathlib
import pkgutil
import sys
from logging.handlers import TimedRotatingFileHandler

import aiohttp

try:
    import click

    click_version = tuple(map(int, click.__version__.split('.')))
    if click_version < (7, 0):
        raise ImportError
except ImportError:
    sys.exit('click 7.0 or higher must be installed')


@click.group(invoke_without_command=True)
@click.pass_context
def main(ctx):
    """Starts the bot."""
    if ctx.invoked_subcommand is None:
        run_bot()


@main.command()
@click.pass_context
def checkenv(ctx):
    """Checks that Violet can run in this environment."""
    _check_version(ctx, 'Python', (3, 6), sys.version_info[:3])

    asyncpg = _check_dependency(ctx, 'asyncpg', (0, 16))
    discord = _check_dependency(ctx, 'discord', (1, 5), 'discord.py')
    pillow = _check_dependency(ctx, 'PIL', (5, 3), 'pillow')
    pint = _check_dependency(ctx, 'pint', (0, 8, 1))
    toml = _check_dependency(ctx, 'toml', (0, 10))

    try:
        with open('config.toml') as f:
            cfg_dict = toml.load(f)
    except FileNotFoundError:
        click.echo('config.toml does not exist.', file=sys.stderr)
        return ctx.abort()

    _setup_cog_config()
    try:
        from .bot import VioletConfig
        config = VioletConfig('', cfg_dict)
    except KeyError as e:
        click.echo('Error in config.toml: {}: {}'.format(type(e).__qualname__, e), file=sys.stderr)
        return ctx.abort()

    import asyncio
    loop = asyncio.get_event_loop()
    conn = None
    try:
        conn = loop.run_until_complete(asyncpg.connect(**config.postgres.connection_args()))
    except OSError as e:
        click.echo('Error connecting to Postgres: {}: {}'.format(type(e).__qualname__, e), file=sys.stderr)
        return ctx.abort()
    finally:
        if conn is not None:
            loop.run_until_complete(conn.close())

    click.echo('Environment seems to be set up correctly.')


@main.command()
@click.pass_context
def create_home_guild(ctx):
    """Creates and sets up a home guild."""
    import asyncio
    import discord
    import toml
    client = discord.Client()

    @client.event
    async def on_ready():
        icon = await client.user.avatar_url_as(format='png').read()
        home_guild = await client.create_guild('{} Home'.format(client.user.name), icon=icon)
        click.echo('Home guild created with ID {}'.format(home_guild.id))
        try:
            channel = await client.wait_for('guild_channel_create', timeout=10,
                                            check=lambda channel: channel.guild.id == home_guild.id)
        except asyncio.TimeoutError:
            # Started listening too late; get the new guild and its channel
            channel = client.get_guild(home_guild.id).text_channels[0]
        invite = await channel.create_invite(max_uses=1, reason='Bot owner invite')
        click.echo('Join the guild at {}'.format(invite.url))
        await client.logout()
        client.loop.stop()

    with open('config.toml') as cfgfile:
        config = toml.load(cfgfile)
    client.run(config['discord_token'])


def _check_dependency(ctx, name, required_version, display_name=None):
    if display_name is None:
        display_name = name

    try:
        module = __import__(name)
    except ImportError:
        fmt = '{0} is not installed. Violet requires {0} version {1} or higher.'
        click.echo(fmt.format(display_name, '.'.join(map(str, required_version))), file=sys.stderr)
        return ctx.abort()

    if hasattr(module, 'version_info'):
        version = module.version_info
    elif hasattr(module, '__version__'):
        version = tuple(map(int, module.__version__.split('.')))
    else:
        fmt = 'Unable to determine version for module {}. This almost certainly means it is out of date.'
        click.echo(fmt.format(display_name), file=sys.stderr)
        return ctx.abort()
    _check_version(ctx, display_name, required_version, version[:3])
    return module


def _check_version(ctx, module, required, actual):
    if actual < required:
        fmt = 'Violet requires {} version {} or higher. Version {} is installed.'
        click.echo(fmt.format(module, '.'.join(map(str, required)), '.'.join(map(str, actual))), file=sys.stderr)
        ctx.abort()


def run_bot():
    import toml
    from .bot import Violet, VioletConfig
    _setup_cog_config()
    with open('config.toml') as f:
        config = VioletConfig('', toml.load(f))
    bot = Violet(config)
    _setup_logging(config.logging)
    bot.logger.debug('Set up logging')
    _load_extensions(bot)
    bot.logger.debug('Loaded extensions')

    bot.logger.info('Initialization complete, connecting to Discord...')
    bot.logger.debug('Running Violet (config=%r)', config)
    bot.run(config.discord_token)


def _setup_logging(log_cfg):
    formatter = logging.Formatter('[%(asctime)s] [%(levelname)s] [%(name)s] %(message)s')
    if log_cfg.stdout_level != 'disabled':
        stdout_logger = logging.getLogger(log_cfg.stdout_logger)
        stdout_logger.setLevel(logging.DEBUG)
        stdout_handler = logging.StreamHandler(sys.stdout)
        stdout_handler.setFormatter(formatter)
        stdout_logger.addHandler(stdout_handler)
        stdout_handler.setLevel(getattr(logging, log_cfg.stdout_level.upper()))

    logging_dir = pathlib.Path('logs')
    logging_dir.mkdir(exist_ok=True)

    for (log_type, logger_name) in [('root', ''), ('discord', 'discord'), ('violet', 'violet')]:
        lvl = getattr(log_cfg, log_type + '_level')
        if lvl != 'disabled':
            log_dir = logging_dir / log_type
            log_dir.mkdir(exist_ok=True)
            log = logging.getLogger(logger_name)
            log.setLevel(logging.DEBUG)
            handler = TimedRotatingFileHandler(str(log_dir / 'log.txt'), 'midnight', encoding='utf-8', utc=True)
            handler.setFormatter(formatter)
            handler.setLevel(getattr(logging, lvl.upper()))
            log.addHandler(handler)


def _setup_cog_config():
    import typing
    from .bot import CogsConfig
    for cog_cls in _cogs():
        config_cls = typing.get_type_hints(cog_cls).get('config')
        if config_cls is None:
            continue
        CogsConfig.__annotations__[cog_cls.__name__.lower()] = config_cls


def _load_extensions(bot):
    for name, _ in _extensions():
        bot.logger.debug('Loading extension %s', name)
        bot.load_extension(name)


def _extensions():
    for _, name, _ in pkgutil.iter_modules(['violet/commands'], 'violet.commands.'):
        yield (name, importlib.import_module(name))


def _cogs():
    from .violetlib import Cog
    for name, ext in _extensions():
        for cls in vars(ext).values():
            if isinstance(cls, type) and cls.__module__ == name and issubclass(cls, Cog):
                yield cls


if __name__ == '__main__':
    main()
