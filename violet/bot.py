# coding=utf-8
import asyncio
import json
import logging
import typing

import aiohttp
import asyncpg
import discord
from discord.ext.commands import Bot, when_mentioned_or

from .violetlib import Config, VioletContext


class Violet(Bot):
    config: 'VioletConfig'
    logger: logging.Logger = logging.getLogger(__name__)
    pool: asyncpg.pool.Pool
    home_guild: typing.Optional[discord.Guild]
    prefixes: typing.MutableMapping[int, typing.Sequence[str]]
    hidden = False  # For help messages

    def __init__(self, config: 'VioletConfig') -> None:
        client_kwargs = {'case_insensitive': True}
        if config.message_cache_size > 0:
            client_kwargs['max_messages'] = config.message_cache_size
        client_kwargs['intents'] = discord.Intents(
            guilds=True,
            members=True,
            emojis=True,
            presences=True,
            messages=True,
            reactions=True,
            typing=True,
        )
        super().__init__(type(self).command_prefix, **client_kwargs)
        self.config = config
        self.http_session = aiohttp.ClientSession(loop=self.loop)
        self.home_guild = None
        self.prefixes = {}

    async def command_prefix(self, message: discord.Message) -> typing.Sequence[str]:
        guild_id = message.author.id if message.guild is None else message.guild.id
        defaults = ['v!', 'V!']
        prefixes = when_mentioned_or(*self.prefixes.setdefault(guild_id, defaults))(self, message)
        if message.guild is None:
            prefixes.append('')
        return prefixes

    async def start(self, *args, bot: bool = True, reconnect: bool = True) -> None:
        await asyncio.gather(self.setup_db(), self.login(*args, bot=bot))
        self.owner_id = (await self.application_info()).owner.id
        await self.connect(reconnect=reconnect)

    async def setup_db(self):
        conn_kwargs = self.config.postgres.connection_args()
        self.logger.debug('Connecting to asyncpg with args %r', conn_kwargs)
        self.pool = await asyncpg.create_pool(**conn_kwargs, init=self._init_conn)
        await asyncio.gather(self._setup_db_self(), *[cog.setup_db() for cog in self.cogs.values()])

    async def _setup_db_self(self):
        async with self.pool.acquire() as conn:
            await conn.execute("CREATE TABLE IF NOT EXISTS prefixes (guild_id int8 PRIMARY KEY NOT NULL, "
                               "prefixes varchar(16)[] NOT NULL DEFAULT '{}');")
            self.prefixes = dict(await conn.fetch('SELECT * FROM prefixes;'))

    @staticmethod
    async def _init_conn(conn: asyncpg.Connection) -> None:
        await conn.set_type_codec('json', schema='pg_catalog', encoder=json.dumps, decoder=json.loads, format='text')
        await conn.set_type_codec('jsonb', schema='pg_catalog', encoder=json.dumps, decoder=json.loads, format='text')

    async def get_context(self, message: discord.Message, *, cls=VioletContext) -> VioletContext:
        return await super().get_context(message, cls=cls)

    async def on_ready(self) -> None:
        home_guild_ok = False
        self.home_guild = home_guild = self.get_guild(self.config.home_guild_id)
        if home_guild is None:
            self.logger.fatal('Home guild is invalid: I must be in the home guild')
        elif not home_guild.me.guild_permissions.administrator:
            self.logger.fatal('Home guild is invalid: I must have administrator permissions in the home guild')
        else:
            home_guild_ok = True
        if not home_guild_ok:
            await self.logout()
            self.loop.stop()
            return

        self.logger.info('Hello! I am {0} (ID {0.id}), and my home is {1} (ID {1.id}).'.format(
                         self.user, self.home_guild))
        await self.set_presence()

    async def on_message(self, message: discord.Message) -> None:
        if not message.author.bot:
            ctx = await self.get_context(message)
            self.dispatch('context', ctx)
            await self.invoke(ctx)

    async def on_guild_join(self, guild: discord.Guild):
        await self.set_presence()

    async def on_guild_remove(self, guild: discord.Guild):
        await self.set_presence()

    async def set_presence(self):
        cfg = self.config.presence
        try:
            status = discord.Status[cfg.status.lower()]
        except KeyError:
            raise ValueError(f'Unknown status type {cfg.status!r}')

        if cfg.activity:
            try:
                activity_type = discord.ActivityType[cfg.activity_type.lower()]
            except KeyError:
                raise ValueError(f'Unknown activity type {cfg.activity_type!r}')
            if activity_type is discord.ActivityType.streaming and not cfg.activity_url:
                raise ValueError('Streaming activity requires a URL')

            activity = discord.Activity(type=activity_type, url=cfg.activity_url or None,
                                        name=cfg.activity.format(client=self, guild_count=len(self.guilds)))
        else:
            activity = None
        await self.change_presence(activity=activity, status=status)


class VioletConfig(Config):
    discord_token: str
    home_guild_id: int
    message_cache_size: int = 0
    presence: 'PresenceConfig'
    postgres: 'PostgresConfig'
    logging: 'LoggingConfig'
    cogs: 'CogsConfig'


class PresenceConfig(Config):
    status: str = 'online'
    activity: str = 'for "@{client.user} prefix" in {guild_count} guilds'
    activity_type: str = 'watching'
    activity_url: str = ''


class PostgresConfig(Config):
    user: str = 'violet'
    password: str = ''
    host: str = 'localhost'
    port: int = 5432
    database: str = 'violet'
    min_connections: int = 0
    max_connections: int = 0
    lifetime_requests: int = 0
    lifetime_seconds: float = 0

    def connection_args(self) -> typing.Mapping[str, typing.Any]:
        kwargs = {key: self[key] for key in ('user', 'password', 'host', 'port', 'database')}

        optional_args = [('min_connections', 'min_size'),
                         ('max_connections', 'max_size'),
                         ('lifetime_requests', 'max_queries'),
                         ('lifetime_seconds', 'max_inactive_connection_lifetime')]
        for config_attr, arg_name in optional_args:
            value = self[config_attr]
            if value > 0:
                kwargs[arg_name] = value
        return kwargs


class LoggingConfig(Config):
    stdout_logger: str = 'violet'
    stdout_level: str = 'info'
    root_level: str = 'disabled'
    discord_level: str = 'disabled'
    violet_level: str = 'info'


class CogsConfig(Config):  # Filled by cogs as they're loaded
    __annotations__ = {}  # Required, otherwise the dict will be shared for multiple classes (apparently)
