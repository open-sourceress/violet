# coding=utf-8
import re
import typing

import discord

from . import context
from .types import AnyUser, AnyChannel

T = typing.TypeVar('T')

format_re = re.compile(r'{[^\}]+\}')  # Match any str.format arguments
format_empty_re = re.compile(r'{([\[.!:][^\}]*)?\}')  # Match str.format arguments with no name or index
mass_mention_re = re.compile('@(everyone|here)')
member_mention_re = re.compile(r'<@!?(\d+)>')
role_mention_re = re.compile(r'<@&(\d+)>')
channel_mention_re = re.compile(r'<#(\d+)>')


def cleaner_sub(cleaner: typing.Callable[[T], str], getter: typing.Callable[[int], T], backup: str) \
        -> typing.Callable[[typing.Any], str]:
    def subber(match) -> str:
        matched_id = int(match.group(1))
        obj = getter(matched_id)
        return backup.format(matched_id) if obj is None else cleaner(obj)

    return subber


def clean(ctx: 'context.VioletContext', text: typing.Union[str, AnyChannel, AnyUser, discord.Role], *,
          mass: bool = True, member: bool = True, role: bool = True, channel: bool = True):
    if isinstance(text, (discord.User, discord.Member)):
        return _clean_user(text)
    elif isinstance(text, discord.Role):
        return _clean_role(text)
    elif isinstance(text, (discord.TextChannel, discord.VoiceChannel, discord.CategoryChannel, discord.DMChannel,
                           discord.GroupChannel)):
        return _clean_channel(text)

    cleaned_text = str(text)
    if mass:
        cleaned_text = mass_mention_re.sub(lambda match: '@\N{ZERO WIDTH SPACE}' + match.group(1), cleaned_text)
    if member:
        cleaned_text = member_mention_re.sub(cleaner_sub(_clean_user, ctx.get_member, '<@\N{ZERO WIDTH SPACE}{}>'),
                                             cleaned_text)
    if role:
        cleaned_text = role_mention_re.sub(cleaner_sub(_clean_role, ctx.get_role, '<@&\N{ZERO WIDTH SPACE}{}>'),
                                           cleaned_text)
    if channel:
        cleaned_text = channel_mention_re.sub(
            cleaner_sub(_clean_channel, ctx.get_channel, '<@#\N{ZERO WIDTH SPACE}{}>'), cleaned_text)
    return cleaned_text


def is_clean(text: str) -> bool:
    return all(regex.search(text) is None for regex in
               (mass_mention_re, member_mention_re, role_mention_re, channel_mention_re))


def _clean_user(user: AnyUser) -> str:
    if is_clean(user.display_name):
        return user.display_name
    elif is_clean(str(user)):
        return str(user)
    else:
        return f'<@\N{ZERO WIDTH SPACE}{user.id}>'


def _clean_role(role: discord.Role) -> str:
    if is_clean(role.name):
        return role.name
    else:
        return f'<@&\N{ZERO WIDTH SPACE}{role.id}>'


def _clean_channel(channel: AnyChannel) -> str:
    if is_clean(channel.name):
        return f'#{channel.name}'
    else:
        return f'<#\N{ZERO WIDTH SPACE}{channel.id}>'


def format(ctx: 'context.VioletContext', content: str, *format_args: typing.Any, clean_mass: bool = True,
           clean_member: bool = True, clean_role: bool = True, clean_channel: bool = True,
           **format_kwargs: typing.Any) -> str:
    """Perform str.format-like string formatting, after cleaning the args of mentions."""

    # Cleaning the args, then passing them to str.format doesn't work. Attribute/__getitem__ lookups, format specs,
    # etc will be applied to the cleaned string instead of to the original object. The work of str.format has to
    # happen first, then cleaning, then formatting into the original string.

    # First step: format params with no index/name, relying just on position in the string, make everything harder.
    # Replace them all with the appropriate index.

    def empty_sub(match: typing.Match, i: typing.MutableSequence[int] = [0]) -> str:
        # This uses mutable-default-argument to keep i local to this function but preserve its value across calls
        i[0] += 1
        return '{%s%s}' % (i[0] - 1, match.group(1) or '')

    content_fmt: str = format_empty_re.sub(empty_sub, content)

    new_args: typing.MutableSequence[str] = []
    cleaner_kwargs = {'mass': clean_mass, 'member': clean_member, 'role': clean_role, 'channel': clean_channel}

    def arg_sub(match: typing.Match) -> str:
        formatted_val = match.group(0).format(*format_args, **format_kwargs)
        new_args.append(clean(ctx, formatted_val, **cleaner_kwargs))
        return '{}'

    content_fmt = format_re.sub(arg_sub, content_fmt)

    return content_fmt.format(*new_args)
