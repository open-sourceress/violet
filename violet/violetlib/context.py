# coding=utf-8
import logging
import typing

import discord
from discord.ext.commands import Context

from . import command, cleaners
from .pagination import Paginator
from .types import AnyChannel, AnyEmoji, AnyTextChannel, AnyUser, ellipsis
from .. import bot


class VioletContext(Context):
    default_color = discord.Color(0x740C9E)

    message: discord.Message
    channel: AnyTextChannel
    guild: typing.Optional[discord.Guild]
    author: AnyUser
    me: AnyUser
    prefix: str
    command: 'typing.Optional[command.VioletCommand]'
    bot: 'bot.Violet'

    @property
    def clean_prefix(self) -> str:
        return self.prefix.replace(f'<@{self.me.id}>', f'@{self.me}').replace(f'<@!{self.me.id}>', f'@{self.me}')

    @property
    def bot_permissions(self) -> discord.Permissions:
        return self.channel.permissions_for(self.me)

    @property
    def author_permissions(self) -> discord.Permissions:
        return self.channel.permissions_for(self.author)

    @property
    def color(self) -> discord.Color:
        return self.default_color if self.guild is None or self.me.color.value == 0 else self.me.color

    @property
    def logger(self) -> logging.Logger:
        return self.bot.logger if self.command is None else self.command.logger

    def get_member(self, identifier: typing.Union[int, str]) -> typing.Optional[AnyUser]:
        members = (self.me, self.channel.recipient) if self.guild is None else self.guild.members
        if isinstance(identifier, int):
            return discord.utils.get(members, id=identifier)
        else:
            mem = discord.utils.get(members, mention=identifier)
            if mem is None:
                mem = next((mem for mem in members if f'{mem.name}#{mem.discriminator}' == identifier), None)
            if mem is None:
                mem = discord.utils.get(members, name=identifier)
            if mem is None:
                mem = discord.utils.get(members, display_name=identifier)
            return mem

    def get_channel(self, identifier: typing.Union[int, str]) -> typing.Optional[AnyChannel]:
        if self.guild is None:
            return self.channel if self.channel.id == identifier else None
        elif isinstance(identifier, int):
            return self.guild.get_channel(identifier)
        else:
            return (discord.utils.get(self.guild.channels, name=identifier) or
                    discord.utils.get(self.guild.channels, mention=identifier))

    def get_role(self, identifier: typing.Union[int, str]) -> typing.Optional[discord.Role]:
        if self.guild is None:
            return None
        elif isinstance(identifier, int):
            return discord.utils.get(self.guild.roles, id=identifier)
        else:
            return (discord.utils.get(self.guild.roles, name=identifier) or
                    discord.utils.get(self.guild.roles, mention=identifier))

    async def send(self, content: str = None, *format_args: typing.Any, tts: bool = False, embed: discord.Embed = None,
                   file: discord.File = None, files: typing.Sequence[discord.File] = None, delete_after: float = None,
                   nonce: int = None, clean_mass: bool = True, clean_member: bool = True, clean_role: bool = True,
                   clean_channel: bool = True, **format_kwargs: typing.Any) -> discord.Message:
        if content is not None:
            content = '\N{ZERO WIDTH SPACE}' + cleaners.format(self, content, *format_args, **format_kwargs,
                                                               clean_mass=clean_mass, clean_member=clean_member,
                                                               clean_role=clean_role, clean_channel=clean_channel)
        return await super().send(content, tts=tts, embed=embed, file=file, files=files, delete_after=delete_after,
                                  nonce=nonce)

    def paginate(self, pages: typing.Sequence[discord.Embed], named_pages: typing.Mapping[str, discord.Embed] = None,
                 reactions: typing.Sequence[typing.Union[AnyEmoji, ellipsis]] = (...,), *, auto_remove: bool = True,
                 timeout: float = 60, whitelist_caller: typing.Optional[AnyUser] = None,
                 starting_page: typing.Union[int, str] = 0) -> Paginator:
        if whitelist_caller is None:
            whitelist_caller = self.author
        return Paginator(self, pages, named_pages, reactions, auto_remove=auto_remove, timeout=timeout,
                         whitelist_caller=whitelist_caller, starting_page=starting_page)
