# coding=utf-8
import asyncio
import types
import typing

import discord

from . import context
from .types import AnyEmoji, AnyUser, ellipsis
from .. import bot


class ReactorExit(BaseException):
    pass


class Reactor:
    __slots__ = ('_bot', 'message', '_reactions', '_auto_remove', '_timeout', '_user')

    def __init__(self, bot: 'bot.Violet', message: discord.Message, initial_reactions: typing.Sequence[AnyEmoji], *,
                 auto_remove: bool = True, timeout: float = 60, whitelist_caller: typing.Optional[AnyUser] = None):
        self._bot = bot
        self.message = message
        self._reactions = initial_reactions
        self._auto_remove = auto_remove
        self._timeout = timeout
        self._user = whitelist_caller

    @property
    def _me(self) -> AnyUser:
        return self.message.channel.me if self.message.guild is None else self.message.guild.me

    @property
    def can_remove_reactions(self) -> bool:
        return self.message.channel.permissions_for(self._me).manage_messages

    async def _end(self) -> None:
        if self._auto_remove:
            if self.can_remove_reactions:
                await self.message.clear_reactions()
            else:
                await asyncio.gather(*[self.message.remove_reaction(emoji, self._me) for emoji in self._reactions])

    def end(self) -> typing.NoReturn:
        raise ReactorExit

    async def __aenter__(self) -> 'Reactor':
        for emoji in self._reactions:
            await self.message.add_reaction(emoji)
        return self

    async def __aexit__(self, exc_type: typing.Optional[typing.Type[BaseException]],
                        exc_val: typing.Optional[BaseException], exc_tb: typing.Optional[types.TracebackType]) -> bool:
        await self._end()
        return isinstance(exc_val, ReactorExit)

    async def __aiter__(self):
        def check(reaction: discord.Reaction, user: AnyUser) -> bool:
            if reaction.message.id != self.message.id:
                return False
            if self._user is not None and user.id != self._user.id:
                return False
            if user.id == self._bot.user.id:
                return False
            return True

        while True:
            try:
                reaction, user = await self._bot.wait_for('reaction_add', check=check, timeout=self._timeout)
                if self._auto_remove and self.can_remove_reactions:
                    await self.message.remove_reaction(reaction, user)
                yield (reaction, user)
            except asyncio.TimeoutError:
                break


class Paginator(Reactor):
    __slots__ = ('_ctx', '_len_pages', 'pages', 'page')
    pagination_reactions = (
        '\N{BLACK LEFT-POINTING DOUBLE TRIANGLE WITH VERTICAL BAR}',  # :track_previous:
        '\N{BLACK LEFT-POINTING TRIANGLE}',  # :arrow_backward:
        '\N{BLACK RIGHT-POINTING TRIANGLE}',  # :arrow_forward:
        '\N{BLACK RIGHT-POINTING DOUBLE TRIANGLE WITH VERTICAL BAR}',  # :track_next:
        '\N{BLACK SQUARE FOR STOP}'  # :stop_button:
    )

    def __init__(self, ctx: 'context.VioletContext', pages: typing.Sequence[discord.Embed],
                 named_pages: typing.Mapping[str, discord.Embed] = None,
                 reactions: typing.Sequence[typing.Union[AnyEmoji, ellipsis]] = (...,), *, auto_remove: bool = True,
                 timeout: float = 60, whitelist_caller: typing.Optional[AnyUser] = None,
                 starting_page: typing.Union[int, str] = 0) -> None:
        all_reactions = list(reactions)
        try:
            ind = all_reactions.index(Ellipsis)
        except ValueError:
            pass  # No default reactions
        else:
            all_reactions[ind:ind + 1] = self.pagination_reactions
        super().__init__(ctx.bot, None, all_reactions, auto_remove=auto_remove, timeout=timeout,
                         whitelist_caller=whitelist_caller)
        self._ctx = ctx
        self._len_pages = len(pages)  # Max page number, excluding named pages
        self.pages: typing.Dict[typing.Union[int, str], discord.Embed] = dict(enumerate(pages))
        if named_pages:
            self.pages.update(named_pages)
        self.page = starting_page

    async def goto_page(self, page: typing.Union[int, str]) -> None:
        self.page = (page + self._len_pages) % self._len_pages if isinstance(page, int) else page
        await self.message.edit(embed=self.pages[self.page])

    async def next(self, amt: int = 1) -> None:
        return await self.goto_page((self.page + amt if isinstance(self.page, int) else 0))

    async def prev(self, amt: int = 1) -> None:
        return await self.goto_page((self.page - amt if isinstance(self.page, int) else -amt))

    async def run(self):
        async with self:
            async for _ in self:
                pass

    async def __aenter__(self) -> 'Paginator':
        self.message = await self._ctx.send(embed=self.pages[self.page])
        return await super().__aenter__()

    async def __aiter__(self):
        async for reaction, user in super().__aiter__():
            try:
                index = self.pagination_reactions.index(reaction.emoji)
            except ValueError:
                if reaction.emoji in self.pages:
                    await self.goto_page(reaction.emoji)
                else:
                    yield (reaction, user)
                continue
            if index == 0:
                await self.goto_page(0)
            elif index == 1:
                await self.prev()
            elif index == 2:
                await self.next()
            elif index == 3:
                await self.goto_page(-1)
            else:
                break

    def __await__(self):
        return self.run().__await__()
