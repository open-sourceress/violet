# coding=utf-8
import logging
import pathlib
import sys
import typing

import asyncpg
from discord.ext import commands

from . import command, config, context
from .. import bot


class CogMeta(commands.CogMeta):
    def __new__(mcs, name: str, bases: typing.Tuple[typing.Type], members: typing.Mapping[str, typing.Any],
                **kwargs: typing.Any) -> 'CogMeta':
        visible_name: str = kwargs.pop('name', None)
        hidden: bool = kwargs.pop('hidden', False)
        auto_setup: bool = kwargs.pop('auto_setup', True)
        cls: 'CogMeta' = super().__new__(mcs, name, bases, members, **kwargs)
        if auto_setup:
            sys.modules[cls.__module__].setup = cls.setup
        cls.name = cls.__name__ if visible_name is None else visible_name
        cls.hidden = hidden
        return cls

    def setup(cls, bot: 'bot.Violet') -> None:
        cfg = bot.config.cogs[cls.name.lower()] if 'config' in cls.__annotations__ else None
        cog: Cog = cls(bot, cfg)
        bot.add_cog(cog)
        cog.logger.debug('Cog loaded')


class Cog(commands.Cog, metaclass=CogMeta, auto_setup=False):
    name: str
    hidden: bool
    bot: 'bot.Violet'
    logger: logging.Logger

    def __init__(self, bot: 'bot.Violet', config: typing.Optional['config.Config']) -> None:
        self.bot = bot
        self.config = config
        self.logger = logging.getLogger(f'violet.cog.{type(self).__name__}')
        self.asset_dir = pathlib.Path('violet', 'assets', type(self).__name__.lower())

    async def setup_db(self) -> None:
        pass

    @property
    def commands(self) -> 'typing.Collection[command.VioletCommand]':
        return self.get_commands()

    def acquire_db(self, *, timeout: float = None) -> asyncpg.pool.PoolAcquireContext:
        return self.bot.pool.acquire(timeout=timeout)

    def __repr__(self) -> str:
        return f'<Cog name={self.name}>'

    @classmethod
    async def convert(cls, ctx: 'context.VioletContext', argument: str) -> 'Cog':
        cog = ctx.bot.get_cog(argument)
        if cog is None:
            raise commands.BadArgument(f'"{argument}" isn\'t a category.')
        return cog
