# coding=utf-8
import inspect
import logging
import typing

import discord
from discord.ext import commands

from . import checks, cog, context

Handler = typing.Callable[..., typing.Awaitable[None]]  # async function with unknown args


class VioletCommand:
    __original_kwargs__: typing.MutableMapping[str, typing.Any]
    _example_usage: str
    _hidden: bool
    bot_permissions: discord.Permissions
    user_permissions: discord.Permissions

    # Attributes from discord.ext.commands.Command
    name: str
    qualified_name: str
    aliases: typing.Sequence[str]
    help: str
    signature: str
    short_doc: str
    cog: 'typing.Optional[cog.Cog]'
    checks: typing.List[typing.Union[typing.Callable[[context.VioletContext], bool],
                                     typing.Callable[[context.VioletContext], typing.Awaitable[bool]]]]

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.example_usage = kwargs.pop('example_usage', '')
        self.bot_permissions = discord.Permissions()
        self.user_permissions = discord.Permissions()
        for check in self.checks:
            if isinstance(check, checks.CheckABC):
                check.apply(self)

    @property
    def example_usage(self) -> str:
        return self._example_usage

    @example_usage.setter
    def example_usage(self, usage: str) -> None:
        self._example_usage = self.__original_kwargs__['example_usage'] = inspect.cleandoc(usage)

    @property
    def hidden(self) -> bool:
        return self._hidden or (self.cog is not None and self.cog.hidden)

    @hidden.setter
    def hidden(self, value: bool):
        self._hidden = value

    @property
    def logger(self) -> logging.Logger:
        return logging.getLogger(f'violet.command.{self.qualified_name.replace(" ", ".")}')

    @classmethod
    async def convert(cls, ctx: 'context.VioletContext', argument: str) -> 'VioletCommand':
        cmd = ctx.bot.get_command(argument)
        if cmd is None:
            raise commands.BadArgument(f'"{argument}" isn\'t a command.')
        return cmd


class Command(VioletCommand, commands.Command):
    pass


class Group(VioletCommand, commands.Group):
    def command(self, *args, **kwargs) -> typing.Callable[[typing.Callable[..., typing.Any]], 'Command']:
        kwargs.setdefault('cls', Command)
        return super().command(*args, **kwargs)

    def group(self, *args, **kwargs) -> typing.Callable[[typing.Callable[..., typing.Any]], 'Group']:
        kwargs.setdefault('cls', Group)
        kwargs.setdefault('invoke_without_command', True)
        kwargs.setdefault('case_insensitive', True)
        return super().command(*args, **kwargs)


def command(*args, **kwargs) -> typing.Callable[[typing.Callable[..., typing.Any]], 'Command']:
    kwargs.setdefault('cls', Command)
    return commands.command(*args, **kwargs)


def group(*args, **kwargs) -> typing.Callable[[typing.Callable[..., typing.Any]], 'Group']:
    kwargs.setdefault('cls', Group)
    kwargs.setdefault('invoke_without_command', True)
    kwargs.setdefault('case_insensitive', True)
    return commands.group(*args, **kwargs)
