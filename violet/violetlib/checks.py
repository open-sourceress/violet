# coding=utf-8
import abc
import inspect
import typing

import discord
from discord.ext.commands import BotMissingPermissions, CheckFailure, MissingPermissions, NoPrivateMessage, NotOwner, \
    check

from . import format, command as commands, context

D = typing.TypeVar('D', typing.Callable, 'command.VioletCommand')  # Decorated; types checks can be applied to
CheckDecorator = typing.Callable[[D], D]


class CheckABC(abc.ABC):
    @abc.abstractmethod
    def can_run(self, ctx: 'context.VioletContext') -> typing.Union[bool, typing.Awaitable[bool]]:
        ...

    @abc.abstractmethod
    def pass_info(self, ctx: 'context.VioletContext') -> typing.Union[str, typing.Awaitable[str]]:
        ...

    def __call__(self, ctx: 'context.VioletContext') -> typing.Union[bool, typing.Awaitable[bool]]:
        return self.can_run(ctx)

    def apply(self, command: 'commands.VioletCommand'):
        pass


def decorator(cls: typing.Type[CheckABC]) -> typing.Callable[..., CheckDecorator]:
    sig = inspect.signature(cls)

    def deco_outer(*args, **kwargs):
        sig.bind(*args, **kwargs)

        def deco_inner(func: D) -> D:
            instance = cls(*args, **kwargs)
            if isinstance(func, commands.VioletCommand):
                instance.apply(func)
            return check(instance)(func)

        return deco_inner

    return deco_outer


class PermissionsCheck(CheckABC):
    required_permissions: typing.Sequence[str]

    def __init__(self, *required_permissions: str):
        self.required_permissions = required_permissions

    def _check(self, permissions: discord.Permissions,
               exc_factory: typing.Callable[[typing.List[str]], CheckFailure]) -> bool:
        missing = [name for name in self.required_permissions if not getattr(permissions, name)]
        if missing:
            raise exc_factory(missing)
        else:
            return True

    def _perms_pass_info(self, permissions: discord.Permissions) -> str:
        return ', '.join(('{}' if getattr(permissions, name) else '~~{}~~').format(format.permission(name))
                         for name in self.required_permissions)


class AuthorPermissions(PermissionsCheck):
    def can_run(self, ctx: 'context.VioletContext') -> bool:
        return self._check(ctx.author_permissions, MissingPermissions)

    def pass_info(self, ctx: 'context.VioletContext') -> str:
        return f'Your permissions: {self._perms_pass_info(ctx.author_permissions)}'

    def apply(self, command: 'commands.VioletCommand') -> None:
        command.bot_permissions.update(**{name: True for name in self.required_permissions})


author_permissions = decorator(AuthorPermissions)


class BotPermissions(PermissionsCheck):
    def can_run(self, ctx: 'context.VioletContext') -> bool:
        return self._check(ctx.bot_permissions, BotMissingPermissions)

    def pass_info(self, ctx: 'context.VioletContext') -> str:
        return f'My permissions: {self._perms_pass_info(ctx.bot_permissions)}'

    def apply(self, command: 'commands.VioletCommand') -> None:
        command.user_permissions.update(**{name: True for name in self.required_permissions})


bot_permissions = decorator(BotPermissions)


class GuildOnly(CheckABC):
    def can_run(self, ctx: 'context.VioletContext') -> bool:
        if ctx.guild is None:
            raise NoPrivateMessage
        else:
            return True

    def pass_info(self, ctx: 'context.VioletContext') -> str:
        return 'Guild Only'


guild_only = decorator(GuildOnly)


class OwnerOnly(CheckABC):
    async def can_run(self, ctx: 'context.VioletContext') -> bool:
        if ctx.author.id == ctx.bot.owner_id:
            return True
        else:
            raise NotOwner

    async def pass_info(self, ctx: 'context.VioletContext') -> str:
        return 'Owner Only'


owner_only = decorator(OwnerOnly)
