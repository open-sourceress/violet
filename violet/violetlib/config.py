# coding=utf-8
import collections.abc

import typing


class Config(collections.abc.MutableMapping):
    _name: str
    _data: typing.MutableMapping[str, typing.Any]
    _property_types: typing.Mapping[str, typing.Type[typing.Any]]

    def __init__(self, name: str, data: typing.Mapping[str, typing.Any]) -> None:
        self._name = name
        self._data = {}

        self._property_types = typing.get_type_hints(type(self))
        to_remove = {name for name in self._property_types if name.startswith('_')}
        for name in to_remove:
            del self._property_types[name]

        for name in self._property_types:
            try:
                self._data[name] = getattr(type(self), name)
            except AttributeError:
                pass
        self.update(data)

        missing = set(self._property_types) - set(self._data)
        missing_tables = [name for name in missing if issubclass(self._property_types[name], Config)]
        for name in missing_tables:
            try:
                table = self._property_types[name](self._child_name(name), {})  # Create with default values
            except (KeyError, ValueError):  # There are properties without default values
                continue
            else:
                self[name] = table
                missing.remove(name)
        if missing:
            raise ValueError('Missing config keys: ' + ', '.join(repr(self._child_name(name)) for name in missing))

    def _child_name(self, name: str) -> str:
        if self._name:
            return self._name + '.' + name
        else:
            return name

    def __getitem__(self, name: str) -> typing.Any:
        return self._data[name]

    def __setitem__(self, name: str, value: typing.Any) -> None:
        if name not in self._property_types:
            raise KeyError(f'Unrecognized key {self._child_name(name)!r} in config')

        if isinstance(value, collections.abc.MutableMapping):  # Value in data is a table
            if issubclass(self._property_types[name], typing.Mapping):  # Value is supposed to be a table
                if name in self._data:  # Object already there - update() has been called before
                    self._data[name].update(value)
                else:  # First time - create an instance of the proper type
                    self._data[name] = self._property_types[name](self._child_name(name), value)
            else:  # Value is a table, but shouldn't be
                fmt = 'Mismatched config type for key {!r}: expected {}, got table'
                raise ValueError(fmt.format(self._child_name(name), type(self._data[name]).__name__))
        elif issubclass(self._property_types[name], typing.Mapping):  # Value isn't a table, but is supposed to be
            fmt = 'Mismatched config type for key {!r}: expected table, got {}'
            raise ValueError(fmt.format(self._child_name(name), type(value).__name__))
        else:  # Value isn't a table, and isn't supposed to be
            expected_type = type(self).__annotations__[name]
            if isinstance(value, expected_type):
                self._data[name] = value
            else:
                fmt = 'Mismatched config type for key {!r}: expected {}, got {}'
                raise ValueError(fmt.format(self._child_name(name), expected_type.__name__, type(value).__name__))

    __delitem__ = None

    def __iter__(self) -> typing.Iterator[str]:
        return iter(self._data)

    def __len__(self) -> int:
        return len(self._data)

    def __getattribute__(self, name: str) -> typing.Any:
        if not name.startswith('_') and name in self._property_types:
            return self._data[name]
        else:
            return super().__getattribute__(name)

    def __repr__(self) -> str:
        return '<{1} config name={0._name!r} data={0._data!r}>'.format(self, type(self).__qualname__)
