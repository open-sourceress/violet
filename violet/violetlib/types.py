# coding=utf-8
import typing

import discord

AnyUser = typing.Union[discord.User, discord.Member]
AnyChannel = typing.Union[
    discord.TextChannel, discord.VoiceChannel, discord.CategoryChannel, discord.DMChannel, discord.GroupChannel]
AnyTextChannel = typing.Union[discord.TextChannel, discord.DMChannel, discord.GroupChannel]
AnyEmoji = typing.Union[str, discord.Emoji]
ellipsis = type(Ellipsis)
