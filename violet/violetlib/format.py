# coding=utf-8
import typing
from datetime import timedelta as timedelta_cls

time_components = ('weeks', 'days', 'hours', 'minutes', 'seconds')


def timedelta(delta: timedelta_cls):
    """Format a timedelta in "{} weeks, {} days, {} hours, {} minutes, {} seconds" format. Zeroes are removed."""
    minutes, seconds = divmod(delta.seconds, 60)
    hours, minutes = divmod(minutes, 60)
    weeks, days = divmod(delta.days, 7)
    return ', '.join('{} {}'.format(comp, name[:-1] if comp == 1 else name) for comp, name in
                     zip((weeks, days, hours, minutes, seconds), time_components) if comp)


def pluralize(values: typing.List[str]) -> str:
    if len(values) == 0:
        return ''
    elif len(values) == 1:
        return values[0]
    elif len(values) == 2:
        return f'{values[0]} and {values[1]}'
    else:
        return f'{", ".join(values[:-1])}, and {values[-1]}'


def permission(name: str) -> str:
    return ' '.join(name.replace('guild', 'server').split('_')).title()


def permissions(*names: str) -> str:
    return pluralize([permission(name) for name in names])
