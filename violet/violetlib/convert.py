# coding=utf-8
import datetime
import re
import typing

import discord
from discord.ext.commands import BadArgument, Converter, RoleConverter

from .context import VioletContext


def _make_timedelta_re(**parts: str) -> typing.Pattern[str]:
    num_fmt = r'\d+(\.\d*)?'
    group_fmt = rf'(?P<{{}}>{num_fmt})'
    text_fmt = r'\s*({}),?\s*'
    groups = [f'({group_fmt.format(name)}{text_fmt.format(pat)})?' for name, pat in parts.items()]
    return re.compile(''.join(groups) + '$')


timedelta_re = _make_timedelta_re(weeks='w|wks?|weeks?', days='d|days?', hours='h|hrs?|hours?',
                                  minutes='m|mins?|minutes?', seconds='s|secs?|seconds?')


def timedelta(arg) -> datetime.timedelta:
    if (match := timedelta_re.match(arg)) is not None:
        return datetime.timedelta(**{name: float(value or 0) for name, value in match.groupdict().items()})
    else:
        raise BadArgument(f'{arg} is not a valid duration')


class BotUsableRole(RoleConverter):
    """Specialization of RoleConverter that ensures the provided role can be given and taken by the bot."""

    async def convert(self, ctx: VioletContext, argument: str) -> discord.Role:
        role = await super().convert(ctx, argument)
        if role >= ctx.me.top_role and ctx.me.id != ctx.guild.owner.id:
            raise BadArgument(f'`{role.name}` is above my top role')
        else:
            return role


class UsableRole(BotUsableRole):
    """Specialization of BotUsableRole that ensures the provided role is below the caller's top role."""

    async def convert(self, ctx: VioletContext, argument: str) -> discord.Role:
        role = await super().convert(ctx, argument)
        if role >= ctx.author.top_role and ctx.author.id != ctx.guild.owner.id:
            raise BadArgument(f'`{role.name}` is above your top role')
        else:
            return role


class Permissions(Converter):
    async def convert(self, ctx: VioletContext, argument: str) -> discord.Permissions:
        if argument.isdigit():
            return discord.Permissions(int(argument))
        perms = discord.Permissions()
        perms.update(**{argument.strip().lower().replace('server', 'guild').replace(' ', '_'): True})
        if not perms.value:  # Permissions.update silently discards invalid names
            raise BadArgument(f'`{argument}` is not a valid permission name')
        return perms


if typing.TYPE_CHECKING:
    timedelta = datetime.timedelta
    BotUsableRole = discord.Role
    UsableRole = discord.Role
    Permissions = discord.Permissions
