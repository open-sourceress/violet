# coding=utf-8
import itertools
import typing

T = typing.TypeVar('T')


def segment(iterable: typing.Iterable[T], size: int) -> typing.Sequence[typing.Sequence[T]]:
    itr = tuple(iterable)
    return tuple(itr[i:i + size] for i in range(0, len(itr), size))


# https://stackoverflow.com/a/24527424
def lazy_segment(iterable: typing.Iterable[T], size: int) -> typing.Iterable[typing.Iterable[T]]:
    itr = iter(iterable)
    for seg_start in itr:  # To raise a StopIteration
        yield itertools.chain((seg_start,), itertools.islice(itr, size - 1))
